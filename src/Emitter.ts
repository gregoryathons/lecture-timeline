export default class Emitter {
    protected _element: HTMLElement;

    addEventListener(type: string, callback: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions | undefined) {
        return this._element.addEventListener(type, callback, options);
    }

    removeEventListener(type: string, callback: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions | undefined) {
        return this._element.removeEventListener(type, callback, options);
    }

    dispatchEvent(event: CustomEvent) {
        return this._element.dispatchEvent(event);
    }

    appendChild(child: HTMLElement) {
        return this._element.appendChild(child);
    }

    removeChild(child: HTMLElement) {
        return this._element.removeChild(child);
    }
}