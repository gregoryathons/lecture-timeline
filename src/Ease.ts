export const Ease = {
    Linear: (t: number) => {
        return t;
    },

    // Sine

    SineIn: (t: number) => {
        return -1 * Math.cos( t * ( Math.PI / 2 ) ) + 1;
    },

    SineOut: (t: number) => {
        return Math.sin( t * ( Math.PI / 2 ) );
    },

    SineInOut: (t: number) => {
        return -0.5 * ( Math.cos( Math.PI * t ) - 1 );
    },

    // Quad

    QuadIn: (t: number) => {
        return t * t;
    },

    QuadOut: (t: number) => {
        return t * ( 2 - t );
    },

    QuadInOut: (t: number) => {
        return -0.5 * ( Math.cos( Math.PI * t ) - 1 );
    },

    // Cubic

    CubicIn: (t: number) => {
        return t * t * t;
    },

    CubicOut: (t: number) => {
        const t1 = t - 1;
        return t1 * t1 * t1 + 1;
    },

    CubicInOut: (t: number) => {
        return t < 0.5 ? 4 * t * t * t : ( t - 1 ) * ( 2 * t - 2 ) * ( 2 * t - 2 ) + 1;
    },

    // Quart

    QuartIn: (t: number) => {
        return t * t * t * t;
    },

    QuartOut: (t: number) => {
        const t1 = t - 1;
        return 1 - t1 * t1 * t1 * t1;
    },

    QuartInOut: (t: number) => {
        const t1 = t - 1;
        return t < 0.5 ? 8 * t * t * t * t : 1 - 8 * t1 * t1 * t1 * t1;
    },

    // Quint

    QuintIn: (t: number) => {
        return t * t * t * t * t;
    },

    QuintOut: (t: number) => {
        const t1 = t - 1;
        return 1 + t1 * t1 * t1 * t1 * t1;
    },

    QuintInOut: (t: number) => {
        const t1 = t - 1;
        return t < 0.5 ? 16 * t * t * t * t * t : 1 + 16 * t1 * t1 * t1 * t1 * t1;
    },

    // Expo

    ExpoIn: (t: number) => {
        if ( t === 0 ) {
            return 0;
        }    
        return Math.pow( 2, 10 * ( t - 1 ) );
    },

    ExpoOut: (t: number) => {
        if ( t === 1 ) {
            return 1;
        }
        return ( -Math.pow( 2, -10 * t ) + 1 );
    },

    ExpoInOut: (t: number) => {
        if ( t === 0 || t === 1 ) {
            return t;
        }
    
        const scaledTime = t * 2;
        const scaledTime1 = scaledTime - 1;
    
        if ( scaledTime < 1 ) {
            return 0.5 * Math.pow( 2, 10 * ( scaledTime1 ) );
        }
    
        return 0.5 * ( -Math.pow( 2, -10 * scaledTime1 ) + 2 );
    },

    // Circ

    CircIn: (t: number) => {
        const scaledTime = t / 1;
        return -1 * ( Math.sqrt( 1 - scaledTime * t ) - 1 );
    },

    CircOut: (t: number) => {
        const t1 = t - 1;
        return Math.sqrt( 1 - t1 * t1 );
    },

    CircInOut: (t: number) => {
        const scaledTime = t * 2;
        const scaledTime1 = scaledTime - 2;

        if ( scaledTime < 1 ) {
            return -0.5 * ( Math.sqrt( 1 - scaledTime * scaledTime ) - 1 );
        }

        return 0.5 * ( Math.sqrt( 1 - scaledTime1 * scaledTime1 ) + 1 );
    },

    // Back

    BackIn: (t: number, magnitude: number = 1.70158) => {
        return t * t * ( ( magnitude + 1 ) * t - magnitude );
    },

    BackOut: (t: number, magnitude: number = 1.70158) => {
        const scaledTime = ( t / 1 ) - 1;
    
        return (
            scaledTime * scaledTime * ( ( magnitude + 1 ) * scaledTime + magnitude )
        ) + 1;
    },

    BackInOut: (t: number, magnitude: number = 1.70158) => {
        const scaledTime = t * 2;
        const scaledTime2 = scaledTime - 2;

        const s = magnitude * 1.525;

        if ( scaledTime < 1 ) {

            return 0.5 * scaledTime * scaledTime * (
                ( ( s + 1 ) * scaledTime ) - s
            );

        }

        return 0.5 * (
            scaledTime2 * scaledTime2 * ( ( s + 1 ) * scaledTime2 + s ) + 2
        );
    },

    // Elastic

    ElasticIn: (t: number, magnitude: number = 0.7) => {
        if ( t === 0 || t === 1 ) {
            return t;
        }
    
        const scaledTime = t / 1;
        const scaledTime1 = scaledTime - 1;
    
        const p = 1 - magnitude;
        const s = p / ( 2 * Math.PI ) * Math.asin( 1 );
    
        return -(
            Math.pow( 2, 10 * scaledTime1 ) *
            Math.sin( ( scaledTime1 - s ) * ( 2 * Math.PI ) / p )
        );
    },

    ElasticOut: (t: number, magnitude: number = 0.7) => {
        const p = 1 - magnitude;
        const scaledTime = t * 2;

        if ( t === 0 || t === 1 ) {
            return t;
        }

        const s = p / ( 2 * Math.PI ) * Math.asin( 1 );
        return (
            Math.pow( 2, -10 * scaledTime ) *
            Math.sin( ( scaledTime - s ) * ( 2 * Math.PI ) / p )
        ) + 1;
    },

    ElasticInOut: (t: number, magnitude: number = 0.65) => {
        const p = 1 - magnitude;

        if (t === 0 || t === 1) {
            return t;
        }

        const scaledTime = t * 2;
        const scaledTime1 = scaledTime - 1;
        
        const s = p / ( 2 * Math.PI ) * Math.asin( 1 );

        if ( scaledTime < 1 ) {
            return -0.5 * (
                Math.pow( 2, 10 * scaledTime1 ) *
                Math.sin( ( scaledTime1 - s ) * ( 2 * Math.PI ) / p )
            );
        }

        return (
            Math.pow( 2, -10 * scaledTime1 ) *
            Math.sin( ( scaledTime1 - s ) * ( 2 * Math.PI ) / p ) * 0.5
        ) + 1;
    },

    // Bounce

    BounceIn: (t: number) => {
        return 1 - Ease.BounceOut( 1 - t );
    },

    BounceOut: (t: number) => {
        const scaledTime = t / 1;

        if ( scaledTime < ( 1 / 2.75 ) ) {
            return 7.5625 * scaledTime * scaledTime;
        } 
        else if ( scaledTime < ( 2 / 2.75 ) ) {
            const scaledTime2 = scaledTime - ( 1.5 / 2.75 );
            return ( 7.5625 * scaledTime2 * scaledTime2 ) + 0.75;
        }
        else if ( scaledTime < ( 2.5 / 2.75 ) ) {
            const scaledTime2 = scaledTime - ( 2.25 / 2.75 );
            return ( 7.5625 * scaledTime2 * scaledTime2 ) + 0.9375;
        }
        else {
            const scaledTime2 = scaledTime - ( 2.625 / 2.75 );
            return ( 7.5625 * scaledTime2 * scaledTime2 ) + 0.984375;
        }
    },

    BounceInOut: (t: number) => {
        if ( t < 0.5 ) {
            return Ease.BounceIn( t * 2 ) * 0.5;
        }
        return ( Ease.BounceOut( ( t * 2 ) - 1 ) * 0.5 ) + 0.5;
    }
};