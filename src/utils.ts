declare global {
    interface Number {
        roundToThousands: () => number;
        roundToTenth: () => number;
    }
    interface Array<T> {
        shuffle: () => any;
    }
}

// so we don't end up with -2.90562958549249e value
Number.prototype.roundToThousands = function(): number {
    return Math.round(Number(this) * 1000) / 1000;
};
Number.prototype.roundToTenth = function(): number {
    return Math.round(Number(this) * 10) / 10;
};

Array.prototype.shuffle = function(): any {
    let length = this.length, j, temp;
    if (length > 0) {
        while (--length) {
            j = Math.floor(Math.random() * (length + 1));
            temp = this[length];
            this[length] = this[j];
            this[j] = temp;
        }
    }
    return this;
};

// clamps a number between min and max
export const clamp = (num: number, min: number, max: number) => Math.min(Math.max(num, min), max);

// handy dany lerp
export const lerp = (start: number, end: number, t: number) => (1 - t) * start + t * end;

// radians to degrees, degreeds to radians
export const toDeg = (radians: number) => radians * (180 / Math.PI);
export const toRad = (degrees: number) => degrees * (Math.PI / 180);

// readable time output
export const secondsToString = (totalSeconds: number) => {
    const minutes = Math.floor((((totalSeconds % 31536000) % 86400) % 3600) / 60);
    const seconds = (((totalSeconds % 31536000) % 86400) % 3600) % 60;
    return `${minutes}:${(seconds < 10 ? 0 : "") + seconds.toFixed(1)}`;
};

export async function wait(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}