import { Track } from ".";
import Emitter from "./Emitter";
import { ElementType } from "./Const";
import { clamp, lerp } from "./utils";
import { log, trace } from "./Timeline";

// define the Action Element
class HTMLActionElement extends HTMLElement {}
customElements.define(ElementType.Action, HTMLActionElement);

export default class Action extends Emitter {
    static id = 1;
    
    id = 0;
    enabled = true; // if true, it will execute the methods onBefore, onStart, etc
    locked = false; // if true, it will be disabled for selection via Editor
    _locked = false; // if true, will show the ability to toggle in the Editor
    hidden = false; // if true, the Action must set the `hidden` property on it's respective elements to `true`
    _hidden = false; // if true, will show the ability to toggle in the Editor
    interactive = false; // if true, Interaction will be called - used internally 
    track: Track; // the Track this Action is attached to
    clipId = 0; // the Clip (if any) this Action is attached to
    // isDependent = true; // if it's dependent on other Actions, for the editor
    name = "";

    percentage = -1;
    percentage01 = 0;
    duration = 0.001;
    startTime = 0;
    get endTime() {
        return this.startTime + this.duration; 
    }
    get timeline() {
        return this.track?.timeline;
    }
    get clip() {
        return this.track?.getClip(this.clipId);
    }
    get index() {
        return this.track?.actions.indexOf(this);
    }
    get type() {
        return this.constructor.name;
    }

    constructor(data: any | null = null) {
        super();

        this.id = data?.id || Action.id++;
        this.enabled = data && typeof data.enabled === "boolean" ? data.enabled : this.enabled;
        this.locked = data && typeof data.locked === "boolean" ? data.locked : this.locked;
        this.hidden = data && typeof data.hidden === "boolean" ? data.hidden : this.hidden;
        this.name = data?.name || this.name;
        this.clipId = data?.clipId || this.clipId;
        this.startTime = data?.startTime || this.startTime;
        this.duration = data?.duration || this.duration;

        // create our custom element
        this._element = document.createElement(ElementType.Action);
        this._element.dataset.id = this.id.toString();

        // so we don't get duplicates
        if (this.id >= Action.id) {
            Action.id = this.id + 1;
        }
    }

    addTo(track: Track) {
        this.track = track;

        // append to
        this.track.appendChild(this._element);

        return this;
    }

    // removes itself from the Track or Clip, returns itself
    removeFrom() {
        this._element.remove();
        this.reset();
        return this;
    }

    // so the Action can reset itself when jumping
    reset() {
        this.percentage = -1;
    }

    // will be called every tick
    update(time: number, jumpedTo: boolean = false) {
        const before = this.percentage;

        this.percentage = lerp(0, 1, ((time - this.startTime) / this.duration).roundToThousands());
        this.percentage01 = clamp(this.percentage, 0, 1);

        // if disabled, don't continue
        if (!this.enabled) {
            return;
        }

        if (before < 0 && this.percentage >= 0) {
            this.onStart(jumpedTo ? 0 : 1);
        }
        if (this.percentage > 0 && this.percentage < 1) {
            this.onUpdate(jumpedTo ? 0 : 1);
        }
        if (before < 1 && this.percentage >= 1) {
            this.onEnd(jumpedTo ? 0 : 1);
        }
    }

    onStart(direction: number) {}
    onUpdate(direction: number) {}
    onEnd(direction: number) {}

    // passed via Interaction
    onInteraction(event: Event) {}

    export(): any {
        return {
            type: this.type,
            id: this.id,
            name: this.name,
            enabled: this.enabled,
            locked: this.locked,
            hidden: this.hidden,
            clipId: this.clipId,
            startTime: this.startTime,
            duration: this.duration
        };
    }

    // for the editor

    // so each Action creates its own layout and events
    editorHTML(content: HTMLElement) {
        content.innerHTML = "Hello World!";
    }

    // returns a name friendly string
    editorName() {
        return `(${this.id}) ${this.name || this.type}`;
    }

    // returns a boolean, the Action will determine if it's ready (checking it's dependencies, etc)
    get isValid() {
        return true;
    }
}