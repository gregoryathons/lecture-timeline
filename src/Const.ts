export const TimelineEvent = {
    Play: "timeline-play",
    Pause: "timeline-pause",
    End: "timeline-end",
    Update: "timeline-update",
    JumpTo: "timeline-jump-to",

    TrackAdded: "track-added",
    TrackRemoved: "tack-removed",
    BookmarkAdded: "bookmark-added",
    BookmarkRemoved: "bookmark-removed",

    ClipAdded: "clip-added",
    ClipRemoved: "clip-removed",
    ClipsSorted: "clips-sorted",

    GroupAdded: "group-added",
    GroupRemoved: "group-removed",
    
    ActionAdded: "action-added",
    ActionRemoved: "action-removed",
    ActionBefore: "action-before",
    ActionStarted: "action-started",
    ActionUpdated: "action-updated",
    ActionEnded: "action-ended",
    ActionAfter: "action-after",
    ActionTriggered: "action-triggered",
    ActionInteraction: "action-interaction",
    ActionInteractionCheck: "action-interaction-check",
    ActionInteractionResults: "action-interaction-results",

    ImportStart: "import-start",
    ImportStop: "import-stop",

    LoaderStart: "loader-start",
    LoaderStop: "loader-stop",
    LoaderResourceAdd: "loader-resource-add"
};

export const ElementType = {
    Action: "action-element",
    Track: "track-element",
    Clip: "clip-element",
    Group: "group-element",
};