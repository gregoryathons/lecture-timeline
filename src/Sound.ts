import { Timeline } from ".";

// help from
// https://github.com/dy/string-to-arraybuffer/blob/master/index.js
// https://github.com/audiojs/audio-buffer-from/blob/master/index.js

interface IOptions {
    volume?: number;
    panner?: number;
    complete?: Function;
};

const defaultSampleRate = 44100;
const scratchSampleRate = 22050;

export default class Sound {
    timeline: Timeline;

    videoInstances: {[name: string]: VideoInstance} = {}; // for Voice Over
    soundInstances: {[name: string]: SoundInstance} = {}; // sound effects

    audioContext: AudioContext;
    gainNode: GainNode;

    constructor(timeline: Timeline) {
        this.timeline = timeline;

        this.createAudioContext();
        this.unlock();

        // used to improve sound quality and prevent audio distortion
        const dynamicsCompressorNode = this.audioContext.createDynamicsCompressor();
		dynamicsCompressorNode.connect(this.audioContext.destination);

        this.gainNode = this.audioContext.createGain();
        this.gainNode.connect(dynamicsCompressorNode);
    }

    createAudioContext() {
        let audioContext = new AudioContext();
        // check if hack is necessary. 
        // only occurs in iOS6+ devices and only when you first boot the iPhone, or play a audio/video with a different sample rate
		// if (/(iPhone|iPad)/i.test(navigator.userAgent) && audioContext.sampleRate !== defaultSampleRate) {
        if (audioContext.sampleRate !== defaultSampleRate) {
            const buffer = audioContext.createBuffer(1, 1, defaultSampleRate);
            const dummy = audioContext.createBufferSource();
            dummy.buffer = buffer;
            dummy.connect(audioContext.destination);
            dummy.start(0);
            dummy.disconnect();
            audioContext.close() // dispose old context

            audioContext = new AudioContext();
        }
        this.audioContext = audioContext;
    }

    unlock() {
        // scratch buffer for enabling iOS to dispose of web audio buffers correctly
        const scratchBuffer = this.audioContext.createBuffer(1, 1, scratchSampleRate);

        const unlock = () => {
            const source = this.audioContext.createBufferSource();
            source.buffer = scratchBuffer;
            source.connect(this.audioContext.destination);
            source.start(0);

            if (this.audioContext.state === "running") {
                // source.disconnect();

                // remove the event
                ["touchstart", "touchend", "click", "keydown"].forEach(type => {
                    document.removeEventListener(type, unlock, true);
                });
            }
        };

        // apply the event
        ["touchstart", "touchend", "click", "keydown"].forEach(type => {
            document.addEventListener(type, unlock, true);
        });
    }

    play(name: string, options: IOptions = {} as IOptions) {
        // does the sound exist?
        if (this.soundInstances[name]) {
            return this.playSound(name, options);
        }
        // how about the video?
        if (this.videoInstances[name]) {
            return this.playVideo(name, options);
        }

        const resource = this.timeline.loader.getResource(name);

        // create the Sound Instance to our array
        if (resource.type === "sound") {
            this.soundInstances[name] = new SoundInstance(this.timeline, resource.audioBuffer);
            this.playSound(name, options);
        }
        // add Video Instance to our array
        else {
            this.videoInstances[name] = new VideoInstance(this.timeline, resource.dataURL);
            this.playVideo(name, options);
        }
    }

    // usually called via Visibility
    pause() {
        this.audioContext.suspend();

        Object.values(this.videoInstances).forEach(videoInstance => {
            videoInstance.pause();
        });
    }

    // usually called via Visibility
    resume() {
        this.audioContext.resume();

        Object.values(this.videoInstances).forEach(videoInstance => {
            videoInstance.resume();
        });
    }

    // usually called via Timeline when jumping
    stop() {
        Object.values(this.soundInstances).forEach(soundInstance => {
            soundInstance.stop();
        });

        Object.values(this.videoInstances).forEach(videoInstance => {
            videoInstance.stop();
        });
    }

    private playSound(name: string, options: IOptions) {
        this.soundInstances[name].play(options);
    }

    private playVideo(name: string, options: IOptions) {
        this.videoInstances[name].play(options);
    }

    set volume(value: number) {
        Object.values(this.soundInstances).forEach(soundInstance => {
            soundInstance.volume = value;
        });
        Object.values(this.videoInstances).forEach(videoInstance => {
            videoInstance.volume = value;
        });
    }

    set playbackRate(value: number) {
        Object.values(this.soundInstances).forEach(soundInstance => {
            soundInstance.playbackRate = value;
        });
        Object.values(this.videoInstances).forEach(videoInstance => {
            videoInstance.playbackRate = value;
        });
    }
}


class SoundInstance {
    timeline: Timeline;
    audioContext: AudioContext;
    audioBuffer: AudioBuffer;
    gainNode: GainNode;
    sources: Array<AudioBufferSourceNode> = [];

    constructor(timeline: Timeline, audioBuffer: AudioBuffer) {
        this.timeline = timeline;
        this.audioContext = this.timeline.sound.audioContext;
        this.audioBuffer = audioBuffer;

        this.gainNode = this.audioContext.createGain();
        this.gainNode.connect(this.audioContext.destination);
    }

    play(options: IOptions) {
        // create our Audio Node
        const bufferSource = new AudioBufferSourceNode(this.audioContext, { buffer: this.audioBuffer });

        // -1 left, 0 center, 1 right
        const pannerNode = new StereoPannerNode(this.audioContext, { pan: options.panner || 0 });

        // const bufferSource = this.sound.audioContext.createBufferSource();
        // bufferSource.buffer = this.audioBuffer;
        bufferSource.connect(this.gainNode).connect(pannerNode);

        // set the volume
        this.volume = options.volume || 1;

        // add to our array
        this.sources.push(bufferSource);

        // remove once we are done playing it
        bufferSource.addEventListener("ended", () => {
            this.sources.splice(this.sources.indexOf(bufferSource), 1);

            // callback, if any
            (options.complete || function() {})();
        });

        bufferSource.start(0);
    }

    pause() {}

    resume() {}

    stop() {
        this.sources.forEach(source => source.stop());
        this.sources.length = 0;
    }

    set volume(value: number) {
        this.gainNode.gain.value = this.timeline.volume * value;
    }

    // don't do it, or it will sound all chipmunk like
    set playbackRate(value: number) {}

    destroy() {
        this.gainNode.disconnect();
    }
}


class VideoInstance {
    timeline: Timeline;
    element: HTMLVideoElement;
    base64: string;

    constructor(timeline: Timeline, base64: string) {
        this.timeline = timeline;

        this.element = document.createElement("video") as HTMLVideoElement;
        this.base64 = base64;
    }

    play(options: IOptions) {
        // create the source
        const source = document.createElement("source") as HTMLSourceElement;
        source.type = "video/mp4";
        source.src = this.base64;
        this.element.appendChild(source);

        // load it
        this.element.load();

        this.volume = options.volume || 1;
        this.playbackRate = this.timeline.playbackRate || 1;

        const self = this;
        this.element.addEventListener("ended", function onEnded() {
            self.element.removeEventListener("ended", onEnded);
            (options.complete || function() {})();

            // remove it afterwards
            self.destroy();
        });

        // play it
        this.resume();
    }

    pause() {
        if (this.element.querySelector("source")) {
            this.element.pause();
        }
    }

    resume() {
        if (this.element.querySelector("source")) {
            this.element.play();
        }
    }

    stop() {
        this.destroy();
    }

    set volume(value: number) {
        this.element.volume = this.timeline.volume * value;
    }

    set playbackRate(value: number) {
        this.element.playbackRate = value;
    }

    destroy() {
        const sources = this.element.querySelectorAll("source");

        if (sources.length) {
            this.element.pause();
        }

        Array.from(sources).forEach(source => source.remove());

        // need to call `load` on the element to ensure it removes it
        this.element.load();
    }
}