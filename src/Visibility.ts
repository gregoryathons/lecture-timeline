import { Timeline } from ".";

export default class Visibility {
    isFocused = true;
    autoStop = true;
    wasPaused = false;

    timeline: Timeline;

    constructor(timeline: Timeline) {
        this.timeline = timeline;

        this.setup();
    }

    setup() {
        let hidden = "";

		if (document.hidden !== undefined) {
			hidden = "visibilitychange";
		}
		else {
			["webkit", "moz", "ms"].forEach((prefix) => {
				if ((document as any)[`${prefix}Hidden`] !== undefined) {
					hidden = `${prefix}Hidden`;
				}
			});
		}

		if (hidden !== "") {
			document.addEventListener(hidden, (event: Event) => {
                if (document.hidden || event.type === "pause") {
                    if (this.autoStop) {
                        this.onHide(event.type);
                    }
                }
                else {
                    if (this.autoStop) {
                        this.onShow(event.type);
                    }
                }
            });
		}

        window.addEventListener("focus", (event) => {
            if (this.autoStop) {
                this.onShow(event.type);
            }
        });

        window.addEventListener("blur", (event) => {
            if (this.autoStop) {
                this.onHide(event.type);
            }
        });
    }

    onShow(type: string) {
        if (this.isFocused) {
            return;
        }

        this.isFocused = true;

        if (this.wasPaused) {
            this.timeline.resume();
            this.timeline.sound.resume();
        }
    }

    onHide(type: string) {
        if (!this.isFocused) {
            return;
        }

        this.isFocused = false;
        this.wasPaused = this.timeline.isPlaying;

        this.timeline.pause();
        this.timeline.sound.pause();
    }
}