import { TimelineEvent } from "./Const";
import Timeline from "./Timeline";

export interface IResource {
    isLoaded: boolean;
    name: string;
    title: string;
    kind: string;
    tags: Array<string>;
    dataURL: string; // base64
};

export interface IResourceSoundInfo {
    bytes: number;
    dur: number;
    etag: string;
    id: number;
    path: string;
};

export interface IResourceSound extends IResource {
    type: "sound";
    audioBuffer: AudioBuffer;
    mp3: IResourceSoundInfo;
    mp4: IResourceSoundInfo;
};

export interface IResourceImage extends IResource {
    type: "image";
    path: string;
};

class Loader extends EventTarget {
    isLoading = false;
    timeline: Timeline;
    // audioContext: AudioContext;
    rootPath = "";
    tags: Array<string> = [];

    resources: {[type: string]: Array<IResourceSound | IResourceImage>} = {
        sounds: [],
        images: [],
    };

    constructor(timeline: Timeline) {
        super();

        this.timeline = timeline;
        // this.audioContext = new AudioContext();
    }

    addTag(tag: string) {
        if (this.tags.indexOf(tag) < 0) {
            this.tags.push(tag);
            return true;
        }
        return false;
    }

    addResource(type: string, newResources: Array<IResourceSound | IResourceImage>) {
        // add to existing
        const existingResources = this.resources[type];
        if (!existingResources) {
            throw `No resource by that name: ${type}`;
        }

        // let's make sure we aren't overwriting an existing
        newResources.forEach(newResource => {
            // if we find a duplicate name, don't add it
            if (!existingResources.find(existingResource => newResource.name === existingResource.name)) {

                // can't do `existingResources.push(newResource as IResourceSound)`
                if (/(sound(s)?)/ig.test(type)) {
                    newResource.type = "sound";
                }
                else if (/(image(s)?)/ig.test(type)) {
                    newResource.type = "image";
                }

                // default value
                newResource.isLoaded = false;

                // add to existing
                existingResources.push(newResource);

                // tell everyone
                this.dispatchEvent(new CustomEvent(TimelineEvent.LoaderResourceAdd, {
                    bubbles: true,
                    detail: {
                        action: "added",
                        resource: newResource
                    }
                }));
            }
        });

        // array.push.apply(array, info);
    }

    // returns the Resource object by name
    getResource(name: string) {
        let resource: any;
        Object.keys(this.resources).forEach(type => {
            resource = resource || this.resources[type].find((res: IResourceSound | IResourceImage) => res.name === name);
        });
        return resource;
    }

    // returns an array of Resource objects by it's `kind`
    getResources(type: string, kind: string = "") {
        let returnedResources: Array<IResourceSound | IResourceImage> = [];
        (this.resources[type] || []).forEach(res => {
            if (res.kind === kind || !kind) {
                returnedResources.push(res);
            }
        });
        return returnedResources;
    }

    // gets the path of the resource
    getPath(resource: IResourceSound | IResourceImage) {
        if (resource.type === "sound") {
            return resource.kind === "nv" ? resource.mp4.path : resource.mp3.path;
        }
        else if (resource.type === "image") {
            return resource.path;
        }
        return "";
    }

    // load all resources via passsed names
    async loadResources(names: string[] = []) {
        return Promise.all(Object.keys(this.resources).map(async type => {
            return Promise.all(this.resources[type].map(async (resource: IResourceSound | IResourceImage) => {
                if (names.indexOf(resource.name) > -1) {
                    await this.loadResource(resource);
                }
            }));
        }));
    }

    // load the resource
    async loadResource(resource: string | IResourceSound | IResourceImage) {
        let name = "";

        // passed a number? convert it to a IResource
        if (typeof resource === "string") {
            name = resource;
            resource = this.getResource(resource) as IResourceSound | IResourceImage;
        }

        if (!resource) {
            if (name) {
                console.log("could not load", name);
            }
            return;
        }

        if (!resource.isLoaded) {
            this.isLoading = true;

            // load start
            this.dispatchEvent(new CustomEvent(TimelineEvent.LoaderStart, {
                bubbles: true,
                detail: {
                    resource: resource
                }
            }));

            // load it
            let response = await fetch(this.rootPath + this.getPath(resource));

            // everything was ok
            if (response.ok) {
                // get our dataURL - this will be passed to the iFrame eventually
                let blob = await response.clone().blob();

                // make a File Reader to convert it as a base64
                const fileReader = new FileReader();
                await new Promise(resolve => {
                    fileReader.onload = () => {
                        (resource as IResourceSound | IResourceImage).dataURL = fileReader.result as string;
                        resolve(true);
                    };
                    fileReader.readAsDataURL(blob);
                });

                // if it's a video or audio type, we must decode it
                if (resource.type === "sound") {
                    const arrayBuffer = await response.clone().arrayBuffer();
                    const audioBuffer = await this.timeline.sound.audioContext.decodeAudioData(arrayBuffer);// this.audioContext.decodeAudioData(arrayBuffer);
                    resource.audioBuffer = audioBuffer;
                }

                // done
                resource.isLoaded = true;
            }

            this.isLoading = false;

            // load stop
            this.dispatchEvent(new CustomEvent(TimelineEvent.LoaderStop, {
                bubbles: true,
                detail: {
                    resource: resource
                }
            }));
        }
    }
}

export default Loader;