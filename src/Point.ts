export default class Point extends DOMPoint {
    equal(point: Point) {
        return this.x === point.x && this.y === point.y;    
    }

    add(point: Point) {
        this.x += point.x;
        this.y += point.y;
        return this;
    }

    subtract(point: Point) {
        this.x -= point.x;
        this.y -= point.y;
        return this;
    }

    scale(scalar: number) {
        this.x *= scalar;
        this.y *= scalar;
        return this;
    }

    magnitude() {
        return Point.distance(new Point(), this);
    }

    static distance(pointA: Point, pointB: Point) {
        return Math.sqrt(Math.pow(pointB.x - pointA.x, 2) + Math.pow(pointB.y - pointA.y, 2));
    }

    static direction(pointA: Point, pointB: Point) {
        return Math.atan2(pointB.y - pointA.y, pointB.x - pointA.x);
    }
}