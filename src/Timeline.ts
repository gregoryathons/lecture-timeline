import { Action, Track, Clip, Group } from ".";
import { TimelineEvent } from "./Const";
import Emitter from "./Emitter";
import Interaction from "./Interaction";
import Loader from "./Loader";
import Sound from "./Sound";
import Visibility from "./Visibility";

let _showLog = true;
export function log(...data: any) {
    if (_showLog) {
        console.log.apply(console.log, data);
    }
};
export function trace(...data: any) {
    if (_showLog) {
        console.groupCollapsed.apply(console.groupCollapsed, data);
        console.trace();
        console.groupEnd();
    }
};

interface ITimelineOptions {
    content: HTMLElement | string;
    subtitles: HTMLElement | string;
};

export default class Timeline extends Emitter {
    private _isPlaying = false;
    private _isLocked: Action | null = null;

    visibility: Visibility;
    interaction: Interaction;
    loader: Loader;
    sound: Sound;

    content: HTMLElement; // where our elements will be added to
    subtitles: HTMLElement; // where our CC will be added to
    tracks: Array<Track> = []; // our Tracks
    currentTrack: Track | null; // current Track in use
    deltaTime = 0;
    deltaScale = 0;

    private lastTimestamp = 0; // to get the delta time
    
    private raf = 0; // request animation frame
    private _volume = 1;
    private _playbackRate = 1;

    set showLog(log: boolean) {
        _showLog = log;
    }
    get showLog() {
        return _showLog;
    }

    // viewer: HTMLElement | string
    constructor(options: ITimelineOptions) {
        super();

        this.content = <HTMLElement> (typeof options.content === "string" ? document.querySelector(options.content) : options.content);
        this.subtitles = <HTMLElement> (typeof options.subtitles === "string" ? document.querySelector(options.subtitles) : options.subtitles);

        if (!this.content) {
            throw "Content Element does not exist!";
        }
        if (!this.subtitles) {
            throw "Subtitles Element does not exist!";
        }

        // off screen Video element, our Voice Over
        this._element = <HTMLVideoElement> document.createElement("video");

        this.visibility = new Visibility(this);
        this.interaction = new Interaction(this);
        this.sound = new Sound(this);
        this.loader = new Loader(this);
    }

    getVideo() {
        return this._element as HTMLVideoElement;
    }

    jumpTo(time: number) {
        time = time.roundToTenth();

        // pause it
        this.pause();

        // stop the sounds
        this.sound.stop();
        
        // get the first one
        if (!this.currentTrack) {
            this.currentTrack = this.tracks[0];

            if (!this.currentTrack) {
                throw "NO TRACK jumpTo";
            }
        }

        this._isPlaying = false;
        this._isLocked = null;

        this.currentTrack?.jumpTo(time);

        // dispatch
        this.dispatchEvent(new CustomEvent(TimelineEvent.JumpTo, {
            detail: {
                time: time
            }
        }));
    }

    play() {
        if (this.isPlaying) {
            return;
        }

        // get the first one
        if (!this.currentTrack) {
            this.currentTrack = this.tracks[0];
            if (!this.currentTrack) {
                throw "NO TRACK play";
            }
        }
        
        this._isPlaying = true;
        this._isLocked = null;

        // dispatch
        this.dispatchEvent(new CustomEvent(TimelineEvent.Play, {
            detail: {
                trackId: this.currentTrack.id,
                time: this.time
            }
        }));

        // update
        if (this.currentTrack.currentClip) {
            this.lastTimestamp = performance.now();
            this.update(this.lastTimestamp);
        }
        else {
            this.end();
        }
    }

    pause(locked: Action | null = null) {
        if (!this.isPlaying) {
            return;
        }
        this._isPlaying = false;
        this._isLocked = locked;

        // pause the video and sounds
        this.getVideo().pause();
        this.sound.pause();

        // clear/cancel the other stuff
        window.cancelAnimationFrame(this.raf);

        this.dispatchEvent(new CustomEvent(TimelineEvent.Pause));
    }

    resume(pickLock: boolean = false) {
        if (this.isPlaying) {
            return;
        }
        if (!pickLock && this.isLocked) {
            return;
        }
        this._isPlaying = true;
        this._isLocked = null;

        this.dispatchEvent(new CustomEvent(TimelineEvent.Play, {
            detail: {
                trackId: this.currentTrack?.id,
                time: this.time
            } 
        }));

        if (this.currentTrack?.currentClip) {
            this.lastTimestamp = performance.now();
            this.update(this.lastTimestamp);
        }
        else {
            this.end();
        }
    }

    end() {
        this.pause();

        this.dispatchEvent(new CustomEvent(TimelineEvent.End));
    }

    update(timestep: number) {
        let deltaTime = Math.abs(timestep - this.lastTimestamp);
        this.lastTimestamp = timestep;
        this.deltaTime = deltaTime * 0.001;
        this.deltaScale = this.deltaTime * this.playbackRate;

        // update the Track
        this.currentTrack?.update();

        this.dispatchEvent(new CustomEvent(TimelineEvent.Update, { 
            detail: {
                time: this.time,
                timestep: timestep,
                deltaTime: this.deltaTime
            }
        }));

        // keep going?
        if (this.currentTrack?.currentClip) {
            if (this.isPlaying) {
                this.raf = window.requestAnimationFrame(timestep => this.update(timestep));
            }
        }
        // done
        else {
            this.end();
        }
    }

    // should be called from outside
    checkInteraction() {
        const event = new CustomEvent(TimelineEvent.ActionInteractionCheck);
        this._isLocked?.onInteraction(event);
        this.dispatchEvent(event);
    }
 
    loadTrackClip(track: Track, advanceTime: number = 0) {
        // empty the old source tags
        Array.from(this.getVideo().querySelectorAll("source")).forEach(source => source.remove());

        // create the new source tag
        const source = document.createElement("source");
        source.type = "video/mp4";
        source.src = track.currentClip!.getResource().dataURL;

        // append and start loading
        const videoElement = this.getVideo();
        videoElement.appendChild(source);
        videoElement.load();

        // set volume and playbackRate
        videoElement.volume = this.volume;
        videoElement.playbackRate = this.playbackRate;

        // offset the time
        videoElement.currentTime = advanceTime;
    }

    // add Track
    addTrack(track: Track) {
        // append it
        this.tracks.push(track.addTo(this));

        this.dispatchEvent(new CustomEvent(TimelineEvent.TrackAdded, { 
            detail: { 
                track: track
            }
        }));

        return track;
    }

    // remove Track
    removeTrack(track: Track) {
        const index = this.tracks.indexOf(track.removeFrom());
        if (index >= 0) {
            this.tracks.splice(index, 1);
        }

        if (track === this.currentTrack) {
            this.currentTrack = null; // remove it
        }

        this.dispatchEvent(new CustomEvent(TimelineEvent.TrackRemoved, { 
            detail: { 
                track: track 
            } 
        }));

        return this;
    }

    get isPlaying() {
        return this._isPlaying;
    }
    get isLocked() {
        return this._isLocked !== null;
    }
    get time() {
        return this.currentTrack?.time || 0;
    }

    // getter/setter for volume
    get volume() {
        return this._volume;
    }
    set volume(value) {
        this._volume = value;
        this.getVideo().volume = this._volume;
        this.sound.volume = this._volume;
    }

    // getter/setter for playback rate
    get playbackRate() {
        return this._playbackRate;
    }
    set playbackRate(value) {
        this._playbackRate = value;
        this.getVideo().playbackRate = this._playbackRate;
        this.sound.playbackRate = this._playbackRate;
    }

    getTrack(id: number) {
        return this.tracks.find(track => track.id === id) || null;
    }

    getClip(id: number) {
        let found: Clip | null = null;
        this.tracks.forEach(track => {
            found = found || track.clips.find(clip => clip.id === id) || null;
        });
        return found;
    }

    getAction(id: number) {
        let found: Action | null = null;
        this.tracks.forEach(track => {
            found = found || track.getAction(id);
        });
        return found;
    }

    getGroup(id: number) {
        let found: Group | null = null;
        this.tracks.forEach(track => {
            found = found || track.getGroup(id);
        });
        return found;
    }

    // import/export

    empty() {
        // empty tracks
        while (this.tracks.length) {
            this.removeTrack(this.tracks.pop()!);
        }
        // deselect Track
        this.currentTrack = null;
        // reset id's
        Track.id = Clip.id = Action.id = 1;
    }

    async import(data: any) {
        this.dispatchEvent(new CustomEvent(TimelineEvent.ImportStart));

        // out with the old
        this.empty();

        // in with the new
        data.tracks.forEach((track: any) => {
            this.addTrack(new Track(track));
        });

        // get all the Resource id's used
        let resourceNames: Array<string> = [];
        this.tracks.forEach(track => {
            track.actions.forEach((action: any) => resourceNames = resourceNames.concat(action.resourceNames || []));
            track.clips.forEach((clip: any) => resourceNames = resourceNames.concat([clip.resourceName]));
        });

        // console.log("load", resourceNames);
        
        await this.loader.loadResources(resourceNames);

        this.dispatchEvent(new CustomEvent(TimelineEvent.ImportStop));
    }

    export() {
        const timeline = {
            tracks: [] as any,
            tags: this.loader.tags
        };
        this.tracks.forEach(track => timeline.tracks.push(track.export()));
        return timeline;
    }
}