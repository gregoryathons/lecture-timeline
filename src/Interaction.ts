import { Timeline, Action } from ".";

export default class Interaction {
    timeline: Timeline;
    ftnInteractionBound: Function;
    windowEvents = ["click", "input", "keyup", "mousedown", "mousemove", "mouseup", "touchstart", "touchmove", "touchend", "resize"];
    documentEvents = ["drag", "dragstart", "dragend", "dragenter", "dragleave", "dragover", "drop"];

    constructor(timeline: Timeline) {
        this.timeline = timeline;

        this.ftnInteractionBound = (event: Event) => this.onInteraction(event);

        this.enable();
    }

    enable() {
        this.windowEvents.forEach(name => {
            window.addEventListener(name, this.ftnInteractionBound as EventListener);
        });
        this.documentEvents.forEach(name => {
            document.addEventListener(name, this.ftnInteractionBound as EventListener);
        });
    }

    disable() {
        this.windowEvents.forEach(name => {
            window.removeEventListener(name, this.ftnInteractionBound as EventListener);
        });
        this.documentEvents.forEach(name => {
            document.removeEventListener(name, this.ftnInteractionBound as EventListener);
        });
    }

    private onInteraction(event: Event) {
        this.timeline.currentTrack?.actions.forEach((action: Action) => {            
            if (action.interactive) {
                action.onInteraction(event);
            }
        });
    }
}