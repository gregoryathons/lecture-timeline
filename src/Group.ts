import { Track, Matrix } from ".";
import Emitter from "./Emitter";
import { ElementType } from "./Const";
import { log, trace } from "./Timeline";
import { unwrap } from "./Actions/Styles/utils";
import { AddElement } from "./Actions";

// define the Group Element
class HTMLGroupElement extends HTMLElement {}
customElements.define(ElementType.Group, HTMLGroupElement);

function convertToUnit(value: number, ratio: "width" | "height", element: HTMLElement) {
    const css = window.getComputedStyle(element);
    const borderLR = parseFloat(css.borderLeftWidth) + parseFloat(css.borderRightWidth);
    const borderTB = parseFloat(css.borderTopWidth) + parseFloat(css.borderBottomWidth);

    const rect = element.getBoundingClientRect();
    const size = ratio === "width" ? rect.width - borderLR : rect.height - borderTB;

    return (value * 100) / size;
};

export default class Group extends Emitter {
    static id = 1;

    id = 0;
    track: Track;

    startTime = 0;
    duration = Infinity;
    otherIds: Array<number> = [];
    rect: DOMRect = <DOMRect> {};
    matrix: DOMMatrix = <DOMMatrix> {};

    element: HTMLElement; // this Group element, wraps the otherElements
    otherElements: Array<HTMLElement> = [];
    populated = false;
    disposed = false;

    get timeline() {
        return this.track?.timeline;
    }
    get endTime() {
        return this.startTime + this.duration;
    }

    constructor(data: any | null = null) {
        super();

        this.id = data?.id || Group.id++;
        this.startTime = data?.startTime || this.startTime;
        this.duration = data?.duration || this.duration;
        this.otherIds = data?.otherIds || this.otherIds;
        this.rect = data?.rect || this.rect;
        this.matrix = data?.matrix || this.matrix;

        // create our element
        this._element = document.createElement(ElementType.Group);
        this._element.dataset.id = this.id.toString();

        // so we don't get duplicates
        if (this.id >= Group.id) {
            Group.id = this.id + 1;
        }
    }

    addTo(track: Track) {
        this.track = track;

        // append to
        this.track.appendChild(this._element);

        return this;
    }

    removeFrom() {
        this._element.remove();
        this.removeElement();
        this.reset();
        return this;
    }

    reset() {
        this.element?.remove();
        this.otherElements.length = 0;
        this.populated = false;
        this.disposed = false;
    }

    update(time: number, jumpedTo: boolean = false) {
        if (this.disposed) {
            return;
        }

        if (time >= this.startTime && !this.populated) {
            // create it
            this.createElement();
            // mark it has been
            this.populated = true;
        }
        if (this.populated) {
            // it's now empty
            if (!this.element.children.length) {
                this.element?.remove();
                this.disposed = true;
            }
            // done
            if (time >= this.endTime) {
                this.removeElement();
                this.disposed = true;
            }
        }
    }

    createElement() {
        this.element = document.createElement("div");
        this.element.draggable = false;
        this.element.dataset.groupId = this.id.toString();
        this.element.classList.add("element", "group");

        // TODO - get highest most element, not just the Timeline's viewer
        this.timeline.content.appendChild(this.element);

        // get all our elements
        this.otherIds.forEach(otherId => {
            let otherElement = this.timeline.content.querySelector(`.element[data-action-id="${otherId}"]`) as HTMLElement;
            if (otherElement) {
                this.otherElements.push(otherElement);
            }
        });

        // get bounds of the other elements to set `this.element` position and size relative to them
        const viewerRect = this.timeline.content.getBoundingClientRect();
        const rect = this.getElementsBounds(this.otherElements);
        rect.x = convertToUnit(rect.x - viewerRect.x, "width", this.timeline.content);
        rect.y = convertToUnit(rect.y - viewerRect.y, "height", this.timeline.content);
        rect.width = convertToUnit(rect.width, "width", this.timeline.content);
        rect.height = convertToUnit(rect.height, "height", this.timeline.content);

        // set the position, but don't override our props
        this.setRectAndMatrix(rect, new DOMMatrix(), false);

        // set the other elements inside ours
        this.otherElements.forEach(element => {
            
            const actionId = parseInt(element.dataset.actionId!);
            const action = this.timeline.getAction(actionId) as unknown as AddElement;
            if (!action) {
                return;
            }

            const groupRect = this.element.getBoundingClientRect();
            const rect = element.getBoundingClientRect();
            const x = ((rect.left - groupRect.left) / groupRect.width) * 100;
            const y = ((rect.top - groupRect.top) / groupRect.height) * 100;

            element.style.inset = `${y}% auto auto ${x}%`;
            if (element.style.width !== "auto") {
                element.style.width = `${(rect.width / groupRect.width) * 100}%`;
            }
            if (element.style.height !== "auto") {
                element.style.height = `${(rect.height / groupRect.height) * 100}%`;
            }

            // log(element.style.width, element.style.height, rect);

            // get the parent of this element before we add to this Group's element
            const otherParent = element.parentElement;

            // add to our Group
            this.element.appendChild(element);

            // is that other parent a Group? is it now empty? remove it
            if (otherParent && otherParent.closest(".group")) {
                const parentGroup = otherParent.closest(".group") as HTMLElement;
                if (!parentGroup.children.length) {
                    const otherGroupId = parseInt(parentGroup.dataset.groupId!);
                    const otherGroup = this.track.getGroup(otherGroupId);

                    // remove the other Group
                    if (otherGroup) {
                        otherGroup.element?.remove();
                        otherGroup.disposed = true;
                    }
                }
            }
        });

        // log(this.id, this.rect.width, rect.width, convertToUnit(this.element.getBoundingClientRect().width, "width", this.timeline.content));
        // if (!this.rect.width) { this.rect.width = rect.width; }
        // if (!this.rect.height) { this.rect.height = rect.height; }

        // finally, set the final rect and matrix
        this.setRectAndMatrix(this.rect, this.matrix);
    }

    removeElement() {
        if (!this.element || !this.element.parentElement) {
            return;
        }

        const parent = this.element.parentElement;
        
        this.otherElements.forEach(element => {
            const actionId = parseInt(element.dataset.actionId!);
            const action = this.timeline.getAction(actionId) as unknown as AddElement;
            if (!action) {
                return;
            }
            
            const parentRect = parent.getBoundingClientRect();
            const rect = element.getBoundingClientRect();
            const x = ((rect.left - parentRect.left) / parentRect.width) * 100;
            const y = ((rect.top - parentRect.top) / parentRect.height) * 100;
            const w = element.style.width === "auto" || element instanceof HTMLImageElement ? 0 : (rect.width / parentRect.width) * 100;
            const h = element.style.height === "auto" || element.querySelector("svg") ? 0 : (rect.height / parentRect.height) * 100;

            // log(actionId, x, ((rect.x - parentRect.x) / parentRect.width) * 100);
            // log("ungroup", actionId, x, y, w, h);
            // log("ungroup", actionId, action.rect.x, rect.left - parentRect.left, parentRect.width, (rect.left - parentRect.left) / parentRect.width);

            parent.appendChild(element);

            // action.setRectAndMatrix(new DOMRect(x, y, w, h), action.matrix);
            action.setRectAndMatrix(new DOMRect(x, y, action.rect.width, action.rect.height), action.matrix);
        });

        this.element.remove();
    }

    // get the bounds, snippet of code was taken from Phaser PWA
    getElementsBounds(elements: Array<HTMLElement>) {
		let minX = Infinity, maxX = -Infinity;
		let minY = Infinity, maxY = -Infinity;

		elements.forEach(element => {
            const rect = element.getBoundingClientRect();
			minX = Math.min(rect.left, minX);
			maxX = Math.max(rect.right, maxX);
			minY = Math.min(rect.top, minY);
			maxY = Math.max(rect.bottom, maxY);
		});

		return new DOMRect(minX, minY, maxX - minX, maxY - minY);
	}

    setRectAndMatrix(rect: DOMRect, matrix: DOMMatrix, setProps: boolean = true): boolean {
        // apply the transform info
        this.element.style.inset = `${rect.y}% auto auto ${rect.x}%`;
        this.element.style.width = `${rect.width}%`;
        this.element.style.height = `${rect.height}%`;
        
        const m = Matrix.fromMatrix(matrix);
        this.element.style.transform = m.toString();

        // set the props
        if (setProps) {
            this.rect = rect;
            this.matrix = m;
        }

        return true;
    }

    // import/export

    export() {
        // round to 2 decimal places
        const r = (n: number) => (n * 100) / 100;

        // so there is less data being exported
        // @ts-ignore
        this.rect && Object.keys(this.rect).forEach(prop => this.rect[prop] = r(this.rect[prop]));
        // @ts-ignore
        this.matrix && Object.keys(this.matrix).forEach(prop => this.matrix[prop] = r(this.matrix[prop]));

        return { 
            id: this.id,
            startTime: this.startTime,
            duration: this.duration,
            otherIds: this.otherIds,
            rect: this.rect,
            matrix: this.matrix
        };
    }
}