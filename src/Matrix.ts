import Point from "./Point";

interface IMatrix {
    translateX: number;
    translateY: number;
    skewX: number;
    skewY: number;
    scaleX: number;
    scaleY: number;
    rotation: number;
};

export default class Matrix extends DOMMatrix {
    concat(matrix: Matrix) {
        const m = new Matrix();
        m.a = this.a * matrix.a + this.c * matrix.b;
        m.b = this.b * matrix.a + this.d * matrix.b;
        m.c = this.a * matrix.c + this.c * matrix.d;
        m.d = this.b * matrix.c + this.d * matrix.d;
        m.e = this.a * matrix.e + this.c * matrix.f + this.e;
        m.f = this.b * matrix.e + this.d * matrix.f + this.f;
        return m;
    }

    deltaTransformPoint(point: Point) {
        return new Point(
            this.a * point.x + this.c * point.y,
            this.b * point.x + this.d * point.y
        );
    }

    inverse() {
        const determinant = this.a * this.d - this.b * this.c;
        const m = new Matrix();
        m.a = this.d / determinant;
        m.b = -this.b / determinant;
        m.c = -this.c / determinant;
        m.d = this.a / determinant;
        m.e = (this.c * this.f - this.d * this.e) / determinant;
        m.f = (this.b * this.e - this.a * this.f) / determinant;
        return m;
    }

    doRotate(theta: number, aboutPoint: Point | null = null) {
        return this.concat(Matrix.rotation(theta, aboutPoint));
    }

    doScale(sx: number, sy: number, aboutPoint: Point | null = null) {
        return this.concat(Matrix.scale(sx, sy, aboutPoint));
    }

    doTranslation(e: number, f: number) {
        return this.concat(Matrix.translation(e, f));
    }

    transformPoint(point: Point) {
        return new Point(
            this.a * point.x + this.c * point.y + this.e,
            this.b * point.x + this.d * point.y + this.f
        );
    }

    // https://stackoverflow.com/questions/9818702/is-there-js-plugin-convert-the-matrix-parameter-to-css3-transform-property
    decompose(useLU = false) {
        const determinant = this.a * this.d - this.b * this.c;

        const info = <IMatrix> {
            translateX: this.e,
            translateY: this.f,
            skewX: 0,
            skewY: 0
        };

        if (useLU) {
            if (this.a) {
                info.skewX = Math.atan(this.c / this.a);
                info.skewY = Math.atan(this.b / this.a);
                info.scaleX = this.a;
                info.scaleY = determinant / this.a;
            }
            else if (this.b) {
                info.skewX = Math.atan(this.d / this.b);
                info.scaleX = this.b;
                info.scaleY = determinant / this.b;
                info.rotation = Math.PI * 0.5;
            }
            else {
                info.skewX = Math.PI * 0.25;
                info.scaleX = this.c;
                info.scaleY = this.d;
            }
        }
        else {
            if (this.a || this.b) {
                const r = Math.sqrt(this.a * this.a + this.b * this.b);
                info.skewX = Math.atan((this.a * this.c + this.b * this.d) / (r * r));
                info.scaleX = r;
                info.scaleY = determinant / r;
                info.rotation = this.b > 0 ? Math.acos(this.a / r) : -Math.acos(this.a / r);
            }
            else if (this.c || this.d) {
                const s = Math.sqrt(this.c * this.c + this.d * this.d);
                info.rotation = Math.PI * 0.5 - (this.d > 0 ? Math.acos(-this.c / s) : -Math.acos(this.c / s));
                info.skewY = Math.atan((this.a * this.c + this.b * this.d) / (s * s));
                info.scaleX = determinant / s;
                info.scaleY = s;
            }
            else {
                info.scaleX = info.scaleY = 0;
            }
        }

        return info;
    }

    static rotation(theta: number, aboutPoint: Point | null = null) {
        let m = new Matrix();
        m.a = Math.cos(theta);
        m.b = Math.sin(theta);
        m.c = -Math.sin(theta);
        m.d = Math.cos(theta);
    
        if (aboutPoint) {
            m = Matrix.translation(aboutPoint.x, aboutPoint.y).concat(m).concat(
                Matrix.translation(-aboutPoint.x, -aboutPoint.y)
            );
        }
    
        return m;
    };

    static scale(sx: number, sy: number, aboutPoint: Point | null = null) {
        sy = sy || sx;

        let m = new Matrix();
        m.a = sx;
        m.b = 0;
        m.c = 0;
        m.d = sy;

        if (aboutPoint) {
            m = Matrix.translation(aboutPoint.x, aboutPoint.y).concat(m).concat(
                Matrix.translation(-aboutPoint.x, -aboutPoint.y)
            );
        }

        return m;
    };

    static translation(e: number, f: number) {
        const m = new Matrix();
        m.a = 1;
        m.b = 0;
        m.c = 0;
        m.d = 1;
        m.e = e;
        m.f = f;
        return m;
    };
};