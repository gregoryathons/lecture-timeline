import { Timeline, Action, Clip, Group, ActionType } from ".";
import Emitter from "./Emitter";
import { TimelineEvent, ElementType } from "./Const";
import { log } from "./Timeline";

// define the Track Element
class HTMLTrackElement extends HTMLElement {}
customElements.define(ElementType.Track, HTMLTrackElement);

interface IBookmark {
    name: string;
    time: number;
};

export default class Track extends Emitter {
    static id = 1;

    id = 0;
    timeline: Timeline;

    bookmarks: Array<IBookmark> = [];
    actions: Array<Action> = [];
    groups: Array<Group> = [];
    clips: Array<Clip> = [];
    currentClip: Clip | null;

    name = "";
    timeOffset = 0; // the time offset, this is for tracks NOT main to offset the main track
    time = 0; // current/last time

    constructor(data: any | null = null) {
        super();

        this.id = data?.id || Track.id++;
        this.name = data?.name;
        this.timeOffset = data?.timeOffset || this.timeOffset;
        this.bookmarks = data?.bookmarks || this.bookmarks;

        // create our element
        this._element = document.createElement(ElementType.Track);
        this._element.dataset.id = this.id.toString();

        // add the Actions
        data?.actions?.forEach((action: any) => {
            const type = action.type as keyof typeof ActionType;
            this.addAction(new ActionType[type](action));
        });

        // add the Groups
        data?.groups?.forEach((group: any) => this.addGroup(new Group(group)));

        // add the Clips
        data?.clips?.forEach((clip: any) => this.addClip(new Clip(clip)));

        // so we don't get duplicates
        if (this.id >= Track.id) {
            Track.id = this.id + 1;
        }
    }

    addTo(timeline: Timeline) {
        this.timeline = timeline;

        // append to
        this.timeline.appendChild(this._element);

        return this;
    }

    removeFrom() {
        // remove Groups
        while (this.groups.length) {
            this.removeGroup(this.groups.pop()!);
        }
        // remove Actions
        while (this.actions.length) {
            this.removeAction(this.actions.pop()!);
        }
        // remove Clips
        while (this.clips.length) {
            this.removeClip(this.clips.pop()!);
        }
        // remove element
        this._element.remove();
        // empty it
        this.currentClip = null;
        return this;
    }

    update() {
        // increment our timer for now to check when the next Clip will start
        this.time += this.timeline.deltaScale;

        if (!this.currentClip) {
            return;
        }

        const videoElement = this.timeline.getVideo();

        // will start the audio when it's time
        if (videoElement.paused && this.time >= this.currentClip.startTime && this.time < this.currentClip.endTime) {
            videoElement.play();
        }

        // set the time so it's actually in sync with the Video element's current time
        if (!videoElement.paused) {
            this.time = this.currentClip.startTime + videoElement.currentTime;
        }
        
        // check the Track's Actions
        this.actions.forEach(action => {
            // keep playing through the Actions
            if (this.timeline.isPlaying) {
                action.update(this.time, false);

                // Interaction had to have stopped it
                if (!this.timeline.isPlaying) {
                    this.time = action.startTime;
                }
            }
        });

        this.groups.forEach(group => {
            if (this.timeline.isPlaying) {
                group.update(this.time, false);
            }
        });

        // end of video, check if we need to load the next one
        if (videoElement.paused) {
            const index = this.clips.indexOf(this.currentClip);
            const nextClip = this.clips[index + 1];

            // there is another, so load it
            if (nextClip) {
                this.currentClip = nextClip;
                this.timeline.loadTrackClip(this);
            }
        }

        // done
        if (this.time > this.duration) {
            this.currentClip = null;
        }
    }

    jumpTo(time: number) {
        // empty everything if this is the first Track
        if (this.isMain) {
            // remove all the current elements
            Array.from(this.timeline.content.children).filter((kid: any) => kid.classList.contains("element")).forEach((kid: any) => kid.remove());
        }
        // offset the main timeline
        else {
            this.timeline.tracks[0].jumpTo(this.timeOffset);
        }

        this.actions.forEach(action => action.reset());
        this.groups.forEach(group => group.reset());

        this.actions.forEach(action => action.update(time, true));
        this.groups.forEach(group => group.update(time, true));

        // set time
        this.time = time;

        this.determineClip();
    }

    determineClip() {
        if (!this.clips.every(clip => clip.timeline)) {
            return;
        }

        // determine the audio we need to load
        let nextClip = this.clips.find(clip => this.time >= clip.startTime && this.time <= clip.endTime);

        // couldn't find a Clip, must be between 2 Clip. Get the next one
        if (!nextClip) {
            nextClip = this.clips.find(clip => clip.startTime >= this.time);
        }

        // still couldn't find a Clip, we must be at the end???
        if (!nextClip) {
            return;
        }

        // set as the current
        this.currentClip = nextClip;

        // load the audio from the Segment
        this.timeline.loadTrackClip(this, this.time - this.currentClip.startTime);
    }

    addClip(clip: Clip) {
        this.clips.push(clip.addTo(this));
        
        this.dispatchEvent(new CustomEvent(TimelineEvent.ClipAdded, { 
            bubbles: true,
            detail: {
                track: this,
                clip: clip 
            } 
        }));

        return clip;
    }

    removeClip(clip: Clip) {
        const index = this.clips.indexOf(clip.removeFrom());
        if (index >= 0) {
            this.clips.splice(index, 1);
        }

        if (this.currentClip === clip) {
            this.currentClip = null;
        }

        this.dispatchEvent(new CustomEvent(TimelineEvent.ClipRemoved, {
            bubbles: true,
            detail: {
                track: this,
                clip: clip 
            }
        }));

        return this;
    }

    getClip(id: number) {
        return this.clips.find(clip => clip.id === id) || null;
    }

    addAction(action: Action) {
        this.actions.push(action.addTo(this));

        this.dispatchEvent(new CustomEvent(TimelineEvent.ActionAdded, { 
            bubbles: true,
            detail: { 
                track: this,
                action: action 
            } 
        }));

        return action;
    }

    removeAction(action: Action) {
        const index = this.actions.indexOf(action.removeFrom());
        if (index >= 0) {
            this.actions.splice(index, 1);
        }

        this.dispatchEvent(new CustomEvent(TimelineEvent.ActionRemoved, {
            bubbles: true,
            detail: { 
                track: this,
                action: action
            } 
        }));

        return this;
    }

    getAction(id: number) {
        return this.actions.find(action => action.id === id) || null;
    }

    addGroup(group: Group) {
        this.groups.push(group.addTo(this));

        this.dispatchEvent(new CustomEvent(TimelineEvent.GroupAdded, { 
            bubbles: true,
            detail: { 
                track: this,
                group: group 
            } 
        }));

        return group;
    }

    removeGroup(group: Group) {
        const index = this.groups.indexOf(group.removeFrom());
        if (index >= 0) {
            this.groups.splice(index, 1);
        }

        this.dispatchEvent(new CustomEvent(TimelineEvent.GroupRemoved, {
            bubbles: true,
            detail: { 
                track: this,
                group: group
            } 
        }));

        return this;
    }

    getGroup(id: number) {
        return this.groups.find(group => group.id === id) || null;
    }

    addBookmark(name: string, time: number) {
        // create the object
        const bookmark = {
            name: name,
            time: time
        } as IBookmark;
        
        // add 
        this.bookmarks.push(bookmark);

        // tell everyone
        this.dispatchEvent(new CustomEvent(TimelineEvent.BookmarkAdded, {
            bubbles: true,
            detail: bookmark
        }));

        return bookmark;
    }

    removeBookmark(bookmark: IBookmark) {
        // remove from array
        const index = this.bookmarks.indexOf(bookmark);
        if (index >= 0) {
            this.bookmarks.splice(index, 1);
        }

        // tell everyone
        this.dispatchEvent(new CustomEvent(TimelineEvent.BookmarkRemoved, {
            bubbles: true,
            detail: bookmark
        }));

        return this;
    }
    
    // returns the end/last time
    get duration() {
        let length = 0;

        this.clips.forEach(clip => length = Math.max(length, clip.endTime));

        this.actions.forEach(action => length = Math.max(length, action.endTime));

        return length;
    }

    get isMain() {
        return this.timeline.tracks[0] === this;
    }

    // import/export

    export() {
        const track = { 
            id: this.id,
            name: this.name,
            timeOffset: this.timeOffset,
            bookmarks: this.bookmarks,
            actions: [] as any,
            groups: [] as any,
            clips: [] as any
        };
        this.actions.forEach(action => track.actions.push(action.export()));
        this.groups.forEach(group => track.groups.push(group.export()));
        this.clips.forEach(clip => track.clips.push(clip.export()));
        return track;
    }
}