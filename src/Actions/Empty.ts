import { TimelineEvent, Action } from "..";
import { trace } from "../Timeline";

export default class Empty extends Action {
    
    override onStart(direction: number) {
        if (direction <= 0) {
            return;
        }

        console.log("EMPTY");
        
        this.dispatchEvent(new CustomEvent(TimelineEvent.ActionTriggered, {
            bubbles: true,
            detail: {
                type: this.type
            }
        }));
    }

    override editorHTML(content: HTMLElement): void {
        content.innerHTML = "";
    }
}