// https://github.com/JDMCreator/Table.js

import ElementType from "./ElementType";
import AddElement from "../AddElement";

interface IMatrixCell {
    x: number;
    y: number;
    cols: number;
    rows: number;
    cell: HTMLTableCellElement;
    cellRef: IMatrixCell;
};

export default class Table extends ElementType {

    static maxIterations = 50;

    sampleTable: HTMLTableElement;
    defaultMatrix = [
        // 2x2
        [{ x: 0, y: 0, cols: 1, rows: 1, cell: null, cellRef: null }, { x: 0, y: 0, cols: 1, rows: 1, cell: null, cellRef: null }],
        [{ x: 0, y: 0, cols: 1, rows: 1, cell: null, cellRef: null }, { x: 0, y: 0, cols: 1, rows: 1, cell: null, cellRef: null }]
    ];

    constructor(action: AddElement) {
        super(action);

        const rect = this.action.rect;
        rect.width = rect.width || 50;
        rect.height = rect.height || 50;

        this.action.options.matrix = (this.action.options.matrix || this.defaultMatrix) as IMatrixCell;
    }

    createElement() {
        const element = document.createElement("table") as HTMLTableElement;

        const matrix = this.action.options.matrix;

        for (let i = 0; i < matrix.length; i++) {
            const tr = document.createElement("tr") as HTMLTableRowElement;
            tr.setAttribute("height", `${100 / matrix.length}%`);

            for (let j = 0; j < matrix[i].length; ) {
                let rows = matrix[i][j].rows;
                let cols = matrix[i][j].cols;

                if (rows || cols) {
                    const td = document.createElement("td") as HTMLTableCellElement;
                    if (rows > 1) { td.rowSpan = rows; }
                    if (cols > 1) { td.colSpan = cols; }
                    td.setAttribute("width", `${(cols / matrix[i].length) * 100}%`);
                    tr.appendChild(td);

                    j += cols;
                }
                else {
                    j++;
                }
            }
            element.appendChild(tr);
        }

        return element;
    }

    appendChild(otherAction: AddElement) {
        const tds = Array.from(this.action.element.querySelectorAll("td"));
        const cellIndex = otherAction.options.cellIndex;
        if (typeof cellIndex === "number" && tds[cellIndex]) {
            tds[cellIndex].appendChild(otherAction.element);
        }
    }

    editorHTML(content: HTMLElement) {
        content.innerHTML = `
            <div class="row table">
                <div class="nine columns offset-by-three">
                    <div>
                        <span class="three columns">Row</span>
                        <button class="three columns row-before">
                            <span class="material-icons">expand_less</span>
                        </button>
                        <button class="three columns row-after">
                            <span class="material-icons">expand_more</span>
                        </button>
                        <button class="three columns row-remove">
                            <span class="material-icons">close</span>
                        </button>
                    </div>
                    <div>
                        <span class="three columns">Col</span>
                        <button class="three columns col-before">
                            <span class="material-icons">chevron_left</span>
                        </button>
                        <button class="three columns col-after">
                            <span class="material-icons">chevron_right</span>
                        </button>
                        <button class="three columns col-remove">
                            <span class="material-icons">close</span>
                        </button>
                    </div>
                    <div>
                        <button class="six columns merge">Merge</button>
                        <button class="six columns split">Split</button>
                    </div>
                </div>
                <div class="nine columns offset-by-three" data-table-outline="true">
                    <!-- will be generated -->
                </div>
            </div>
        `;

        content.addEventListener("click", (event: MouseEvent) => {
            let target = event.target as HTMLElement;

            // admin clicked on the td
            let td = target.closest("td");
            if (td) {
                td.classList.toggle("selected");
            }
            // admin is manipulating the table
            else if (target.tagName.toLowerCase() === "button") {
                let cells = Array.from(tableOutline.querySelectorAll(".selected"));
                let first = cells.length ? cells[0] as HTMLTableCellElement : null;

                // rows
                if (target.classList.contains("row-before") && first) {
                    this.insertRow(this.position(first)!.y);
                }
                else if (target.classList.contains("row-after") && first) {
                    this.insertRow(this.position(first)!.y + first.rowSpan);
                }
                else if (target.classList.contains("row-remove") && first) {
                    this.removeRow(this.position(first)!.y);
                }
                // cols
                else if (target.classList.contains("col-before") && first) {
                    this.insertCol(this.position(first)!.x);
                }
                else if (target.classList.contains("col-after") && first) {
                    this.insertCol(this.position(first)!.x + first.colSpan);
                }
                else if (target.classList.contains("col-remove") && first) {
                    this.removeCol(this.position(first)!.x);
                }
                // merge
                else if (target.classList.contains("merge")) {
                    this.merge(cells as Array<HTMLTableCellElement>);
                }
                // split
                else if (target.classList.contains("split")) {
                    this.split(cells as Array<HTMLTableCellElement>);
                }

                // get the matrix
                this.action.options.matrix = JSON.parse(JSON.stringify(this.matrix()));
                content.dispatchEvent(new CustomEvent("input", { bubbles: true }));
            }
        });

        this.sampleTable = this.createElement() as HTMLTableElement;
        Array.from(this.sampleTable.querySelectorAll("tr")).forEach(tr => tr.removeAttribute("height"));
        Array.from(this.sampleTable.querySelectorAll("td")).forEach(td => td.removeAttribute("width"));
        this.sampleTable.classList.add("outline");

        const tableOutline = <HTMLElement> content.querySelector(`.row [data-table-outline]`);
        tableOutline.appendChild(this.sampleTable);
    }

    get table() {
        return this.sampleTable;//this.action.element;
    }

    isChildCell(cell: any) {
        if (!Table.isCell(cell)) {
            return false;
        }
        while (cell = cell.parentElement) {
            if (cell === this.table) {
                return true;
            }
            else if (Table.isCell(cell)) {
                return false;
            }
        }
        return false;
    }

    normalize(matrix: Array<Array<IMatrixCell>>) {
        matrix = matrix || this.matrix();
        // first, we adjust rowspan attribute
        let change = false;
        rowLoop: for (let i = 0; i < matrix.length; i++) {
            let row = matrix[i];
            for (let j = 0; j < row.length; j++) {
                let cell = row[j];
                if (!cell.cellRef) {
                    continue rowLoop;
                }
            }
            change = true;
            for (let j = 0; j < row.length; j++) {
                let cell = row[j];
                cell.cellRef.cell.rowSpan -= 1;
                j += cell.cellRef.cell.colSpan - 1;
            }
        };
        // calculate the max column
        let colLength = 0;
        for (let i = 0; i < matrix.length; i++) {
            colLength = Math.max(colLength, matrix[i].length);
        }
        // adjust colspan attribute
        colLoop: for (let j = 0; j < colLength; j++) {
            for (let i = 0; i < matrix.length; i++) {
                let cell = matrix[i][j];
                if (cell && !cell.cellRef) {
                    continue colLoop;
                }
            }
            change = true;
            for (let i = 0; i < matrix.length; i++) {
                let cell = matrix[i][j];
                if (cell) {
                    cell.cellRef.cell.colSpan -= 1;
                    i += cell.cellRef.cell.rowSpan - 1;
                }
            }
        };
        // let table = this.action.element as HTMLTableElement;
        let table = this.table;
        let rows = table.rows;
        // we remove all empty rows
        for (let i = 0; i < rows.length; i++) {
            let row = rows[i];
            if (row && row.cells.length <= 0) {
                row.remove(); // remove empty row
                i--;
                change = true;
            }
        }
        // refresh the matrix and the max length of rows
        // TODO : improve speed (maybe adjust the matrix on the fly?)
        if (change) {
            colLength = 0;
            matrix = this.matrix()
            for (let i = 0; i < matrix.length; i++) {
                colLength = Math.max(colLength, matrix[i].length);
            }
        }
        // finally, expand the colspan of cells if there are empty cells
        for (let i = 0; i < matrix.length; i++) {
            if (rows[i]) {
                let cells = rows[i].cells;
                if (matrix[i].length < colLength && cells && cells[0]) {
                    cells[cells.length - 1].colSpan += colLength - cells.length;
                    change = true;
                }
            }
        }
        return true;
    }

    removeCol(pos: number) {
        if (!pos && pos !== 0) {
            pos = -1;
        }
        let matrix = this.matrix();
        for (let i = 0; i < matrix.length; i++) {
            let pos2;
            let row = matrix[i];
            if (pos < 0) {
                pos2 = row.length + pos;
                if (pos2 < 0) {
                    pos2 = 0;
                }
            }
            else if (pos >= row.length) {
                pos2 = row.length - 1;
            }
            else {
                pos2 = pos;
            }
            let cell = row[pos2];
            if (cell) {
                i += (cell.cellRef || cell).cell.rowSpan - 1;
                if (cell.cellRef) {
                    cell.cellRef.cell.colSpan -= 1;
                }
                else {
                    if (cell.cell.colSpan > 1) {
                        cell.cell.colSpan -= 1;
                    }
                    else {
                        cell.cell.parentElement?.removeChild(cell.cell);
                    }
                }
            }
        }
    }

    removeRow(pos: number) {
        if (!pos && pos !== 0) {
            pos = -1;
        }
        // let table = this.action.element as HTMLTableElement;
        let table = this.table;
        let matrix = this.matrix();
        if (pos < 0) {
            pos = matrix.length + pos;
            if (pos < 0) {
                pos = 0;
            }
        }
        else if (pos >= matrix.length) {
            pos = matrix.length - 1;
        }
        let row = matrix[pos], nextrow = matrix[pos + 1];
        for (let i = 0; i < row.length; i++) {
            let cell = row[i];
            if (cell.cellRef) {
                if (cell.cellRef.cell.rowSpan > 1) {
                    cell.cellRef.cell.rowSpan -= 1;
                }
            }
            else if (cell.cell.rowSpan > 1 && nextrow) {
                if (cell.cell.rowSpan !== 0) {
                    cell.cell.rowSpan -= 1;
                }
                let before = nextrow[i].cell;
                for (let j = i; j < nextrow.length; j++) {
                    if (nextrow[j].cell) {
                        before = nextrow[j].cell;
                        j = nextrow.length;
                    }
                }
                table.rows[pos + 1].insertBefore(cell.cell, before);					
            }
            i += (cell.cellRef || cell).cell.colSpan - 1;
        }
        table.rows[pos].parentElement?.removeChild(table.rows[pos]);
    }

    insertCol(pos: number) {
        if (!pos && pos !== 0) {
            pos = -1;
        }
        // let table = this.action.element as HTMLTableElement;
        let table = this.table;
        let matrix = this.matrix();
        for (let i = 0; i < matrix.length; i++) {
            let pos2; 
            let row = matrix[i];
            if (pos < 0) {
                pos2 = row.length + pos + 1;
                if (pos2 < 0) {
                    pos2 = 0;
                }
            }
            else if (pos > row.length) {
                pos2 = row.length;
            }
            else {
                pos2 = pos;
            }
            let copycell = matrix[i][pos2 === 0 ? pos2 : pos2 - 1];
            let cell;
            let actualCell = matrix[i][pos2 - 1];
            if (actualCell && (actualCell.cellRef || actualCell.cell.colSpan !== 1)) {
                (actualCell.cellRef || actualCell).cell.colSpan += 1;
                i += (actualCell.cellRef || actualCell).cell.rowSpan - 1;
            }
            else {
                actualCell = matrix[i][pos2];
                let i2 = i;
                if (copycell) {
                    let cellOrig = copycell.cell || copycell.cellRef.cell;
                    cell = document.createElement(cellOrig.tagName) as HTMLTableCellElement;
                    cell.rowSpan = cellOrig.rowSpan;
                    i += cellOrig.rowSpan - 1;
                }
                else {
                    cell = document.createElement("td") as HTMLTableCellElement;
                }
                // if(callback){
                //     callback.call(this, cell);
                // }
                table.rows[i2].insertBefore(cell, actualCell ? actualCell.cell : null);
            }	
        }
    }

    insertRow(pos: number) {
        if (!pos && pos !== 0){
            pos = -1;
        }
        // let table = this.action.element as HTMLTableElement;
        let table = this.table;
        let tr = document.createElement("tr");
        let matrix = this.matrix();
        if (pos < 0) {
            pos = matrix.length + pos + 1;
            if (pos < 0) { 
                pos = 0
            }
        }
        else if (pos > matrix.length) {
            pos = matrix.length;
        }
        let copyrow = pos === 0 ? matrix[pos] : matrix[pos - 1];
        if (copyrow) {
            for (let i = 0; i < copyrow.length; i++) {
                let copycell = copyrow[i];
                if (copycell && (copycell.cellRef || copycell.cell.rowSpan != 1)) {
                    (copycell.cellRef || copycell).cell.rowSpan += 1;
                    i += (copycell.cellRef || copycell).cell.colSpan - 1;
                }
                else {
                    let cellOrig = copycell.cell || copycell.cellRef.cell;
                    let cell = document.createElement(cellOrig.tagName) as HTMLTableCellElement;
                    cell.colSpan = cellOrig.colSpan;
                    i += cellOrig.colSpan - 1;
                    // if (callback){callback.call(this,cell)}
                    tr.appendChild(cell);
                }
            }
        }
        else {
            let cell = document.createElement("td");
            // if(callback){callback.call(this,cell)}
            tr.appendChild(cell);
        }
        ((table.rows[0] || {}).parentElement || table).insertBefore(tr, table.rows[pos]);
    }

    split(cells: Array<HTMLTableCellElement>) {
        // let table = this.action.element as HTMLTableElement;
        let table = this.table;
        for (let i = 0; i < cells.length; i++) {
            let cell = cells[i];
            if (cell.rowSpan <= 1 && cell.colSpan <= 1) {
                continue;
            }
            let matrix = this.matrix();
            let pos = this.position(cell, matrix)!;
            let rows = new Array(cell.rowSpan);
            for (let n = 0; n < cell.rowSpan; n++) {
                rows[n] = document.createDocumentFragment();
                for (let m = 0; m < cell.colSpan; m++) {
                    if (!(n === 0 && m === 0)) {
                        let ncell = document.createElement(cell.tagName);
                        // if (callback) {
                        //     callback.call(this, ncell);
                        // }
                        rows[n].appendChild(ncell);
                    }
                }
            }
            cell.rowSpan = cell.colSpan = 1;
            for (let n = 0; n < rows.length; n++) {
                let row = matrix[pos.y + n], before = null;
                for (let m = pos.x + (n === 0 ? 1 : 0); m < row.length; m++) {
                    if (row[m].cell) {
                        before = row[m].cell;
                        m = row.length;
                    }
                }
                table.rows[pos.y + n].insertBefore(rows[n], before);
            }
        }
    }

    merge(cells: Array<HTMLTableCellElement>) {
        let h, v, returnValue = false;
        for (let i = 0; i < Table.maxIterations; i++) {
            v = this._mergeVertical(cells);
            h = this._mergeHorizontal(cells);
            if (v || h) {
                returnValue = true;
            }
            else if (!v && !h) {
                break;
            }
        }
        return returnValue;
    }

    _mergeVertical(cells: Array<HTMLTableCellElement>) {
        // cells = ensureArray(cells);
        // if(cells.length<=1){
        //     return false;
        // }
        let matrix = this.matrix();
        let returnValue = false;

        for (let i = 0; i < cells.length; i++) {
            // Build group
            let cell = cells[i];
            let group: Array<HTMLTableCellElement> = [];
            let length = cell.colSpan;
            let finalRowSpan = cell.rowSpan;

            if (this.isChildCell(cell)) {
                var pos = this.position(cell, matrix);
                if (!pos) {
                    continue;
                }

                for (let j = pos.y + cell.rowSpan; j < matrix.length; j++) {
                    let nextgroup = [];
                    let subheight = 0;
                    let subrow = matrix[j];
                    let sublength = 0;
                    for (let h = pos.x; h < subrow.length; h++) {
                        let subcell = subrow[h].cell;
                        sublength += (subcell || {}).colSpan || 1;
                        subheight = subheight === 0 ? (subcell || {}).rowSpan : subheight;
                        if (subcell && cells.indexOf(subcell) >= 0 && sublength <= length && subcell.rowSpan === subheight) {
                            nextgroup.push(subcell);
                            if (length === sublength) {
                                group = group.concat(nextgroup);
                                finalRowSpan += subheight;
                                h = subrow.length; // Get out of the loop
                            }
                            h += subcell.colSpan - 1;
                            j += subcell.rowSpan - 1;
                        }
                        else {
                            // End of the group. We have to get out of two loops
                            h = subrow.length;
                            j = matrix.length;
                        }
                    }
                }
            }
            if (group.length > 0) {
                // if (callback) {
                //     callback.call(	this,
                //             length,		// colSpan
                //             finalRowSpan,	// rowSpan
                //             cell,		// Cell that will be kept
                //             group		// An array that will be removed
                //     )
                // }
                for (let j = 0; j < group.length; j++) {
                    group[j].remove();// parentElement.removeChild(group[j]);
                }
                cell.rowSpan = finalRowSpan;
                cell.colSpan = length;
                returnValue = true;
            }
        }
        return returnValue;
    }

    _mergeHorizontal(cells: Array<HTMLTableCellElement>){
        // cells = ensureArray(cells);
        // if(cells.length<=1){
        //     return false;
        // }
        let matrix = this.matrix();
        let returnValue = false;

        // start horizontal group
        for (let i = 0; i < cells.length; i++) {
            let cell = cells[i];
            let group: Array<HTMLTableCellElement> = [];
            let height = cell.rowSpan;
            let finalColSpan = cell.colSpan;

            if (this.isChildCell(cell)) {
                let pos = this.position(cell, matrix);
                if (!pos) {
                    continue;
                }
                let subrow = matrix[pos.y];
                for (let j = pos.x + cell.colSpan; j < subrow.length; j++) {
                    let nextgroup: Array<HTMLTableCellElement> = [];
                    let sublength = 0; 
                    let subheight = 0;
                    for (let h = pos.y; h < matrix.length; h++) {
                        let subcell = matrix[h][j].cell;
                        subheight += (subcell || {}).rowSpan || 1;
                        sublength = sublength === 0 ? (subcell || {}).colSpan : sublength;
                        if (subcell && cells.indexOf(subcell) >= 0 && subheight <= height && subcell.colSpan === sublength) {
                            nextgroup.push(subcell);
                            if (height === subheight) {
                                group = group.concat(nextgroup);
                                finalColSpan += sublength;
                                h = matrix.length; // get out of the loop
                            }
                            h += subcell.rowSpan - 1;
                            j += subcell.colSpan - 1;
                        }
                        else {
                            // end of the group, need get out of two loops
                            h = matrix.length;
                            j = subrow.length;
                        }
                    }
                }
            }
            if (group.length > 0) {
                // if(callback){
                //     callback.call(	this,
                //             finalColSpan,	// colSpan
                //             height,		// rowSpan
                //             cell,		// Cell that will be kept
                //             group		// An array that will be removed
                //     )
                // }
                for (let j = 0; j < group.length; j++) {
                    group[j].remove();//parentElement.removeChild(group[j]);
                }
                cell.rowSpan = height;
                cell.colSpan = finalColSpan;
                returnValue = true;
            }
        }
        return returnValue;
    }

    position(cell: HTMLTableCellElement, matrix: Array<Array<IMatrixCell>> | null = null) {
        matrix = matrix || this.matrix();
        for (let i = 0; i < matrix.length; i++) {
            let row = matrix[i];
            for (let j = 0; j < row.length; j++) {
                let cellO = row[j]
                if (cellO && cellO.cell === cell) {
                    return { x: cellO.x, y: cellO.y } as IMatrixCell;
                }
            }
        }
        return null;
    }

    matrix() {
        // let table = this.action.element as HTMLTableElement;
        let table = this.table;
        let matrix: Array<Array<IMatrixCell>> = [];
        let expandCells: Array<IMatrixCell> = [];
        let rows = table.rows;

        for (let i = 0; i < rows.length; i++) {
            matrix.push([]);
        }

        for (let i = 0; i < rows.length; i++) {
            let row = rows[i];
            let realCol = 0;

            for (let j = 0; j < row.cells.length; j++) {
                let cell = row.cells[j] as HTMLTableCellElement;

                if (typeof matrix[i][realCol] !== "object") {
                    let rowSpan = Math.floor(Math.abs(isNaN(cell.rowSpan) ? 1 : cell.rowSpan));

                    if (rowSpan === 0 && cell.ownerDocument && cell.ownerDocument.compatMode === "BackCompat") {
                        rowSpan = 1;
                    }

                    if (rowSpan === 1) {
                        if (!cell.colSpan || cell.colSpan < 2) {
                            matrix[i][realCol] = { x: realCol, y: i, cell: cell } as IMatrixCell;
                        }
                        else {
                            let o = matrix[i][realCol] = { x: realCol, y: i, cell: cell } as IMatrixCell;
                            for (let k = 1; k < cell.colSpan; k++) {
                                matrix[i][realCol + k] = { x: realCol + k, y: i, cellRef: o } as IMatrixCell;
                            }
                        }
                    }
                    else {
                        let o = matrix[i][realCol] = { x: realCol, y: i, cell: cell } as IMatrixCell;
                        if (rowSpan === 0) {
                            expandCells.push(o);
                        }
                        // four-level loop
                        for (let n = 0, nl = Math.max(rowSpan, 1); n < nl; n++) {
                            for (let m = 0; m < cell.colSpan; m++) {
                                if (!(n === 0 && m === 0)) {
                                    let o2 = matrix[i + n][realCol + m] = { x: realCol + m, y: i + n, cellRef: o } as IMatrixCell;
                                    if (rowSpan === 0) {
                                        expandCells.push(o2);
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    j--;
                }
                realCol++;
            }
        }

        if (expandCells.length) {
            for (let i = 0; i < expandCells.length; i++) {
                let expandCell = expandCells[i];
                let x = expandCell.x;
                let y = expandCell.y;
                for (let j = y + 1; j < matrix.length; j++) {
                    matrix[j].splice(x, 0, { x: x, y: j, cellRef: expandCell.cellRef || expandCell } as IMatrixCell);
                    for (let h = x + 1; h < matrix[j].length; h++) {
                        matrix[j][h].x++;
                    }
                }
            }
        }

        // set the cols and rows
        for (let i = 0; i < matrix.length; i++) {
        	for (let j = 0; j < matrix[i].length; j++) {
                let cell = matrix[i][j];
                if (cell && cell.cell) {
                    matrix[i][j].cols = cell.cell.colSpan;
                    matrix[i][j].rows = cell.cell.rowSpan;
                }
            }
        }

        return matrix;
    }

    static isCell(cell: any) {
        return cell instanceof HTMLTableCellElement;
    }
};