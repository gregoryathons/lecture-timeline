import ElementType from "./ElementType";
import AddElement from "../AddElement";
import { breakApartText } from "../utils";

export default class Text extends ElementType {
    hasText = true;

    constructor(action: AddElement) {
        super(action);

        const rect = this.action.rect;
        rect.width = rect.width || 0;
        rect.height = 0; // auto
    }
    
    createElement() {
        const element = document.createElement("p") as HTMLElement;
        element.innerHTML = breakApartText((this.action.options.innerText || "").toString());
        element.style.textAlign = (this.action.options.textAlign || "left").toString();
        return element;
    }

    editorHTML(content: HTMLElement) {
        const options = this.action.options;

        content.innerHTML = `
            <div class="row">
                <div class="three columns">
                    Text
                </div>
                <div class="nine columns">
                    <textarea name="inner-text" class="u-full-width" placeholder="Hello World">${options.innerText || ""}</textarea>
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Alignment
                </div>
                <div class="nine columns">
                    <select name="text-align" class="u-full-width">
                        <option value="left" ${options.textAlign === "left" || !options.textAlign ? "selected" : ""}>Left</option>
                        <option value="center" ${options.textAlign === "center" ? "selected" : ""}>Center</option>
                        <option value="right" ${options.textAlign === "right" ? "selected" : ""}>Right</option>
                        <option value="justify" ${options.textAlign === "justify" ? "selected" : ""}>Justify</option>
                    </select>
                </div>
            </div>
        `;

        const inputInnerText = <HTMLTextAreaElement> content.querySelector(`textarea[name="inner-text"]`);
        const selectTextAlign = <HTMLSelectElement> content.querySelector(`select[name="text-align"]`);
        
        inputInnerText.addEventListener("input", () => {
            options.innerText = inputInnerText.value;
        });
        selectTextAlign.addEventListener("input", () => {
            options.textAlign = selectTextAlign.value;
        });
    }
};