import ElementType from "./ElementType";
import AddElement from "../AddElement";

export default class SVG extends ElementType {
    constructor(action: AddElement) {
        super(action);

        const rect = this.action.rect;
        if (!rect.width && !rect.height) {
            rect.height = 25;
        }
        rect.width = 0; // auto
    }

    createElement() {
        const element = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        element.setAttributeNS(null, "viewBox", "0 0 100 100");
        element.setAttributeNS(null, "preserveAspectRatio", "xMinYMin meet");
        element.innerHTML = `
            <defs>
                <style type="text/css"><![CDATA[
                    path {fill-rule: evenodd;}
                    .a {fill: #A6A6A6;}
                    .b {fill: #D9D9D9;}
                ]]></style>
            </defs>
            <path class="a" d="M88.8,100H11.2C5,100,0,95,0,88.8V11.2C0,5,5,0,11.2,0h77.7C95,0,100,5,100,11.2v77.7C100,95,95,100,88.8,100z" />
            <path class="b" d="M11.2,2.9c-4.6,0-8.3,3.7-8.3,8.3v77.7c0,4.6,3.7,8.3,8.3,8.3h77.7c4.6,0,8.3-3.7,8.3-8.3V11.2c0-4.6-3.7-8.3-8.3-8.3H11.2z" />
            <path class="a" d="M54.1,50l37-37c1.1-1.1,1.1-3,0-4.1c-1.1-1.1-3-1.1-4.1,0l-37,37l-37-37c-1.1-1.1-3-1.1-4.1,0c-1.1,1.1-1.1,3,0,4.1l37,37l-37,37c-1.1,1.1-1.1,3,0,4.1C9.5,91.7,10.2,92,11,92c0.7,0,1.5-0.3,2.1-0.9l37-37l37,37c0.6,0.6,1.3,0.9,2.1,0.9c0.7,0,1.5-0.3,2.1-0.9c1.1-1.1,1.1-3,0-4.1L54.1,50z" />
        `;

        return element as SVGElement;
    }

    editorHTML(content: HTMLElement) {
        content.innerHTML = "";
    }
};