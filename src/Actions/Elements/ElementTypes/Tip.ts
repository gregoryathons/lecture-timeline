import ElementType from "./ElementType";
import AddElement from "../AddElement";
import { breakApartText } from "../utils";

export default class Tip extends ElementType {
    hasText = true;

    get textElement() {
        return this.action.element.querySelector(".text") as HTMLElement;
    }

    constructor(action: AddElement) {
        super(action);

        const rect = this.action.rect;
        rect.width = rect.width || 50;
        rect.height = rect.height || 25;
    }

    createElement() {
        const element = document.createElement("div") as HTMLImageElement;
        element.classList.add("tip");

        let url = "";
        if (this.action.resourceNames.length) {
            url = this.action.timeline.loader.getResource(this.action.resourceNames[0]).dataURL;
        }

        element.innerHTML = `
            <div class="context">
                <div class="image" style="background-image: url(${url})"></div>
                <div class="text">${breakApartText((this.action.options.innerText || "").toString())}</div>
            </div>
        `;

        return element;
    }

    editorHTML(content: HTMLElement) {
        content.innerHTML = `
            <div class="row">
                <div class="three columns">
                    Icon
                </div>
                <div class="nine columns">
                    <select name="resource" class="u-full-width"></select>
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Text
                </div>
                <div class="nine columns">
                    <textarea name="inner-text" class="u-full-width" placeholder="Hello World">${this.action.options.innerText || ""}</textarea>
                </div>
            </div>
        `;

        const selectResource = <HTMLSelectElement> content.querySelector(`select[name="resource"]`);
        const inputInnerText = <HTMLTextAreaElement> content.querySelector(`textarea[name="inner-text"]`);

        this.action.timeline.loader.getResources("images").forEach(resource => {
            const option = document.createElement("option");
            option.value = resource.name.toString();
            option.text = resource.title;
            option.selected = resource.name === this.action.resourceNames[0];
            selectResource.options.add(option);
        });

        selectResource.addEventListener("input", () => {
            this.action.resourceNames[0] = selectResource.value;
        });
        inputInnerText.addEventListener("input", () => {
            this.action.options.innerText = inputInnerText.value;
        });

        selectResource.dispatchEvent(new CustomEvent("input"));
    }
};