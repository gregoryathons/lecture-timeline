import Text from "./Text";
import { breakApartText } from "../utils";

export default class Header extends Text {
    createElement() {
        const element = document.createElement("h1") as HTMLElement;
        element.innerHTML = breakApartText((this.action.options.innerText || "").toString());
        element.style.textAlign = (this.action.options.textAlign || "left").toString();
        return element;
    }
};