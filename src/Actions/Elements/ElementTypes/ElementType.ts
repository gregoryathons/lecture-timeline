import AddElement from "../AddElement";

export default class ElementType {
    action: AddElement;
    hasText = false;

    get textElement() {
        return this.action.element as HTMLElement;
    }

    constructor(action: AddElement) {
        this.action = action;
    }

    createElement() {
        return document.createElement("div") as HTMLElement | SVGElement;
    }

    appendChild(otherAction: AddElement) {
        this.action.element.appendChild(otherAction.element);
    }

    editorHTML(content: HTMLElement) {}
};