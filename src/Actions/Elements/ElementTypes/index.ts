import AddElement from "../AddElement";
import ElementType from "./ElementType";
import Text from "./Text";
import Header from "./Header";
import Image from "./Image";
import SVG from "./SVG";
import Tip from "./Tip";
import Table from "./Table";

export default ElementType;

export const elementTypes = [
    Text,
    Header,
    Image,
    SVG,
    Tip,
    Table
];

export function getElementTypeInstance(action: AddElement) {
    switch (action.elementType) {
        case "Text": return new Text(action);
        case "Header": return new Header(action);
        case "Image": return new Image(action);
        case "SVG": return new SVG(action);
        case "Tip": return new Tip(action);
        case "Table": return new Table(action);
    }
    throw "Improper Element Type!";
};