import ElementType from "./ElementType";
import AddElement from "../AddElement";

export default class Image extends ElementType {
    constructor(action: AddElement) {
        super(action);

        const rect = this.action.rect;
        if (!rect.width && !rect.height) {
            rect.height = 50;
        }
        rect.width = 0; // auto
    }

    createElement() {
        const element = document.createElement("img") as HTMLImageElement;

        // set the source
        if (this.action.resourceNames.length) {
            element.src = this.action.timeline.loader.getResource(this.action.resourceNames[0]).dataURL;
        }

        return element;
    }

    editorHTML(content: HTMLElement) {
        content.innerHTML = `
            <div class="row">
                <div class="three columns">
                    Image
                </div>
                <div class="nine columns">
                    <select name="resource" class="u-full-width"></select>
                </div>
            </div>
        `;

        const selectResource = <HTMLSelectElement> content.querySelector(`select[name="resource"]`);
        this.action.timeline.loader.getResources("images").forEach(resource => {
            const option = document.createElement("option");
            option.value = resource.name.toString();
            option.text = resource.title;
            option.selected = resource.name === this.action.resourceNames[0];
            selectResource.options.add(option);
        });

        selectResource.addEventListener("input", () => {
            this.action.resourceNames[0] = selectResource.value;
        });

        selectResource.dispatchEvent(new CustomEvent("input"));
    }
};