import { Action, Track, Clip } from "../../";
import AddElement from "./AddElement";
import { createActionsDropDown, createVisibleDropDown, getActions, validActionRelations } from "../utils";
import { log, trace } from "../../Timeline";
import { breakApartText } from "./utils";

export default class UpdateText extends Action {

    otherIds: Array<number> = []; // the element we are changing
    oldText = ""; // old text/html
    newText = ""; // new text

    constructor(data: any | null = null) {
        super(data);

        this.otherIds = data?.otherIds || this.otherIds;
        this.newText = data?.newText || this.newText;
    }

    // override onBefore() {
    //     const otherAction = this.timeline.getAction(this.otherIds[0]) as unknown as AddElement;
    //     if (otherAction && this.oldText) {
    //         otherAction.getTextElement!.innerHTML = this.oldText; // html
    //         this.oldText = "";
    //     }
    // }

    override reset() {
        this.oldText = "";
        super.reset();
    }

    override onStart() {
        const otherAction = this.timeline.getAction(this.otherIds[0]) as unknown as AddElement;
        if (otherAction) {
            
            // remember the old text
            const otherElement = otherAction.getTextElement!;
            
            // save the html
            this.oldText = otherElement.innerHTML;

            // update to new text
            otherElement.innerHTML = breakApartText(this.newText);
        }
    }

    override editorHTML(content: HTMLElement) {
        // markup
        content.innerHTML = `
            <hr />
            <div class="row">
                <div class="three columns">
                    Element
                </div>
                <div class="nine columns">
                    <select name="other-ids" class="u-full-width"></select>
                </div>
            </div>
            <div class="row" data-for="action-id" style="display: none">
                <div class="three columns">
                    Existing
                </div>
                <div class="nine columns">
                    <textarea name="old-text" class="u-full-width" readonly></textarea>
                </div>
            </div>
            <div class="row" data-for="action-id" style="display: none">
                <div class="three columns">
                    Updated
                </div>
                <div class="nine columns">
                    <textarea name="new-text" class="u-full-width">${this.newText}</textarea>
                </div>
            </div>
        `;

        // reference to the other Action highlighted
        let otherAction: AddElement | null = null;

        // list out the types available
        const selectAction = <HTMLSelectElement> content.querySelector(`select[name="other-ids"]`);
        createVisibleDropDown(this, selectAction, (action: Action) => {
            if (!(action as AddElement).hasText) {
                return -1;
            }
            return this.otherIds.indexOf(action.id) >= 0 ? 1 : 0;
        });
        
        // keep reference to the Action id when admin updates
        selectAction.addEventListener("input", () => {
            if (selectAction.selectedIndex < 0) {
                return;
            }

            const otherId = parseInt(selectAction.value);

            // set reference to the otherAction
            otherAction = this.timeline.getAction(otherId) as unknown as AddElement;

            // show the row(s) if we have a valid Action
            content.querySelectorAll(".row[data-for]").forEach(el => {
                (el as HTMLElement).style.display = otherAction ? "" : "none";
            });

            // this is our output row, show the text
            if (otherAction) {
                // this.onBefore();
                if (this.oldText) {
                    otherAction.getTextElement.innerHTML = this.oldText; // html
                }

                // show the text
                const oldText = content.querySelector(`textarea[name="old-text"]`) as HTMLTextAreaElement;                
                oldText.value = otherAction.getTextElement.innerText;
            }

            // finally set the otherId property, do this now because `onBefore` is being called
            this.otherIds[0] = otherId;
        });

        // new text
        const inputNewText = <HTMLTextAreaElement> content.querySelector(`textarea[name="new-text"]`);
        inputNewText.addEventListener("input", (event) => {
            this.newText = inputNewText.value;
            // this.onBefore();
            if (otherAction && this.oldText) {
                otherAction.getTextElement.innerHTML = this.oldText; // html
            }
            this.onStart();
        });

        // emit it
        selectAction.dispatchEvent(new CustomEvent("input"));
    }

    override get isValid(): boolean {
        return this.otherIds.length > 0 && validActionRelations(this, this.otherIds);
    }
    
    override export() {
        return Object.assign(super.export(), {
            otherIds: this.otherIds,
            newText: this.newText
        });
    }
}