import AddElement from "./AddElement";
import { Action, Track } from "../..";
import { Ease } from "../../Ease";
import Matrix from "../../Matrix";
import { log, trace } from "../../Timeline";
import { lerp, toDeg } from "../../utils";
import { createDurationsDropDown, createEaseDropDown, createVisibleDropDown, getActions, validActionRelations } from "../utils";


// the values we are going to grab and store
const styleValues = ["top", "left", "width", "height", "opacity"];
const transformValues = ["translateX", "translateY", "scaleX", "scaleY", "skewX", "skewY", "rotation"];
const allValues = [...styleValues, ...transformValues];

// the properties when getting/setting from the element
const styleProperties = ["top", "left", "width", "height", "opacity"];
const transformProperties = ["transform"];
const allProperties = [...styleProperties, ...transformProperties];

const defaultValues: {[name: string]: number} = {
    top: 0,
    left: 0,
    width: Infinity,
    height: Infinity,
    opacity: 1,
    translateX: 0,
    translateY: 0,
    scaleX: 1,
    scaleY: 1,
    skewX: 0,
    skewY: 0,
    rotation: 0
};

// our AddElement Action class
export default class TweenElement extends Action {

    otherIds: Array<number> = [];
    otherActions: Array<Action> = [];
    easeType = "Linear";
    startValues: {[name: string]: number} = {};
    endValues: {[name: string]: number} = {};

    constructor(data: any | null = null) {
        super(data);

        this.otherIds = data?.otherIds || this.otherIds;
        this.easeType = data?.easeType || this.easeType;
        this.endValues = data?.endValues || this.endValues;
    }

    // override onBefore() {
    override reset() {        
        // done with it
        this.otherActions.length = 0;
        this.startValues = {}; // empty
        super.reset();
    }

    override onStart(direction: number) {
        if (!this.otherIds.length) {
            return;
        }

        if (!this.otherActions.length) {
            this.otherActions = getActions(this.timeline, this.otherIds);// this.timeline.getAction(this.otherId);

            // if the direction is moving forward, or the admin moved to this point - we set startValues 
            this.otherActions.forEach(otherAction => {
                this.setStartEndValues(otherAction as AddElement, direction >= 0);
                // console.log("set start", direction >= 0);
            });
        }

        // for the admin, show the end result
        if (direction === 0 && this.percentage === 0) {
            this.onEnd();
        }
    }

    override onUpdate() {
        this.otherActions.forEach(otherAction => {
            this.updateProperties((otherAction as AddElement).element, this.percentage01);
        });
    }

    override onEnd() {
        this.otherActions.forEach(otherAction => {
            this.updateProperties((otherAction as AddElement).element, 1);
        });
    }

    override editorHTML(content: HTMLElement) {
        // markup
        content.innerHTML = `
            <hr />
            <div class="row">
                <div class="three columns">
                    Element
                </div>
                <div class="nine columns">
                    <select name="other-id" class="u-full-width"></select>
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Speed
                </div>
                <div class="nine columns">
                    <select name="duration" class="u-full-width"></select>
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Ease Type
                </div>
                <div class="nine columns">
                    <select name="ease-type" class="u-full-width"></select>
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Scale X/Y
                </div>
                <div class="nine columns">
                    <input type="number" name="scaleX" class="u-half-width" />
                    <input type="number" name="scaleY" class="u-half-width" />
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Offset X/Y
                </div>
                <div class="nine columns">
                    <input type="number" name="translateX" class="u-half-width" />
                    <input type="number" name="translateY" class="u-half-width" />
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Skew X/Y
                </div>
                <div class="nine columns">
                    <input type="number" name="skewX" class="u-half-width" />
                    <input type="number" name="skewY" class="u-half-width" />
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Opacity
                </div>
                <div class="nine columns">
                    <input type="range" name="opacity" min="0" max="1" step="0.01" class="u-full-width" />
                </div>
            </div>
        `;

        // fill in the actions
        const selectAction = <HTMLSelectElement> content.querySelector(`select[name="other-id"]`);
        createVisibleDropDown(this, selectAction, (action: Action) => {
            return this.otherIds.indexOf(action.id) >= 0 ? 1 : 0;
        });

        // keep reference to the Action id
        selectAction.addEventListener("input", () => {
            if (selectAction.selectedIndex < 0) {
                return;
            }

            const newOtherId = parseInt(selectAction.value);

            // changed the Action
            if (this.otherIds[0] !== newOtherId) {
                this.otherIds[0] = newOtherId;
                
                // find other element
                getActions(this.timeline, this.otherIds).forEach(otherAction => {
                    // set the new values based of current state
                    this.setStartEndValues(otherAction as AddElement, true, true);
                    this.resetInputValues(content, this.startValues);
                });
            }
            // existing Action
            else {
                const otherActions = getActions(this.timeline, this.otherIds);
                if (otherActions.length) {
                    this.resetInputValues(content, this.endValues);
                }
            }
        });

        // show the fade times
        const selectDuration = <HTMLSelectElement> content.querySelector(`select[name="duration"]`);
        createDurationsDropDown(this, selectDuration);

        // ease type
        const selectEaseType = <HTMLSelectElement> content.querySelector(`select[name="ease-type"]`);
        createEaseDropDown(this, selectEaseType);

        selectEaseType.addEventListener("input", () => {
            this.easeType = selectEaseType.value;
        });

        // add listeners to inputs
        content.querySelectorAll(`select, input`).forEach(element => {
            if (element === selectAction || element === selectDuration || element === selectEaseType) {
                return;
            }

            // set the `endValues`
            element.addEventListener("input", (event) => {
                const name = element.getAttribute("name") || "";
                const value = element instanceof HTMLInputElement || element instanceof HTMLSelectElement ? element.value : "0";
                this.endValues[name] = parseFloat(value);

                // updates it locally
                getActions(this.timeline, this.otherIds).forEach(otherAction => {
                    this.updateProperties((otherAction as AddElement).element, 1);
                });
            });
        });

        // // clamp number length
        // // https://stackoverflow.com/questions/18510845/maxlength-ignored-for-input-type-number-in-chrome
        // content.querySelectorAll(`input[type="number"]`).forEach(el => {
        //     const element = el as HTMLInputElement;
        //     element.addEventListener("keypress", () => {
        //         if (element.value.length >= 4) {
        //             return false;
        //         }
        //     });
        // });

        // dispatch the events
        selectAction.dispatchEvent(new CustomEvent("input"));
    }

    resetInputValues(content: HTMLElement, values: {[name: string]: number}) {
        Object.keys(values).forEach(value => {
            const element = content.querySelector(`input[name="${value}"]`) as HTMLInputElement;
            if (element) {
                element.value = values[value].toString();
            }
        });
    }

    setStartEndValues(otherAction: AddElement | null = null, setStartValues: boolean = false, setEndValues: boolean = false) {
        const element = otherAction ? otherAction.element : null;

        // get all the values we will be using
        allValues.forEach(value => {
            // set startValue if not set
            if (typeof this.startValues[value] === "undefined" || setStartValues) {
                let val = "";
                if (element) {
                    if (styleValues.indexOf(value) > -1) {
                        val = element.style.getPropertyValue(value);
                    }
                    else {
                        val = this.getTransformValue(element, value).toString();
                    }
                }
                this.startValues[value] = val ? parseFloat(val) : defaultValues[value];
            }

            // set endValue if not set
            if (typeof this.endValues[value] === "undefined" || setEndValues) {
                this.endValues[value] = this.startValues[value];
            }
        });
    }

    updateProperties(element: HTMLElement | SVGElement | undefined, percentage: number) {
        if (!element) {
            return;
        }
        allProperties.forEach(property => {
            if (transformProperties.indexOf(property) > -1) {
                this.setTransformValues(element, percentage);
            }
            else {
                this.setStyleValue(element, property, this.startValues[property], this.endValues[property], percentage);
            }
        });
    }

    setStyleValue(element: HTMLElement | SVGElement, prop: string, start: number, end: number, percentage: number) {
        if (typeof start !== "number" || typeof end !== "number") {
            return;
        }

        const easeFunc: Function = Ease[this.easeType as keyof typeof Ease];
        const value = lerp(start, end, easeFunc(percentage));

        if (!isNaN(value) && value > -Infinity && value < Infinity) {
            // trace("set style value", prop, value);
            element.style.setProperty(prop, `${value}${prop !== "opacity" ? "%" : ""}`);
        }
    }

    setTransformValues(element: HTMLElement | SVGElement, percentage: number) {
        const easeFunc: Function = Ease[this.easeType as keyof typeof Ease];
        const sv = this.startValues;
        const ev = this.endValues;

        const translateX = lerp(sv.translateX, ev.translateX, easeFunc(percentage));
        const translateY = lerp(sv.translateY, ev.translateY, easeFunc(percentage));
        const scaleX = lerp(sv.scaleX, ev.scaleX, easeFunc(percentage));
        const scaleY = lerp(sv.scaleY, ev.scaleY, easeFunc(percentage));
        const skewX = lerp(sv.skewX, ev.skewX, easeFunc(percentage));
        const skewY = lerp(sv.skewY, ev.skewY, easeFunc(percentage));
        const rotation = lerp(sv.rotation, ev.rotation, easeFunc(percentage));

        // set the matrix and rotation
        const transformValue = `matrix(${scaleX}, ${skewX}, ${skewY}, ${scaleY}, ${translateX}, ${translateY}) rotate(${rotation}deg)`;

        // apply the style
        element.style.transform = transformValue;
    }

    getTransformValue(element: HTMLElement | SVGElement, prop: string) {
        const matrix = new Matrix(element.style.transform);
        const info = matrix.decompose();

        // @ts-ignore
        return prop === "rotation" ? toDeg(info[prop]) : info[prop];
    }

    // set's the `endValues` - called via Editor
    setRectAndMatrix(rect: DOMRect, matrix: DOMMatrix): boolean {
        if (this.percentage === 0 || this.percentage >= 1) {
            const m = new Matrix(matrix.toString());
            const info = m.decompose();

            this.endValues = {
                left: rect.x,
                top: rect.y,
                width: rect.width,
                height: rect.height,
                translateX: info.translateX,
                translateY: info.translateY,
                scaleX: info.scaleX,
                scaleY: info.scaleY,
                skewX: info.skewX,
                skewY: info.skewY,
                rotation: toDeg(info.rotation)
            };

            return true;
        }

        return false;
    }

    override get isValid(): boolean {
        return this.otherIds.length > 0 && validActionRelations(this, this.otherIds);
    }

    override export() {
        return Object.assign(super.export(), {
            otherIds: this.otherIds,
            easeType: this.easeType,
            endValues: this.endValues
        });
    }
}