export function breakApartText(text: string) {
    let output = "";

    const lines = (text || "").split(/\n/g);
    lines.forEach((line, lineIndex) => {
        // break the words
        const words = line.split(/\s/g);
        words.forEach((word, wordIndex) => {
            // any letters?
            const letters = word.split("");
            // break it down by letters
            output += `<span class="word">`;
            letters.forEach(letter => {
                output += `<span class="letter">${letter}</span>`;
            });
            output += `</span>`;

            // add a space
            if (wordIndex < words.length - 1) {
                output += ` `;
            }
        });
        
        if (lineIndex < lines.length - 1) { 
            output += `<br />`;
        }
    });

    return output;
};