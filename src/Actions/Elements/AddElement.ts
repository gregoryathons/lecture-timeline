import { Action } from "../../";
import Matrix from "../../Matrix";
import { log, trace } from "../../Timeline";
import { createVisibleDropDown } from "../utils";
import ElementType, { getElementTypeInstance } from "./ElementTypes/index";


// our AddElement Action class
export default class AddElement extends Action {
    _locked = true;
    _hidden = true;
    
    element: HTMLElement | SVGElement;
    elementType = "Text";
    elementTypeInstance: ElementType;
    otherIds: Array<number> = []; // if it belongs to another AddElement instance
    resourceNames: Array<string> = []; // name of the Resource, if any
    options: {[name: string]: any} = {}; // string | number | Array<string | number>
    rect: DOMRect = <DOMRect> {};
    matrix: DOMMatrix = <DOMMatrix> {};

    get hasText() {
        return this.elementTypeInstance.hasText;
    }
    get getTextElement() {
        return this.elementTypeInstance.textElement;
    }

    constructor(data: any | null = null) {
        super(data);

        this.elementType = data?.elementType || this.elementType;
        this.otherIds = data?.otherIds || this.otherIds;
        this.resourceNames = data?.resourceNames || this.resourceNames;
        this.options = data?.options || this.options;
        this.rect = data?.rect || this.rect;
        this.matrix = data?.matrix || this.matrix;

        this.elementTypeInstance = getElementTypeInstance(this) as ElementType;
    }

    override reset() {
        this.element?.remove();
        super.reset();
    }

    override onStart(direction: number) {
        if (this.element && this.element.parentElement) {
            return;
        }

        // creates the element from the `elementTypeInstance`
        this.element = this.elementTypeInstance.createElement();

        // add our stuff to it
        this.element.classList.add("element");
        this.element.dataset.actionId = this.id.toString();

        // disable dragging and use the `hidden` attribute
        if (this.element instanceof HTMLElement) {
            this.element.draggable = false;
            // hide it, maybe
            this.element.hidden = direction === 0 ? this.hidden : this.element.hidden;
        }
        // SVG elements don't have `hidden` attribute
        else {
            // hide it, maybe
            this.element.style.visibility = direction === 0 && this.hidden ? "hidden" : "visible";
        }
        
        // set the rect and matrix
        this.setRectAndMatrix(this.rect, this.matrix);

        // check if this will be a child of another, or...
        const otherAction = this.timeline.getAction(this.otherIds[0]) as unknown as AddElement;
        if (otherAction) {
            otherAction.elementTypeInstance.appendChild(this);
        }
        // just to the root viewer
        else {
            this.timeline.content.appendChild(this.element);
        }
    }

    override editorHTML(content: HTMLElement) {
        // markup
        content.innerHTML = `
            <hr />
            <!--
            <div class="row">
                <div class="three columns">
                    Element Type
                </div>
                <div class="nine columns">
                    <select name="element-type" class="u-full-width"></select>
                </div>
            </div>
            -->
            <div class="row">
                <div class="three columns">
                    Child of
                </div>
                <div class="nine columns">
                    <select name="other-ids" class="u-full-width"></select>
                </div>
            </div>
            <div class="row" style="display:none" data-other-ids="true">
                <div class="nine columns offset-by-three">
                    <!-- will be populated -->
                </div>
            </div>
            <div class="row" style="display:none">
                <div class="three columns">
                    Orientation
                </div>
                <div class="nine columns" data-transform-origin="true">
                    <svg height="50" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
                        <rect x="0" y="0" width="99" height="99" style="fill:black"/>
                        <rect x="0" y="0" width="33" height="33" style="fill:red" data-orientation="left top" />
                        <rect x="33" y="0" width="33" height="33" style="fill:red" data-orientation="center top" />
                        <rect x="66" y="0" width="33" height="33" style="fill:red" data-orientation="right top" />
                        <rect x="0" y="33" width="33" height="33" style="fill:red" data-orientation="left center" />
                        <rect x="33" y="33" width="33" height="33" style="fill:red" data-orientation="center center" />
                        <rect x="66" y="33" width="33" height="33" style="fill:red" data-orientation="right center" />
                        <rect x="0" y="66" width="33" height="33" style="fill:red" data-orientation="left bottom" />
                        <rect x="33" y="66" width="33" height="33" style="fill:red" data-orientation="center bottom" />
                        <rect x="66" y="66" width="33" height="33" style="fill:red" data-orientation="right bottom" />
                    </svg>
                </div>
            </div>

            <div class="element-type-options">
                <!-- will be populated -->
            </div>
        `;

        // list out other Actions available
        const selectAction = <HTMLSelectElement> content.querySelector(`select[name="other-ids"]`);
        createVisibleDropDown(this, selectAction, (action: Action) => {
            return this.otherIds.indexOf(action.id) >= 0 ? 1 : 0;
        });
        
        // keep reference to the Action id when admin updates
        selectAction.addEventListener("input", () => {
            this.otherIds[0] = parseInt(selectAction.value);
            this.updateRowOtherIds(content);
        });
        selectAction.dispatchEvent(new CustomEvent("input"));

        // // list out the types available
        // const selectElementType = <HTMLSelectElement> content.querySelector(`select[name="element-type"]`);
        // elementTypes.forEach(type => {
        //     const option = document.createElement("option");
        //     option.text = type.name;
        //     option.value = type.name;
        //     option.selected = type.name === this.elementType;
        //     selectElementType.options.add(option);
        // });

        // selectElementType.addEventListener("input", () => {
        //     this.elementType = selectElementType.value;
        //     this.elementTypeInstance = getElementTypeInstance(this);

        //     this.element?.remove();
        //     this.onStart(0);

        //     // rebuild
        //     this.elementTypeInstance.editorHTML(elementTypeOptions);
        // });

        const elementTypeOptions = <HTMLElement> content.querySelector(`div.element-type-options`);
        this.elementTypeInstance.editorHTML(elementTypeOptions);
    }

    setRectAndMatrix(rect: DOMRect, matrix: DOMMatrix): boolean {
        // can't set if we are dependent
        if (this.otherIds.length && this.otherIds[0] > 0) {
            return false;
        }

        // apply the transform info
        this.element.style.inset = `${rect.y}% auto auto ${rect.x}%`;
        this.element.style.width = rect.width ? `${rect.width}%` : "auto";
        this.element.style.height = rect.height ? `${rect.height}%` : "auto";
            
        const m = Matrix.fromMatrix(matrix);
        this.element.style.transform = m.toString();

        // set the props
        this.rect = rect;
        this.matrix = m;

        return true;
    }

    updateRowOtherIds(content: HTMLElement) {
        // get the other row div
        const rowOtherIds = <HTMLElement> content.querySelector(`.row[data-other-ids]`);
        rowOtherIds.style.display = "none";

        // get the Action by id
        const action = this.timeline.getAction(this.otherIds[0]) as unknown as AddElement;
        if (action && action.elementType === "Table") {
            rowOtherIds.style.display = "block";

            const div = rowOtherIds.querySelector("div")!;
            div.innerHTML = "";

            // create the able
            const table = action.elementTypeInstance.createElement();
            table.classList.add("outline");
            div.appendChild(table);

            const tds = Array.from(table.querySelectorAll("td"));
            const cellIndex = this.options.cellIndex;
            if (typeof cellIndex === "number" && tds[cellIndex]) {
                tds[cellIndex].classList.add("selected");
            }

            div.addEventListener("click", (event: MouseEvent) => {
                let target = event.target as HTMLElement;

                // remove everything
                Array.from(table.querySelectorAll(".selected")).forEach(td => td.classList.remove("selected"));

                // show the one selected
                let td = target.closest("td");
                if (td) {
                    td.classList.add("selected");

                    // keep track of the cell index
                    this.options.cellIndex = Array.from(table.querySelectorAll("td")).indexOf(td);
                }

                // dispatch
                rowOtherIds.dispatchEvent(new CustomEvent("input", { bubbles: true }));
            });
        }
    }
    
    override editorName() {
        let name = `${super.editorName()} ${this.elementType}`;
        if (this.otherIds[0] > 0) {
            const otherEvent = this.timeline.getAction(this.otherIds[0]);
            name += otherEvent ? ` to ${(otherEvent as Action).editorName()}` : "";
        }
        return name;
    }

    override export() {
        // round to 2 decimal places
        const r = (n: number) => (n * 100) / 100;

        // so there is less data being exported
        // @ts-ignore
        this.rect && Object.keys(this.rect).forEach(prop => this.rect[prop] = r(this.rect[prop]));
        // @ts-ignore
        this.matrix && Object.keys(this.matrix).forEach(prop => this.matrix[prop] = r(this.matrix[prop]));

        return Object.assign(super.export(), {
            elementType: this.elementType,
            otherIds: this.otherIds,
            resourceNames: this.resourceNames,
            options: this.options,
            rect: this.rect,
            matrix: this.matrix
        });
    }
}