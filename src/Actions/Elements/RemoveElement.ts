import { Action } from "../../";
import { log } from "../../Timeline";
import { createDurationsDropDown, createVisibleDropDown, validActionRelations } from "../utils";
import AddElement from "./AddElement";

export default class RemoveElement extends Action {
    
    otherIds: Array<number> = []; // the Action ids
    otherActions: Array<Action> = []; // the other Actions
    // actionProperties: {[id: number]: any} = {}; // keeps reference to original properties before being removed

    constructor(data: any | null = null) {
        super(data);

        this.otherIds = data?.otherIds || this.otherIds;
    }

    override reset() {
        this.otherActions.length = 0;
        super.reset();
    }

    override onStart(direction: number) {
        this.otherIds.forEach(id => {
            // get Action
            const otherAction = this.timeline.getAction(id);

            // add Action to array
            if (otherAction && this.otherActions.indexOf(otherAction) < 0) {
                this.otherActions.push(otherAction);
            }
        });

        // for the admin
        if (direction === 0) {
            this.onUpdate(0);
        }
    }

    override onUpdate(direction: number) {
        this.otherActions.forEach(otherAction => {
            const element = (otherAction as AddElement).element;
            element.style.opacity = direction === 0 && this.percentage01 === 0 ? "0" : (1 - this.percentage01).toString();
        });
    }

    override onEnd() {
        Object.values(this.otherActions).forEach(otherAction => (otherAction as AddElement).element.remove());
    }

    override editorHTML(content: HTMLElement) {
        // markup
        content.innerHTML = `
            <hr />
            <div class="row">
                <div class="three columns">
                    Fade Time
                </div>
                <div class="nine columns">
                    <select name="duration" class="u-full-width"></select>
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Element(s)
                    <span class="material-icons" style="float:right">clear_all</span>
                </div>
                <div class="nine columns">
                    <select name="other-ids" class="u-full-width" size="8" multiple></select>
                </div>
            </div>
        `;

        // fill in the actions
        const selectAction = <HTMLSelectElement> content!.querySelector(`select[name="other-ids"]`);
        createVisibleDropDown(this, selectAction, (action: Action) => {
            return this.otherIds.indexOf(action.id) >= 0 ? 1 : 0;
        });

        // keep reference to the Action id
        selectAction.addEventListener("input", () => {
            this.otherIds.length = 0;
            // grab the values selected, the values being the ids
            Array.from(selectAction.selectedOptions).forEach(option => {
                this.otherIds.push(parseInt(option.value));
            });
        });

        content.querySelector("span.material-icons")?.addEventListener("click", () => {
            const any = this.otherIds.length > 0;
            this.otherIds.length = 0;

            Array.from(selectAction.options).forEach(option => {
                if (parseInt(option.value) > 0) {
                    option.selected = any ? false : true;
                    !any && this.otherIds.push(parseInt(option.value));
                }
            });

            content.dispatchEvent(new CustomEvent("input", { bubbles: true }));
        });

        // show the fade times
        const selectDuration = <HTMLSelectElement> content.querySelector(`select[name="duration"]`);
        createDurationsDropDown(this, selectDuration);

        // so it sets the events
        selectAction.dispatchEvent(new CustomEvent("input"));
        // selectDuration.dispatchEvent(new CustomEvent("input"));
    }

    override get isValid(): boolean {
        return this.otherIds.length > 0 && validActionRelations(this, this.otherIds);
    }

    override export() {
        return Object.assign(super.export(), {
            otherIds: this.otherIds
        });
    }
}