import { TimelineEvent, Action } from "..";

export default class Pause extends Action {

    override onStart(direction: number) {
        if (direction <= 0) {
            return;
        }

        this.timeline.pause();

        this.dispatchEvent(new CustomEvent(TimelineEvent.ActionTriggered, {
            bubbles: true,
            detail: {
                type: this.type
            }
        }));
    }
}