import { Action } from "../../";
import { createVisibleDropDown, normalizeTextNode, validActionRelations } from "../utils";

import AddStyle from "./AddStyle";
import { STYLES, unwrap } from "./utils";

export default class RemoveStyle extends Action {
    
    otherIds: Array<number> = [];
    otherActions: Array<Action> = [];
    styleNames: Array<string> = [];

    constructor(data: any | null = null) {
        super(data);

        this.otherIds = data?.otherIds || this.otherIds;
        this.styleNames = data?.styleNames || this.styleNames;
    }

    override reset() {
        this.otherActions.length = 0;
        super.reset();
    }

    override onEnd() {
        this.otherIds.forEach(otherId => {
            // get Action
            const otherAction = this.timeline.getAction(otherId) as unknown as AddStyle;

            // add Action to array
            if (otherAction && otherAction.element && this.otherActions.indexOf(otherAction) < 0) {
                this.otherActions.push(otherAction);

                // remove the styles
                this.styleNames.forEach(styleName => {
                    otherAction.element!.classList.remove(STYLES[styleName]);
                });

                // done with it, normalize
                if (otherAction.element.classList.length <= 0) {
                    unwrap(otherAction.element);
                }
            }
        });
    }

    override editorHTML(content: HTMLElement) {
        // markup
        content.innerHTML = `
            <hr />
            <div class="row">
                <div class="three columns">
                    Style(s)
                </div>
                <div class="nine columns">
                    <select name="other-ids" class="u-full-width" size="8" multiple></select>
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Style Names
                </div>
                <div class="nine columns">
                    <select name="styleNames" class="u-full-width" size="8" multiple></select>
                </div>
            </div>
        `;

        // list out the types available
        const selectAction = <HTMLSelectElement> content.querySelector(`select[name="other-ids"]`);
        createVisibleDropDown(this, selectAction, (action: Action) => {
            if (!(action instanceof AddStyle)) {
                return -1;
            }
            return this.otherIds.indexOf(action.id) >= 0 ? 1 : 0;
        });

        // keep reference to the Action id when admin updates
        selectAction.addEventListener("input", () => {
            this.otherIds.length = 0;

            // grab the values selected, the values being the ids
            Array.from(selectAction.selectedOptions).forEach(option => {
                this.otherIds.push(parseInt(option.value));
            });

            this.otherIds.forEach(otherId => {
                // get reference to the otherAction
                const otherAction = this.timeline.getAction(otherId) as unknown as AddStyle;

                // update the styles available
                selectStyles.options.length = 0;
                otherAction?.styleNames.forEach(styleName => {
                    const option = document.createElement("option");
                    option.text = styleName;
                    option.value = styleName;
                    option.selected = this.styleNames.indexOf(styleName) > -1;
                    selectStyles.options.add(option);
                });
            });
        });

        const selectStyles = <HTMLSelectElement> content.querySelector(`select[name="styleNames"]`);
        // keep reference to the effects
        selectStyles.addEventListener("input", () => {
            this.styleNames.length = 0;
            // grab the values selected, the values being the ids
            Array.from(selectStyles.selectedOptions).forEach(option => {
                this.styleNames.push(option.value);
            });
        });

        // emit it
        selectAction.dispatchEvent(new CustomEvent("input"));
    }

    override get isValid() {
        let flag = true;

        if (!this.otherIds.length) {
            flag = false;
        }
        if (!validActionRelations(this, this.otherIds)) {
            flag = false;
        }
        
        return flag;
    }

    override export() {
        return Object.assign(super.export(), {
            otherIds: this.otherIds,
            styleNames: this.styleNames
        });
    }
}