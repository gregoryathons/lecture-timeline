import AddElement from "../Elements/AddElement";
import { Track, Action } from "../../";
import { trace } from "../../Timeline";
import { getTextRange, normalizeTextNode, validActionRelations, createVisibleDropDown } from "../utils";

import { STYLES, unwrap } from "./utils";

export default class AddStyle extends Action {
    _hidden = true;

    element: HTMLElement | null = null;
    
    otherIds: Array<number> = [];
    styleNames: Array<string> = [];
    startOffset: number = -1;
    endOffset: number = -1;

    constructor(data: any | null = null) {
        super(data);

        this.otherIds = data?.otherIds || this.otherIds;
        this.styleNames = data?.styleNames || this.styleNames;
        this.startOffset = data && typeof data.startOffset === "number" ? data.startOffset : this.startOffset;
        this.endOffset = data && typeof data.endOffset === "number" ? data.endOffset : this.endOffset;
    }

    override reset() {
        this.element?.remove();
        super.reset();
    }

    // this was helpful
    // https://stackoverflow.com/questions/6240139/highlight-text-range-using-javascript
    override onStart(direction: number) {
        if (this.element?.parentElement) {
            return;
        }

        // get the other Action
        const otherAction = this.timeline.getAction(this.otherIds[0]) as unknown as AddElement;

        if (otherAction) {
            this.element = document.createElement("span");
            this.element.dataset.actionId = this.id.toString();
            this.styleNames.forEach(styleName => this.element?.classList.add(STYLES[styleName]));
            
            // hide it, maybe
            this.element.hidden = direction === 0 ? this.hidden : false;

            // our Action's element
            let otherElement = otherAction.getTextElement!;

            // trace(window.location.href, range.toString());
            if (otherElement.innerText !== "") {
                const range = getTextRange(otherElement, this.startOffset, this.endOffset);
                try {
                    this.element.appendChild(range.extractContents());
                    range.insertNode(this.element);
                }
                catch(e) {}
            }
        }
    }

    override editorHTML(content: HTMLElement) {
        // markup
        content.innerHTML = `
            <hr />
            <div class="row">
                <div class="three columns">
                    Element
                </div>
                <div class="nine columns">
                    <select name="other-id" class="u-full-width"></select>
                </div>
            </div>
            <div class="row" data-for="action-id" style="display: none">
                <div class="three columns">
                    Styles
                </div>
                <div class="nine columns">
                    <select name="styleNames" class="u-full-width" size="8" multiple></select>
                </div>
            </div>
            <div class="row" data-for="action-id" data-output="true" style="display: none">
                <div class="three columns">
                    Output
                </div>
                <div class="nine columns" data-contenteditable="true">
                    <!-- will be populated -->
                </div>
            </div>
        `;

        // reference to the other Action highlighted
        let otherAction: AddElement | null = null;

        // list out the types available
        const selectAction = <HTMLSelectElement> content.querySelector(`select[name="other-id"]`);
        createVisibleDropDown(this, selectAction, (action: Action) => {
            if (!(action as AddElement).hasText) {
                return -1;
            }
            return this.otherIds.indexOf(action.id) >= 0 ? 1 : 0;
        });

        // keep reference to the Action id when admin updates
        selectAction.addEventListener("input", () => {
            if (selectAction.selectedIndex < 0) {
                return;
            }

            this.otherIds[0] = parseInt(selectAction.value);

            // get reference to the otherAction
            otherAction = this.timeline.getAction(this.otherIds[0]) as unknown as AddElement;
            
            // show the row(s)
            content.querySelectorAll(".row[data-for]").forEach(el => {
                const element = el as HTMLElement;
                element.style.display = otherAction ? "" : "none";

                // show the output
                if (otherAction && element.dataset.output) {
                    // this.onBefore(); // remove self/normalize text so we get the most recent
                    unwrap(this.element); // remove self/normalize text so we get the most recent
                    outputElement.innerHTML = otherAction.getTextElement!.innerText;

                    // show the highlight
                    const span = document.createElement("span");
                    span.classList.add("selected");

                    const range = getTextRange(outputElement, this.startOffset, this.endOffset);
                    try {
                        span.appendChild(range.extractContents());
                        range.insertNode(span);
                    }
                    catch(e) {}
                }
            });
        });

        const selectStyles = <HTMLSelectElement> content.querySelector(`select[name="styleNames"]`);
        Object.keys(STYLES).forEach(style => {
            const option = document.createElement("option");
            option.text = style;
            option.value = style;
            option.selected = this.styleNames.indexOf(style) > -1;
            selectStyles.options.add(option);
        });

        // keep reference to the effects
        selectStyles.addEventListener("input", () => {
            this.styleNames.length = 0;
            // grab the values selected, the values being the ids
            Array.from(selectStyles.selectedOptions).forEach(option => {
                this.styleNames.push(option.value);
            });
        });

        // apply an event to when the admin releases text selection
        const outputRow = <HTMLElement> content.querySelector(".row[data-output]");
        const outputElement = <HTMLElement> outputRow.querySelector("[data-contenteditable]");
        outputElement.addEventListener("mousedown", (event) => {
            // remove old so our window.getSelection doesn't grab nested nodes
            if (otherAction) {
                unwrap(this.element); // this.onBefore();
                outputElement.innerHTML = otherAction.getTextElement!.innerText;
            }
        });
        outputElement.addEventListener("mouseup", (event) => {
            // get new selection
            const sel = window.getSelection();
            const rangeSelected = sel?.getRangeAt(0) as Range;

            // set the new start and end offsets
            this.startOffset = rangeSelected.startOffset;
            this.endOffset = rangeSelected.endOffset;

            // fake dispatch so it bubbles up to the editor
            selectAction.dispatchEvent(new CustomEvent("input", { bubbles: true }));
        });

        // emit it
        selectAction.dispatchEvent(new CustomEvent("input"));
    }

    override get isValid() {
        let flag = true;

        if (!this.otherIds.length) {
            flag = false;
        }
        if (!validActionRelations(this, this.otherIds)) {
            flag = false;
        }
        
        return flag;
    }
    
    override export() {
        return Object.assign(super.export(), {
            otherIds: this.otherIds,
            styleNames: this.styleNames,
            startOffset: this.startOffset,
            endOffset: this.endOffset
        });
    }
}