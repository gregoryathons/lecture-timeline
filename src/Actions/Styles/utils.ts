// name of the styles, css class name
export const STYLES: {[name: string]: string} = {
    "Bold": "text-bold",
    "Italic": "text-italic",
    "Underline": "text-underline",
    "Red": "text-red",
    "Green": "text-green",
    "Blue": "text-blue",
};

export function unwrap(element: HTMLElement | null) {
    const parent = element?.parentElement;
    if (parent) {
        // unwrap ourself
        while (element.firstChild) {
            parent.insertBefore(element.firstChild, element);
        }
        // now remove ourself
        element.remove();
    }
}