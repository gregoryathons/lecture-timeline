import { TimelineEvent, Action } from "..";
import { trace } from "../Timeline";
import { secondsToString } from "../utils";

export default class Play extends Action {

    trackId = 0;
    trackTime = 0;
    useLastTime = false;

    constructor(data: any | null = null) {
        super(data);

        this.trackId = data?.trackId || this.trackId;
        this.trackTime = data?.trackTime || this.trackTime;
        this.useLastTime = data && typeof data.useLastTime === "boolean" ? data.useLastTime : this.useLastTime;
    }

    override onStart(direction: number) {
        if (direction <= 0) {
            return;
        }

        trace(`- play (${this.id}) -`);

        // find the next track
        const nextTrack = this.timeline.tracks.find(track => track.id === this.trackId);

        // move to the next track
        if (nextTrack) {
            this.timeline.currentTrack = nextTrack;
            this.timeline.currentTrack.jumpTo(this.useLastTime ? this.timeline.currentTrack.time : this.trackTime);

            this.dispatchEvent(new CustomEvent(TimelineEvent.ActionTriggered, {
                bubbles: true,
                detail: {
                    type: this.type,
                    trackId: this.trackId,
                    trackTime: this.useLastTime ? this.timeline.currentTrack.time : this.trackTime
                }
            }));
        }
    }

    override editorHTML(content: HTMLElement) {
        // markup
        content.innerHTML = `
            <hr />
            <div class="row">
                <div class="three columns">
                    Track Name
                </div>
                <div class="nine columns">
                    <select name="trackName" class="u-full-width"></select>
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Last Track Time
                </div>
                <div class="nine columns">
                    <input type="checkbox" name="use-last-time" ${this.useLastTime ? "checked" : ""} />
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Track Time
                </div>
                <div class="seven columns">
                    <!-- max property is declared later -->
                    <input type="range" name="track-time" class="u-full-width" min="0" step="0.1" value="${this.trackTime}" ${this.useLastTime ? "disabled" : ""} />
                </div>
                <div class="two columns">
                    <output></output>
                </div>
            </div>
        `;

        // fill in the track names
        const selectTrackName = <HTMLSelectElement> content.querySelector(`select[name="trackName"]`);
        this.timeline.tracks.forEach(track => {
            const option = document.createElement("option");
            option.text = track.name;
            option.value = track.id.toString();
            option.selected = track.id === this.trackId;
            selectTrackName.options.add(option);
        });

        // apply the change event
        selectTrackName.addEventListener("input", () => {
            this.trackId = parseInt(selectTrackName.value);

            // set the max duration of the input field
            const nextTrack = this.timeline.tracks.find(track => track.id === this.trackId);
            if (nextTrack) {
                inputTime.setAttribute("max", nextTrack.duration.toFixed(1));
            }
        });

        // event for checkbox
        const inputCheckbox = <HTMLInputElement> content.querySelector(`input[name="use-last-time"]`);
        inputCheckbox.addEventListener("input", () => {
            this.useLastTime = inputCheckbox.checked;
            inputTime.disabled = this.useLastTime;
        });

        // apply the input change event
        const inputTime = <HTMLInputElement> content.querySelector(`input[name="track-time"]`);
        inputTime.addEventListener("input", () => {
            this.trackTime = parseFloat(inputTime.value);

            // update the output
            content.querySelector("output")!.innerHTML = secondsToString(this.trackTime);
        });

        // call events
        selectTrackName.dispatchEvent(new CustomEvent("input"));
        inputTime.dispatchEvent(new CustomEvent("input"));
    }

    override export() {
        return Object.assign(super.export(), {
            trackId: this.trackId,
            trackTime: this.trackTime,
            useLastTime: this.useLastTime
        });
    }
}