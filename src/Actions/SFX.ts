import { TimelineEvent, Action } from "..";
import { log, trace } from "../Timeline";
import { createResourceDropDown } from "./utils";

export default class SFX extends Action {

    resourceNames: Array<string> = [];
    volume = 1;
    panner = 0;

    constructor(data: any | null = null) {
        super(data);

        this.resourceNames = data?.resourceNames || this.resourceNames;
        this.volume = typeof data?.volume === "number" ? data.volume : this.volume;
        this.panner = typeof data?.panner === "number" ? data.panner : this.panner;
    }

    override onStart(direction: number) {
        if (direction <= 0) {
            return;
        }

        this.timeline.sound.play(this.resourceNames[0], {
            volume: this.volume, 
            panner: this.panner
        });

        this.dispatchEvent(new CustomEvent(TimelineEvent.ActionTriggered, {
            bubbles: true,
            detail: {
                type: this.type
            }
        }));
    }

    override editorHTML(content: HTMLElement) {
        content.innerHTML = `
            <hr />
            <div class="row">
                <div class="three columns">
                    Source
                </div>
                <div class="nine columns">
                    <select name="resource-name" class="u-full-width"></select>
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Volume
                </div>
                <div class="seven columns">
                    <input type="range" name="volume" min="0" max="1" step="0.1" value="${this.volume}" class="u-full-width" />
                </div>
                <div class="two columns">
                    <output>${this.volume}</output>
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Speaker
                </div>
                <div class="seven columns">
                    <input type="range" name="panner" min="-1" max="1" step="0.1" value="${this.panner}" class="u-full-width" />
                </div>
                <div class="two columns">
                    <output>${this.panner}</output>
                </div>
            </div>
        `;

        // list out the SFX's available
        const selectSFX = <HTMLSelectElement> content.querySelector(`select[name="resource-name"]`);
        createResourceDropDown(this, this.timeline.loader.getResources("sounds", "fx"), selectSFX, this.resourceNames[0]);
        selectSFX.addEventListener("input", () => {
            this.resourceNames[0] = selectSFX.value;
        });

        const inputVolume = <HTMLInputElement> content.querySelector(`input[name="volume"]`);
        inputVolume.addEventListener("input", () => {
            this.volume = parseFloat(inputVolume.value);
            inputVolume.closest(".row")!.querySelector("output")!.innerHTML = this.volume.toString();
        });

        const inputPanner = <HTMLInputElement> content.querySelector(`input[name="panner"]`);
        inputPanner.addEventListener("input", () => {
            this.panner = parseFloat(inputPanner.value);
            inputPanner.closest(".row")!.querySelector("output")!.innerHTML = this.panner.toString();
        });

        selectSFX.dispatchEvent(new CustomEvent("input"));
    }

    override get isValid() {
        return this.resourceNames[0] !== "";
    }

    override export() {
        return Object.assign(super.export(), {
            resourceNames: this.resourceNames,
            volume: this.volume,
            panner: this.panner
        });
    }
}