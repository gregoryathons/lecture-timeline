import { Action } from "../..";
import { TimelineEvent } from "../../Const";
import AddElement from "../Elements/AddElement";
import { breakApartText } from "../Elements/utils";
import { unwrap } from "../Styles/utils";
import { createVisibleDropDown, getTextRange, validActionRelations } from "../utils";
import BaseInteraction from "./BaseInteraction";

export default class Input extends BaseInteraction {

    element: HTMLInputElement;
    otherIds: Array<number> = [];
    startOffset = -1;
    endOffset = -1;
    answer = "";
    additionalSpacing = false;
    replace = false;

    constructor(data: any | null = null) {
        super(data);

        this.otherIds = data?.otherIds || this.otherIds;
        this.startOffset = data && typeof data.startOffset === "number" ? data.startOffset : this.startOffset;
        this.endOffset = data && typeof data.endOffset === "number" ? data.endOffset : this.endOffset;
        this.answer = data?.answer || this.answer;
        this.additionalSpacing = data?.additionalSpacing || this.additionalSpacing;
        this.replace = data?.replace || this.replace;
    }

    override reset() {
        this.interactive = false;
        this.element?.remove();
        super.reset();
    }

    override onStart(direction: number) {
        if (this.element?.parentElement) {
            return;
        }

        // get the other Action
        const otherAction = this.timeline.getAction(this.otherIds[0]) as unknown as AddElement;
        if (otherAction) {

            this.element = <HTMLInputElement> document.createElement("input");
            this.element.type = "text";
            this.element.dataset.actionId = this.id.toString();
            this.element.classList.add("empty");
            this.element.draggable = false;

            // hide it, maybe
            this.element.hidden = direction === 0 ? this.hidden : this.element.hidden;

            // our Action's element
            let otherElement = otherAction.getTextElement!;

            if (otherElement.innerText !== "") {
                const range = getTextRange(otherElement, this.startOffset, this.endOffset);
                try {
                    this.element.appendChild(range.extractContents());
                    range.insertNode(this.element);
                }
                catch(e) {}
            }
        }

        // we added the element, reset some stuff
        if (this.element?.parentElement) {
            this.element.value = "";
            this.element.size = (this.answer.length || 1) + (this.additionalSpacing ? 2 : 0);
            this.element.maxLength = this.element.size;
            this.element.readOnly = false;
            this.element.classList.add("empty");

            // lock the Timeline
            if (direction > 0) {
                this.timeline.pause(this);
                this.interactive = true;
            }
        }
    }

    // override onAfter(direction: number) {
    override onEnd(direction: number) {
        if (this.timeline.isLocked) {
            return;
        }

        this.interactive = false;

        // unwrap itself
        if (!this.replace) {
            unwrap(this.element);
        }
        // replace the text
        else if (this.element?.parentElement) {
            const parser = new DOMParser();
            const node = parser.parseFromString(breakApartText(this.answer), "text/html");
            this.element.parentElement.replaceChild(node.body.firstChild!, this.element);
        }
    }

    override onInteraction(event: Event) {
        if (!this.timeline.isLocked) {
            return;
        }

        switch(event.type) {
            case TimelineEvent.ActionInteractionCheck: {
                this.checkAnswer();
                break;
            }
            case "input": {
                if (event.target === this.element) {
                    this.dispatchEvent(new CustomEvent(TimelineEvent.ActionInteraction, {
                        bubbles: true,
                        detail: {
                            type: this.type,
                            input: this.element
                        }
                    }));
                }
                break;
            }
            case "keyup": {
                // wait for Enter key
                const key = ((event as KeyboardEvent).key || "").toLowerCase();
                if (key === "enter" && event.target === this.element) {
                    this.checkAnswer();
                }
                break;
            }
        }
    }

    checkAnswer() {
        this.interactive = false;
        this.element.readOnly = true;
        const current = this.element.value.trim();
        const correct = current === this.answer;

        // dispatch the event
        this.dispatchEvent(new CustomEvent(TimelineEvent.ActionInteractionResults, {
            bubbles: true,
            detail: {
                type: this.type,
                correct: current === this.answer
            }
        }));

        // play the Track
        const trackId = this.trackIds[correct ? 0 : 1];
        if (trackId) {
            this.playTrack(trackId);
        }
        // play Clip
        else {
            this.playClip(this.resourceNames[correct ? 0 : 1], () => this.timeline.resume(true));
        }
    }

    override editorHTML(content: HTMLElement) {
        const endHTML = `
            <hr />

            <div class="row">
                <div class="three columns">
                    Element
                </div>
                <div class="nine columns">
                    <select name="other-ids" class="u-full-width"></select>
                </div>
            </div>
            <div class="row" data-for="action-id" data-output="true" style="display: none">
                <div class="three columns">
                    Selection
                </div>
                <pre class="nine columns" data-contenteditable="true">
                    <!-- will be populated -->
                </pre>
            </div>
            <div class="row" data-for="action-id" data-output="true" style="display: none">
                <div class="three columns">
                    Answer
                </div>
                <div class="nine columns">
                    <input type="text" name="answer" class="u-full-width" value="${this.answer}" />
                </div>
            </div>
            <div class="row" data-for="action-id" data-output="true" style="display: none">
                <div class="three columns">
                    Additional Spacing
                </div>
                <div class="nine columns">
                    <input type="checkbox" name="additional-spacing" ${this.additionalSpacing ? "checked" : ""} />
                </div>
            </div>
            <div class="row" data-for="action-id" data-output="true" style="display: none">
                <div class="three columns">
                    Replace
                </div>
                <div class="nine columns">
                    <input type="checkbox" name="replace" ${this.replace ? "checked" : ""} />
                </div>
            </div>
        `;

        // call the parent
        super.editorHTML(content, "", "", endHTML);

        // reference to the other Action highlighted
        let otherAction: AddElement | null = null;

        // list out the types available
        const selectAction = <HTMLSelectElement> content.querySelector(`select[name="other-ids"]`);
        createVisibleDropDown(this, selectAction, (action: Action) => {
            if (!(action as AddElement).hasText) {
                return -1;
            }
            return this.otherIds.indexOf(action.id) >= 0 ? 1 : 0;
        });

        // keep reference to the Action id when admin updates
        selectAction.addEventListener("input", () => {
            if (selectAction.selectedIndex < 0) {
                return;
            }

            this.otherIds[0] = parseInt(selectAction.value);

            // get reference to the otherAction
            otherAction = this.timeline.getAction(this.otherIds[0]) as unknown as AddElement;
            
            // show the row(s)
            content.querySelectorAll(".row[data-for]").forEach(el => {
                const element = el as HTMLElement;
                element.style.display = otherAction ? "" : "none";

                // show the output
                if (otherAction && element.dataset.output) {
                    // this.onBefore(); // remove self/normalize text so we get the most recent
                    unwrap(this.element);
                    outputElement.innerHTML = otherAction.getTextElement!.innerText;

                    // show the highlight
                    const span = document.createElement("span");
                    span.classList.add("selected");

                    const range = getTextRange(outputElement, this.startOffset, this.endOffset);
                    try {
                        span.appendChild(range.extractContents());
                        range.insertNode(span);
                    }
                    catch (e) {}
                }
            });
        });

        // apply an event to when the admin releases text selection
        const outputRow = <HTMLElement> content.querySelector(".row[data-output]");
        const outputElement = <HTMLElement> outputRow.querySelector("[data-contenteditable]");
        outputElement.addEventListener("mousedown", (event) => {
            // remove old so our window.getSelection doesn't grab nested nodes
            if (otherAction) {
                // this.onBefore();
                unwrap(this.element);
                outputElement.innerHTML = otherAction.getTextElement!.innerText;
            }
        });
        outputElement.addEventListener("mouseup", (event) => {
            // get new selection
            const sel = window.getSelection();
            const rangeSelected = sel?.getRangeAt(0) as Range;

            // set the new start and end offsets
            this.startOffset = rangeSelected.startOffset;
            this.endOffset = rangeSelected.endOffset;

            // fake dispatch so it bubbles up to the editor
            selectAction.dispatchEvent(new CustomEvent("input", { bubbles: true }));
        });

        // set's the answer
        const inputAnswer = <HTMLInputElement> content.querySelector(`input[name="answer"]`);
        inputAnswer.addEventListener("input", () => {
            this.answer = inputAnswer.value.trim();
        });

        // 
        const inputAdditionalSpacing = <HTMLInputElement> content.querySelector(`input[name="additional-spacing"]`);
        inputAdditionalSpacing.addEventListener("input", () => {
            this.additionalSpacing = inputAdditionalSpacing.checked;
        });

        // 
        const inputReplace = <HTMLInputElement> content.querySelector(`input[name="replace"]`);
        inputReplace.addEventListener("input", () => {
            this.replace = inputReplace.checked;
        });

        // disptach events
        selectAction.dispatchEvent(new CustomEvent("input"));
    }

    override get isValid() {
        let flag = super.isValid;

        if (!this.otherIds.length) {
            flag = false;
        }
        if (!validActionRelations(this, this.otherIds)) {
            flag = false;
        }

        return flag;
    }

    override export() {
        return Object.assign(super.export(), {
            otherIds: this.otherIds,
            startOffset: this.startOffset,
            endOffset: this.endOffset,
            answer: this.answer,
            additionalSpacing: this.additionalSpacing,
            replace: this.replace
        });
    }
}