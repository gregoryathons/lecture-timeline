import { Action } from "../..";
import { createResourceDropDown } from "../utils";

export default class BaseInteraction extends Action {

    duration = 0.1;
    resourceNames: Array<string> = []; // 0 - correct, 1 - wrong
    trackIds: Array<number> = []; // 0 - correct, 1 - wrong

    constructor(data: any | null = null) {
        super(data);

        this.resourceNames = data?.resourceNames || this.resourceNames;
        this.trackIds = data?.trackIds || this.trackIds;
    }

    playTrack(id: number) {
        const nextTrack = this.timeline.tracks.find(track => track.id === id);

        if (!nextTrack) {
            console.warn("NO TRACK SELECTED?");
        }
        else {
            this.timeline.currentTrack = nextTrack;
            this.timeline.currentTrack.jumpTo(0);
            this.timeline.resume(true);
        }
    }

    playClip(name: string, callback: Function) {
        if (!name) {
            console.warn("NO CLIP SELECTED?");
        }
        else {
            this.timeline.sound.play(name, {
                complete: () => {
                    callback();
                }
            });
        }
    }

    markupClipOrTrackDropDown(prefix: string) {
        return `
            <div class="row">
                <div class="three columns">
                    Clip or Track
                </div>
                <div class="nine columns">
                    <select name="${prefix}-clip-or-track" class="u-full-width">
                        <option value="clip">Clip</option>
                        <option value="track">Track</option>
                    </select>
                </div>
            </div>
            <div class="row" data-${prefix}-clip="true" style="display:none">
                <div class="three columns">
                    Clip
                </div>
                <div class="nine columns">
                    <select name="${prefix}-resource-name" class="u-full-width"></select>
                </div>
            </div>
            <div class="row" data-${prefix}-track="true" style="display:none">
                <div class="three columns">
                    Track
                </div>
                <div class="nine columns">
                    <select name="${prefix}-track-id" class="u-full-width"></select>
                </div>
            </div>
        `;
    }

    override editorHTML(content: HTMLElement, correctHTML: string = "", wrongHTML: string = "", endHTML: string = "") {
        // markup
        content.innerHTML = `
            <hr />

            <div class="row">
                <div class="twelve columns">
                    <strong>Correct</strong>
                </div>
            </div>
            ${this.markupClipOrTrackDropDown("correct")}
            ${correctHTML}

            <hr />

            <div class="row">
                <div class="twelve columns">
                    <strong>Wrong</strong>
                </div>
            </div>
            ${this.markupClipOrTrackDropDown("wrong")}
            ${wrongHTML}

            ${endHTML}
        `;

        this.applyEditorEvents(content, "correct");
        this.applyEditorEvents(content, "wrong");
    }

    applyEditorEvents(content: HTMLElement, prefix: string) {
        const index = prefix === "correct" ? 0 : 1;

        // clip or track?
        const selectClipOrTrack = <HTMLSelectElement> content.querySelector(`select[name="${prefix}-clip-or-track"]`);
        selectClipOrTrack.addEventListener("input", () => {
            const value = selectClipOrTrack.value;
            const clipRow = content.querySelector(`.row[data-${prefix}-clip]`) as HTMLElement;
            const trackRow = content.querySelector(`.row[data-${prefix}-track]`) as HTMLElement;

            clipRow.style.display = value === "clip" ? "block" : "none";
            trackRow.style.display = value === "clip" ? "none" : "block";

            if (value === "clip") {
                this.trackIds[index] = 0;
                clipRow.querySelector("select")?.dispatchEvent(new CustomEvent("input"));
            }
            else {
                this.resourceNames[index] = "";
                trackRow.querySelector("select")?.dispatchEvent(new CustomEvent("input"));
            }
        });
        selectClipOrTrack.selectedIndex = this.trackIds[index] ? 1 : 0;

        // display tracks
        const selectTrack = <HTMLSelectElement> content.querySelector(`select[name="${prefix}-track-id"]`);
        this.timeline.tracks.forEach(track => {
            const option = document.createElement("option");
            option.text = track.name;
            option.value = track.id.toString();
            option.selected = this.trackIds[index] !== 0; // first index
            selectTrack.options.add(option);
        });
        selectTrack.addEventListener("input", () => {
            this.trackIds[index] = parseInt(selectTrack.value);
        });

        // display clips
        const selectClip = <HTMLSelectElement> content.querySelector(`select[name="${prefix}-resource-name"]`);
        createResourceDropDown(this, this.timeline.loader.getResources("sounds", "nv"), selectClip, this.resourceNames[index]);
        selectClip.addEventListener("input", () => {
            this.resourceNames[index] = selectClip.value;
        });

        // dispatch
        selectClipOrTrack.dispatchEvent(new CustomEvent("input"));
    }

    override get isValid() {
        let flag = true;

        // nothing set for correct
        if (!this.resourceNames[0] && !this.trackIds[0]) {
            flag = false;
        }
        // nothing set for wrong
        if (!this.resourceNames[1] && !this.trackIds[1]) {
            flag = false;
        }

        return flag;
    }

    override export() {
        return Object.assign(super.export(), {
            resourceNames: this.resourceNames,
            trackIds: this.trackIds
        });
    }
}