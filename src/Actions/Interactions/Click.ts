import { TimelineEvent, Action } from "../../";
import AddElement from "../Elements/AddElement";
import { createVisibleDropDown, getTextNodesIn, getTextRange, validActionRelations } from "../utils";
import BaseInteraction from "./BaseInteraction";

export default class Click extends BaseInteraction {

    correctIds: Array<number> = []; // the correct Action id's selection
    correctOffsets: Array<Array<number>> = [];
    wrongIds: Array<number> = [];
    wrongOffsets: Array<Array<number>> = [];
    resumeOnWrong = true;

    // when selecting the elements/index/ids, this keeps track of them
    selectedIds: Array<number> = [];
    selectedOffsets: Array<Array<number>> = [];

    constructor(data: any | null = null) {
        super(data);

        this.correctIds = data?.correctIds || this.correctIds;
        this.correctOffsets = data?.correctOffsets || this.correctOffsets;
        this.wrongIds = data?.wrongIds || this.wrongIds;
        this.wrongOffsets = data?.wrongOffsets || this.wrongOffsets;
        this.resumeOnWrong = data && typeof data.resumeOnWrong === "boolean" ? data.resumeOnWrong : this.resumeOnWrong;
    }

    // override onBefore(direction: number) {
    override reset() {
        this.selectedIds.length = 0;
        this.selectedOffsets.length = 0;
        this.interactive = false;
    }

    override onStart(direction: number) {
        if (direction > 0) {
            this.timeline.pause(this);
            this.interactive = true;
        }
    }

    // override onAfter(direction: number) {
    override onEnd(direction: number) {
        if (this.timeline.isLocked) {
            return;
        }

        this.interactive = false;

        // remove the elements
        if (this.selectedIds.length || this.selectedOffsets.length) {
            // recursive
            function removeClass(children: HTMLCollection) {
                Array.from(children).forEach(kid => {
                    kid.classList.remove("selected");

                    if (kid.children.length) {
                        removeClass(kid.children);
                    }
                });
            }

            // removes "selected" class
            this.correctIds.forEach(id => {
                removeClass((this.timeline.getAction(id) as unknown as AddElement).element.children);
            });
            this.wrongIds.forEach(id => {
                removeClass((this.timeline.getAction(id) as unknown as AddElement).element.children);
            });
        }

        this.selectedIds.length = 0;
        this.selectedOffsets.length = 0;
    }

    override onInteraction(event: Event) {
        if (event.type !== "click") {
            return;
        }
        if (!this.timeline.isLocked) {
            return;
        }

        const element = event.target as HTMLElement;
        const parent = element.closest(".element") as HTMLElement;
        if (!parent) {
            return;
        }

        const actionId = parseInt(parent.dataset.actionId || "0");
        const selectedCorrect = this.correctIds.indexOf(actionId) >= 0;
        const selectedWrong = this.wrongIds.indexOf(actionId) >= 0;
        
        // did we select something from the `correctIds` or `wrongIds`?
        if (!selectedCorrect && !selectedWrong) {
            return;
        }

        // 0 - neutral, -1 wrong, 1 correct
        let selected = 0;

        // selected a word, and we are checking the words
        if (this.correctOffsets.length && this.wrongOffsets.length) {
            if (!element.classList.contains("letter")) {
                return; // no letter selected
            }

            const parentElement = element.closest(".word")?.parentElement!;
            const index = getTextNodesIn(parentElement, false).indexOf(element.childNodes[0]);

            // console.log(parentElement, index, letters);
            // console.log(letters?.length, letters.indexOf(element));

            const addSpan = (offset: Array<number>) => {
                // add to our list
                this.selectedOffsets.push(offset);

                // wrap the selection around the text
                const span = document.createElement("span");
                span.classList.add("selected");
                try {
                    const range = getTextRange(parentElement, offset[0], offset[1]);
                    span.appendChild(range.extractContents());
                    range.insertNode(span);
                }
                catch (e) {}
            }

            if (selectedCorrect) {
                this.correctOffsets.forEach(offset => {
                    if (offset[0] <= index && offset[1] >= index) {
                        addSpan(offset);
                        selected = 1;
                    }
                });
            }

            if (!selected && selectedWrong) {
                this.wrongOffsets.forEach(offset => {
                    if (offset[0] <= index && offset[1] >= index) {
                        addSpan(offset);
                        selected = -1;
                    }
                });
            }
        }
        // selected an element
        else {
            // did the user select the correct or wrong one?
            if (this.selectedIds.indexOf(actionId) < 0) {
                if (selectedCorrect) {
                    this.selectedIds.push(actionId);
                    selected = 1;
                }
                else if (selectedWrong) {
                    this.selectedIds.push(actionId);
                    selected = -1;
                }
            }
        }

        // nothing
        if (!selected) {
            return;
        }

        let done = false;
        let correct = false;

        // user selected the number required
        if (this.selectedOffsets.length === this.correctOffsets.length && this.selectedOffsets.length) {
            correct = this.selectedOffsets.filter(offset => this.wrongOffsets.indexOf(offset) >= 0).length === 0;
            done = true;
        }
        else if (this.selectedIds.length === this.correctIds.length && this.selectedIds.length) {
            correct = this.selectedIds.filter(id => this.wrongIds.indexOf(id) >= 0).length === 0;
            done = true;
        }

        // dispatch the event
        this.dispatchEvent(new CustomEvent(TimelineEvent.ActionInteraction, {
            bubbles: true,
            detail: {
                type: this.type
            }
        }));

        // the user selected the number required
        if (done) {
            // prevents click spamming
            this.interactive = false;

            // dispatch the event
            this.dispatchEvent(new CustomEvent(TimelineEvent.ActionInteractionResults, {
                bubbles: true,
                detail: {
                    type: this.type,
                    correct: correct
                }
            }));

            // play the Track
            const trackId = this.trackIds[correct ? 0 : 1];
            if (trackId) {
                this.playTrack(trackId);
            }
            // play Clip
            else {
                this.playClip(this.resourceNames[correct ? 0 : 1], () => {
                    if (correct || (!correct && this.resumeOnWrong)) {
                        this.timeline.resume(true);
                    }
                });
            }
        }
    }

    override editorHTML(content: HTMLElement) {
        const correctHTML = `
            <div class="row">
                <div class="three columns">
                    Element(s)
                </div>
                <div class="nine columns">
                    <select name="correct-ids" class="u-full-width" size="8" multiple></select>
                </div>
            </div>
            <div class="row" data-for-type="correct-text" data-output="true" style="display: none">
                <div class="three columns">
                    Output
                    <span class="material-icons" style="float:right">refresh</span>
                </div>
                <pre class="nine columns" data-contenteditable="true">
                    <!-- will be populated -->
                </pre>
            </div>
        `;

        const wrongHTML = `
            <div class="row">
                <div class="three columns">
                    Element(s)
                </div>
                <div class="nine columns">
                    <select name="wrong-ids" class="u-full-width" size="8" multiple></select>
                </div>
            </div>
            <div class="row" data-for-type="wrong-text" data-output="true" style="display: none">
                <div class="three columns">
                    Output
                    <span class="material-icons" style="float:right">refresh</span>
                </div>
                <pre class="nine columns" data-contenteditable="true">
                    <!-- will be populated -->
                </pre>
            </div>
            <div class="row">
                <div class="three columns" title="Resume play if the end-user get it's incorrect">
                    Resume Play
                </div>
                <div class="nine columns">
                    <input type="checkbox" name="resume-on-wrong" ${this.resumeOnWrong ? "checked" : ""} />
                </div>
            </div>
        `;

        super.editorHTML(content, correctHTML, wrongHTML);

        // list out the Elements
        const selectCorrectIds = <HTMLSelectElement> content.querySelector(`select[name="correct-ids"]`);
        createVisibleDropDown(this, selectCorrectIds, (action: Action) => {
            return this.correctIds.indexOf(action.id) >= 0 ? 1 : 0;
        });

        // list out the Elements
        const selectWrongIds = <HTMLSelectElement> content.querySelector(`select[name="wrong-ids"]`);
        createVisibleDropDown(this, selectWrongIds, (action: Action) => {
            return this.wrongIds.indexOf(action.id) >= 0 ? 1 : 0;
        });
        
        // keep reference to the Action id
        selectCorrectIds.addEventListener("input", () => {
            this.correctIds.length = 0;
            // grab the values selected, the values being the ids
            Array.from(selectCorrectIds.selectedOptions).forEach(option => {
                this.correctIds.push(parseInt(option.value));
            });
            this.updateWordOutput(content, "correct", this.correctIds);
        });

        // keep reference to the Action id
        selectWrongIds.addEventListener("input", () => {
            this.wrongIds.length = 0;
            // grab the values selected, the values being the ids
            Array.from(selectWrongIds.selectedOptions).forEach(option => {
                this.wrongIds.push(parseInt(option.value));
            });
            this.updateWordOutput(content, "wrong", this.wrongIds);
        });

        // if the Timeline should resume play
        const checkboxResumeOnWrong = <HTMLInputElement> content.querySelector(`input[name="resume-on-wrong"]`);
        checkboxResumeOnWrong.addEventListener("input", () => {
            this.resumeOnWrong = checkboxResumeOnWrong.checked;
        });

        this.applyWordEvents(content, "correct");
        this.applyWordEvents(content, "wrong");

        // init events
        selectCorrectIds.dispatchEvent(new CustomEvent("input"));
        selectWrongIds.dispatchEvent(new CustomEvent("input"));
    }

    applyWordEvents(content: HTMLElement, what: string) {
        const outputRow = content.querySelector(`.row[data-for-type="${what}-text"]`) as HTMLElement;
        const outputElement = <HTMLElement> outputRow.querySelector("[data-contenteditable]");

        // reset offsets
        outputRow.querySelector("span.material-icons")?.addEventListener("click", () => {
            (what === "correct" ? this.correctOffsets : this.wrongOffsets).length = 0;
            this.dispatchOutputElements(content);
        });

        // updates with the span tags, highlighting the section
        outputElement.addEventListener("change", () => {
            const id = what === "correct" ? this.correctIds[0] : this.wrongIds[0];
            let otherAction = this.timeline.getAction(id) as unknown as AddElement;

            if (!otherAction || !otherAction.hasText) {
                return;
            }

            // set initial text
            // console.log("new lines", otherAction!.getTextElement!.innerText.replace(/\n/g, "<br/>"));
            outputElement.innerHTML = otherAction!.getTextElement!.innerText;

            [this.correctOffsets, this.wrongOffsets].forEach(offsets => {
                offsets.forEach(offset => {
                    const selected = (what === "correct" && offsets === this.correctOffsets) || (what === "wrong" && offsets === this.wrongOffsets);

                    // don't bother showing the other, they don't match
                    if (!selected && !this.isSameTextElement) {
                        return;
                    }

                    const span = document.createElement("span");
                    span.classList.add(selected ? "selected" : "selected2");

                    try {
                        const range = getTextRange(outputElement, offset[0], offset[1]);
                        span.appendChild(range.extractContents());
                        range.insertNode(span);
                    }
                    catch (e) {}
                });
            });
        });

        // makes sure there are no overlaps
        function checkOffset(array: Array<Array<number>>, start: number, end: number) {
            return array.filter(offset => offset[1] >= start && offset[0] <= end).length === 0;
        }

        // after the user selected a range
        outputElement.addEventListener("mouseup", () => {
            // get new selection
            const sel = window.getSelection();
            const range = sel!.rangeCount > 0 ? sel?.getRangeAt(0) : null;
            if (!range) {
                return;
            }
            
            try {
                // https://stackoverflow.com/questions/4811822/get-a-ranges-start-and-end-offsets-relative-to-its-parent-container

                // this piece of code is needed because the `Range.offsetStart` wasn't referencing the parent element
                // it was being offset (like the name in the property) from the previous sibling
                let length = range.toString().length; // *
                let preCaretRange = range.cloneRange();
                preCaretRange.selectNodeContents(outputElement);
                preCaretRange.setEnd(range.endContainer, range.endOffset);
                let startOffset = preCaretRange.toString().length - length; // *
                let endOffset = startOffset + length;

                // console.log(this.correctOffsets, startOffset, endOffset);

                // now we need to check for overlaps
                let append = startOffset !== endOffset && startOffset >= 0;

                // compare with the same element
                if (this.isSameTextElement) {
                    if (!checkOffset(this.correctOffsets, startOffset, endOffset)) {
                        append = false;
                    }
                    if (!checkOffset(this.wrongOffsets, startOffset, endOffset)) {
                        append = false;
                    }
                }
                // compare individually
                else {
                    if (!checkOffset(what === "correct" ? this.correctOffsets : this.wrongOffsets, startOffset, endOffset)) {
                        append = false;
                    }
                }

                // append to array
                if (append) {
                    (what === "correct" ? this.correctOffsets : this.wrongOffsets).push([startOffset, endOffset]);
                }

                this.dispatchOutputElements(content);
            }
            catch (e) {}
        });

        // dispatch the event
        outputElement.dispatchEvent(new CustomEvent("change"));
    }

    updateWordOutput(content: HTMLElement, what: string, actionIds: Array<number>) {
        let showWords = false;

        // show the word selection, maybe
        if (actionIds.length === 1 && actionIds[0] !== 0) {
            let otherAction = this.timeline.getAction(actionIds[0]) as unknown as AddElement;
            if (otherAction.hasText) {
                showWords = true;
            }
        }

        // show/hide the word selection
        const outputRow = content.querySelector(`.row[data-for-type="${what}-text"]`) as HTMLElement;
        const outputElement = <HTMLElement> outputRow.querySelector("[data-contenteditable]");
        outputRow.style.display = showWords ? "block" : "none";

        if (!showWords) {
            (what === "correct" ? this.correctOffsets : this.wrongOffsets).length = 0;
        }

        outputElement.dispatchEvent(new CustomEvent("change"));
    }

    dispatchOutputElements(content: HTMLElement) {
        content.querySelector(`.row[data-for-type="correct-text"] [data-contenteditable]`)?.dispatchEvent(new CustomEvent("change"));
        content.querySelector(`.row[data-for-type="wrong-text"] [data-contenteditable]`)?.dispatchEvent(new CustomEvent("change"));
    }

    get isSameTextElement() {
        return this.correctIds[0] === this.wrongIds[0] && this.correctIds.length === 1 && this.wrongIds.length === 1;
    }
    
    override get isValid() {
        let flag = super.isValid;

        // need something
        if (!this.correctIds.length || !this.wrongIds.length) {
            flag = false;
        }
        if (!validActionRelations(this, this.correctIds.concat(this.wrongIds))) {
            flag = false;
        }

        return flag;
    }

    override export() {
        return Object.assign(super.export(), {
            correctIds: this.correctIds,
            correctOffsets: this.correctOffsets,
            wrongIds: this.wrongIds,
            wrongOffsets: this.wrongOffsets,
            resumeOnWrong: this.resumeOnWrong
        });
    }
}