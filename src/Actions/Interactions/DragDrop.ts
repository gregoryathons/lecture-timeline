import { Action } from "../..";
import AddElement from "../Elements/AddElement";
import { createVisibleDropDown, swapNodes, validActionRelations } from "../utils";
import BaseInteraction from "./BaseInteraction";
import { breakApartText } from "../Elements/utils";
import { TimelineEvent } from "../../Const";
import { wait } from "../../utils";
import { log } from "../../Timeline";


interface IDraggableData {
    id: number;
    otherId: number; // the Action and element
    elements: Array<HTMLElement>; // if it was a text element and the type was words
};

interface IDroppableData {
    id: number;
    otherId: number; // the Action and element
    dragId: number; // the correct Draggable
    elements: Array<HTMLElement>;
    nodeIndex: number; // the specific node to become the desired droppable target
    wordIndex: number; // the correct word index IF `draggable.type` is "word"
};

interface IDragItem {
    item: HTMLElement | null;
    prevDropBox: HTMLElement | null;
    lastItem: HTMLElement | null;
};

interface IElementRect {
    element: HTMLElement;
    rect: DOMRect;
};

enum DragType {
    Element = "element",
    Word = "word"
};

export default class DragDrop extends BaseInteraction {

    dragType = DragType.Element;//  = "element";
    shuffle = false;
    orderBy: Array<number> = [];
    draggables: Array<IDraggableData> = [];
    droppables: Array<IDroppableData> = [];

    dragItem = {} as IDragItem;
    originalText: {[actionId: number]: string} = {}; // keeps record of the original text of the Action element, before this class disrupts the text
    checkingSpaceSpot = false;

    constructor(data: any | null = null) {
        super(data);

        this.dragType = data?.dragType || this.dragType;
        this.shuffle = data && typeof data.shuffle === "boolean" ? data.shuffle : this.shuffle;
        this.orderBy = data?.orderBy || this.orderBy;
        this.draggables = data?.draggables || this.draggables;
        this.droppables = data?.droppables || this.droppables;
    }

    override reset() {
        this.interactive = false;
        this.originalText = {};
        this.checkingSpaceSpot = false;
        super.reset();
    }

    override onStart(direction: number) {
        // stop (and lock) the timeline if we are moving
        if (direction > 0) {
            this.timeline.pause(this);
            this.interactive = true;
        }
        
        this.checkingSpaceSpot = false;
        let didShuffle = false;
        let maxWidth = 0;

        // set the draggables
        this.draggables.forEach(draggable => {
            // find the action
            const action = this.timeline.getAction(draggable.otherId) as unknown as AddElement;
            if (!action) {
                return;
            }

            // create the array of draggable elements
            draggable.elements = [];

            // admin wants the words to be draggable
            if (this.dragType === DragType.Word) {
                // make each word draggable
                const words = Array.from(action.element.querySelectorAll(".word"));
                words.forEach((word, index) => {
                    const wordElement = word as HTMLElement;

                    // is the last word actually a punctuation, don't make it draggable
                    if (!wordElement.innerText || /\W/g.test(wordElement.innerText)) {
                        return;
                    }

                    wordElement.draggable = true;
                    wordElement.classList.add("draggable");
                    wordElement.dataset.sortIndex = index.toString(); // track the sort index

                    // it's also a droppable if we don't have any droppables
                    if (!this.droppables.length) {
                        // so add the class
                        wordElement.classList.add("droppable");
                    }

                    // determine the max width
                    const rect = word.getBoundingClientRect();
                    maxWidth = Math.max(rect.width, maxWidth);

                    // add to array
                    // direction > 0 && draggable.elements.push(wordElement);
                    draggable.elements.push(wordElement);
                });

                // shuffle the order if no droppables
                if (!this.droppables.length) {
                    // do a random shuffle, or...
                    if (this.shuffle) {
                        // shake it up
                        const temp = [...draggable.elements.shuffle()];
                        while (temp.length) {
                            swapNodes(temp[0], temp.pop()!);
                        }
                        didShuffle = true;
                    }
                }
                // we need to resize the words, ignore the punctuation words
                else {
                    words.forEach(word => {
                        const wordElement = word as HTMLElement;
                        if (!/\W/g.test(wordElement.innerText)) {
                            wordElement.style.width = `${maxWidth}px`;
                        }
                    });
                }

                // do a manual shuffle
                // draggable.elements.sort((a, b) => this.orderBy[draggable.elements.indexOf(a)] - this.orderBy[draggable.elements.indexOf(b)]);
                if (this.orderBy.length > 0) {
                    const swappedElements: Array<HTMLElement> = [];

                    draggable.elements.forEach((element, index) => {
                        // wrong index
                        if (parseInt(element.dataset.sortIndex!) !== this.orderBy[index]) {
                            // hasn't been swapped yet
                            if (swappedElements.indexOf(element) < 0) {
                                const otherElement = draggable.elements.find(element => parseInt(element.dataset.sortIndex!) === this.orderBy[index])!;
                                swapNodes(element, otherElement);
                                // remember the elements we swapped
                                swappedElements.push(element, otherElement);
                            }
                        }
                    });
                    
                    didShuffle = true;
                }
            }
            // entire element is draggable
            else if (action.element instanceof HTMLElement) {
                action.element.draggable = true;
                action.element.classList.add("draggable");

                // are we sorting? then we need to add another class
                if (!this.droppables.length) {
                    action.element.classList.add("droppable");
                }

                // get the max width from the elements
                if (action.hasText) {
                    const rect = action.element.getBoundingClientRect();
                    maxWidth = Math.max(rect.width, maxWidth);
                }

                // add to array
                // direction > 0 && draggable.elements.push(action.element);
                draggable.elements.push(action.element);
            }
        });

        // shuffle the elements, this should only apply for the independent elements because we shuffled the words earlier
        if (!didShuffle) {
            // shake it up
            if (this.shuffle) {
                let temp: Array<HTMLElement> = [];
                this.draggables.forEach((draggable, index) => {
                    // track the sort index
                    if (draggable.elements.length) {
                        draggable.elements[0].dataset.sortIndex = index.toString();
                    }
                    // to be shaken, not stirred
                    temp = temp.concat(draggable.elements);
                });
                temp.shuffle();

                // swap the position around, it's up to the admin to make sure they are aligned
                while (temp.length) {
                    const i1 = temp[0];
                    const i2 = temp.pop()!;
                    const inset1 = i1.style.inset;
                    const inset2 = i2.style.inset;
                    i1.style.inset = inset2;
                    i2.style.inset = inset1;
                    swapNodes(i1, i2);
                }
            }
            // manual shuffle
            else if (this.orderBy.length) {
                const swappedDraggables: Array<number> = [];

                this.draggables.forEach((draggable, index) => {
                    // wrong index
                    if (draggable.id !== this.orderBy[index]) {
                        // hasn't been swapped yet
                        if (swappedDraggables.indexOf(draggable.id) < 0) {
                            const otherDraggable = this.draggables.find(draggable => draggable.id === this.orderBy[index])!;

                            const i1 = draggable.elements[0];
                            const i2 = otherDraggable.elements[0];
                            const inset1 = i1.style.inset;
                            const inset2 = i2.style.inset;
                            i1.style.inset = inset2;
                            i2.style.inset = inset1;
                            swapNodes(i1, i2);

                            // remember the draggables we swapped
                            swappedDraggables.push(draggable.id, otherDraggable.id);
                        }
                    }
                });
            }
        }

        // now let's set the droppables
        this.droppables.forEach(droppable => {
            const action = this.timeline.getAction(droppable.otherId) as unknown as AddElement;
            if (!action) {
                return;
            }

            // create the array
            droppable.elements = [];
            
            if (action.hasText) {
                // keep track of the original text, because this class disrupts the original
                if (typeof this.originalText[droppable.otherId] === "undefined" && action.element instanceof HTMLElement) {
                    this.originalText[droppable.otherId] = action.element.innerText;
                }

                // grab the node we will be making droppable
                const node = action.element.childNodes[droppable.nodeIndex] as HTMLElement;
                if (!node) {
                    return;
                }

                // spaces are special, this is a text type
                // https://developer.mozilla.org/en-US/docs/Web/API/Node/nodeType
                if (node.nodeType === 3) {
                    // mark we are
                    this.checkingSpaceSpot = true;

                    // add droppable and no-hint to all the words because they will be used for the `onInteraction(dragover)`
                    Array.from(action.element.querySelectorAll(".word")).forEach(word => {
                        const wordElement = word as HTMLElement;

                        // add as a droppable 
                        if (wordElement.innerText) {//} && !/\W/g.test(wordElement.innerText)) {
                            wordElement.classList.add("droppable", "no-hint");
                            // add to the array
                            // direction > 0 && droppable.elements.push(wordElement);
                            droppable.elements.push(wordElement);
                        }
                    });
                }
                // normal droppable
                else {
                    node.classList.add("droppable");

                    node.innerHTML = "&nbsp;"; // gives it height
                    node.style.width = `${maxWidth}px`; // TODO - make it percentage based

                    // add to the array
                    // direction > 0 && droppable.elements.push(node);
                    droppable.elements.push(node);
                }
            }
        });

        // set id's to our elements
        this.draggables.forEach(draggable => {
            draggable.elements.forEach(element => element.dataset.draggableId = draggable.id.toString());
        });
        this.droppables.forEach(droppable => {
            droppable.elements.forEach(element => element.dataset.droppableId = droppable.id.toString());
        });
    }

    override onEnd() {
        if (this.timeline.isLocked) {
            return;
        }

        this.interactive = false;

        // removes the classes and datasets
        const clearUp = (element: HTMLElement) => {
            element.draggable = false;
            element.classList.remove("draggable", "picked", "droppable", "no-hint");

            delete element.dataset.draggableId;
            delete element.dataset.droppableId;
            delete element.dataset.sortIndex;
            delete element.dataset.wordIndex;
        };

        this.draggables.forEach(draggable => {
            const action = this.timeline.getAction(draggable.otherId) as unknown as AddElement;
            if (action && action.element instanceof HTMLElement) {
                clearUp(action.element);
            }
            if (Array.isArray(draggable.elements)) {
                draggable.elements.forEach(element => clearUp(element));
                draggable.elements.length = 0;
            }
        });

        this.droppables.forEach(droppable => {
            const action = this.timeline.getAction(droppable.otherId) as unknown as AddElement;
            if (action && action.element instanceof HTMLElement) {
                clearUp(action.element);

                // replace the innerHTML with the original text
                action.element.innerHTML = breakApartText(this.originalText[droppable.otherId]);
            }
            if (Array.isArray(droppable.elements)) {
                droppable.elements.forEach(element => clearUp(element));
                droppable.elements.length = 0;
            }
        });
    }

    onInteraction(event: Event) {
        const target = event.target as HTMLElement;

        switch (event.type) {
            case TimelineEvent.ActionInteractionCheck: {
                this.checkAnswer();
                break;
            }
            case "dragstart": {
                // we already have an item being dragged
                if (this.dragItem.item) {
                    event.preventDefault();
                    return;
                }

                const dragItem = target.closest(".draggable") as HTMLElement;
                if (dragItem) {
                    // keep reference of the Action id, this is used for when we are dragging a word in an element
                    const parentElement = dragItem.closest("[data-action-id]") as HTMLElement;
                    if (parentElement !== dragItem) {
                        dragItem.dataset.actionId = parentElement.dataset.actionId;
                        // dragItem.dataset.wordIndex = Array.from(parentElement.childNodes).indexOf(dragItem).toString();
                        dragItem.dataset.wordIndex = Array.from(parentElement.children).indexOf(dragItem).toString();
                    }

                    // set our current drag item, and our previous spot
                    this.dragItem.item = dragItem;
                    this.dragItem.prevDropBox = dragItem.closest(".droppable") as HTMLElement;
                }
                
                break;
            }
            case "dragover": {
                event.preventDefault();
                // (event as DragEvent).dataTransfer!.dropEffect = "move"; // why doesn't this work!?

                const currentItem = this.dragItem.item;
                const overItem = target.closest(".droppable") as HTMLElement;

                // not over a droppable
                if (!currentItem || !overItem || currentItem === overItem) {
                    return;
                }
                // currently animating, don't proceed
                if (currentItem.getAnimations().length || overItem.getAnimations().length) {
                    return;
                }

                // make the draggables sortable amongst themselves, OR if we are adding a element/word into the sentence
                if (!this.droppables.length || this.checkingSpaceSpot) {
                    // get bounds
                    const currentRect = currentItem.getBoundingClientRect();
                    const overRect = overItem.getBoundingClientRect();
                    const padding = this.checkingSpaceSpot ? 0 : 0.2;

                    // need to be more centered with the item
                    const { clientX, clientY } = event as MouseEvent;
                    if (clientX < overRect.x + overRect.width * padding || clientX > overRect.x + overRect.width * (1 - padding)) {
                        this.dragItem.lastItem = null;
                        return;
                    }
                    if (clientY < overRect.y + overRect.height * padding || clientY > overRect.y + overRect.height * (1 - padding)) {
                        this.dragItem.lastItem = null;
                        return;
                    }

                    // if it's a word, we need to figure out the future position
                    if (overItem.classList.contains("word")) {
                        // inject the word between two words
                        if (this.checkingSpaceSpot) {
                            // first or last word, don't bother
                            if ((clientX < overRect.x + overRect.width * 0.5 && overItem.parentElement?.firstElementChild === overItem) || 
                                (clientX > overRect.x + overRect.width * 0.5 && overItem.parentElement?.lastElementChild === overItem)) {
                                return;
                            }

                            // gets the rects before replacement
                            const elementRects: Array<IElementRect> = [];
                            const getElementRects = () => {
                                let children = overItem.parentElement!.children;
                                for (let i = 0; i < children.length; i++) {
                                    elementRects.push({
                                        element: children[i],
                                        rect: children[i].getBoundingClientRect()
                                    } as IElementRect);
                                }
                            };

                            // left side
                            if (clientX < overRect.x + overRect.width * 0.5) {
                                if (overItem.previousElementSibling?.previousElementSibling === currentItem) {
                                    return;
                                }

                                getElementRects();

                                // removes the span.space to the right from the previous position
                                if (currentItem.parentElement === overItem.parentElement) {
                                    currentItem.nextSibling?.remove();
                                }

                                overItem.parentElement?.insertBefore(currentItem, overItem);
                                currentItem.insertAdjacentText("afterend", " ");
                            }
                            // right side
                            else {
                                if (overItem.nextElementSibling?.nextElementSibling === currentItem) {
                                    return;
                                }

                                getElementRects();

                                // removes the span.space to the left from the previous position
                                if (currentItem.parentElement === overItem.parentElement) {
                                    currentItem.previousSibling?.remove();
                                }

                                overItem.parentElement?.insertBefore(currentItem, overItem.nextSibling);
                                currentItem.insertAdjacentText("beforebegin", " ");
                            }

                            // animate
                            Array.from(overItem.parentElement!.childNodes).forEach(node => {
                                const element = node as HTMLElement;
                                const elementRect = elementRects.find(elementRect => elementRect.element === element);
                                if (elementRect) {
                                    const rect = element.getBoundingClientRect();

                                    let top = elementRect.rect.top - rect.top;
                                    let left = elementRect.rect.left - rect.left;
                                    element.animate([{ transform: `translate(${left}px, ${top}px)` }], { duration: 120, direction: "reverse" });
                                }
                            });
                        }
                        // change the order of the words
                        else {
                            // if it was the same last item, don't continue
                            if (overItem === this.dragItem.lastItem) {
                                return;
                            }
                            
                            // swap now, and get potential new position
                            swapNodes(currentItem, overItem);

                            const currentNext = currentItem.getBoundingClientRect();
                            const overNext = overItem.getBoundingClientRect();

                            // swap back
                            swapNodes(currentItem, overItem);

                            // move the new (over) element to the current
                            let top = overNext.top - overRect.top;
                            let left = overNext.left - overRect.left;
                            overItem.animate([{ transform: `translate(${left}px, ${top}px)` }], { duration: 120 });

                            // move the current to the new (over) element
                            top = currentNext.top - currentRect.top;
                            left = currentNext.left - currentRect.left;
                            currentItem.animate([{ transform: `translate(${left}px, ${top}px)` }], { duration: 120 }).addEventListener("finish", () => {
                                swapNodes(currentItem, overItem);
                            });
                        }
                    }
                    // user is swapping with an independent element
                    else if (overItem.classList.contains("element")) {
                        // if it was the same last item, don't continue
                        if (overItem === this.dragItem.lastItem) {
                            return;
                        }
                        
                        const currentCSS = window.getComputedStyle(currentItem);
                        const overCSS = window.getComputedStyle(overItem);

                        let top = parseFloat(currentCSS.top) - parseFloat(overCSS.top);
                        let left = parseFloat(currentCSS.left) - parseFloat(overCSS.left);
                        overItem.animate([{ transform: `translate(${left}px, ${top}px)` }], { duration: 120 });

                        top = parseFloat(overCSS.top) - parseFloat(currentCSS.top);
                        left = parseFloat(overCSS.left) - parseFloat(currentCSS.left);
                        currentItem.animate([{ transform: `translate(${left}px, ${top}px)` }], { duration: 120 }).addEventListener("finish", () => {
                            const temp = overItem.style.inset;
                            overItem.style.inset = currentItem.style.inset;
                            currentItem.style.inset = temp;

                            // so our `checkAnswer` will get the proper order
                            swapNodes(currentItem, overItem);
                        });
                    }

                    // dispatch event
                    this.dispatchEvent(new CustomEvent(TimelineEvent.ActionInteraction, {
                        bubbles: true,
                        detail: {
                            type: this.type
                        }
                    }));
                }
                // stand alone drag and drop to a droppable
                else {
                    // this is better because `dragenter` will only execute once, and this catches child elements
                    overItem.classList.add("over");
                }

                // set as the new item
                this.dragItem.lastItem = overItem;

                break;
            }
            case "dragenter": {
                event.preventDefault();

                break;
            }
            case "dragleave": {
                event.preventDefault();

                const leaveItem = target.closest(".droppable");
                leaveItem?.classList.remove("over");

                break;
            }
            case "dragend": {
                event.preventDefault();

                this.dragItem.item?.classList.remove("picked");
                this.dragItem.item = this.dragItem.lastItem = null;

                break;
            }
            case "drag": {
                event.preventDefault();

                // for some reason, this line can't be set in `dragstart`?!
                this.dragItem.item?.classList.add("picked");

                break;
            }
            case "drop": {
                event.preventDefault();

                // don't need to do anything, the `dragover` event was swapping around the elements
                if (!this.droppables.length || this.checkingSpaceSpot) {
                    return;
                }

                const dragItem = this.dragItem.item as HTMLElement;
                const actionId = parseInt(dragItem.dataset.actionId!);
                const origElement = this.timeline.content.querySelector(`[data-action-id="${actionId}"]`) as HTMLElement;
                const prevDropBox = this.dragItem.prevDropBox;

                // good, we landed in the droppable
                const dropBox = target.closest(".droppable");
                if (dropBox) {
                    dropBox.classList.remove("over");

                    // was it on text?
                    if (dropBox.classList.contains("word")) {
                        // was there a previous element/word in it's place?
                        const prevItem = (dropBox.querySelector(".element") || dropBox.querySelector(".word")) as HTMLElement;
                        if (prevItem) {
                            // swap with previous drop, user is swapping between the two
                            if (prevDropBox) {
                                prevDropBox.innerHTML = "";
                                prevDropBox.appendChild(prevItem);
                            }
                            // tell the original to show itself
                            else {
                                // entire element
                                if (prevItem.classList.contains("element")) {
                                    this.timeline.content.querySelector(`.picked[data-action-id="${prevItem.dataset.actionId}"]`)?.classList.remove("picked");
                                    // origElement.classList.remove("picked");
                                }
                                // was it a word?
                                else if (prevItem.classList.contains("word")) {
                                    Array.from(origElement.querySelectorAll(".word.picked")).forEach(word => {
                                        const wordElement = word as HTMLElement;
                                        if (wordElement.textContent === prevItem.textContent) {
                                            wordElement.classList.remove("picked");
                                        }
                                    });
                                }
                            }
                        }
                        // empty out ourself from the previous droppable
                        else if (prevDropBox) {
                            if (prevDropBox.querySelector(`[data-action-id="${actionId}"]`)) {
                                prevDropBox.innerHTML = "&nbsp;";
                            }
                        }

                        // clone the element and place it inside
                        // we don't move the original element, because we would lose it's position
                        const cloned = dragItem.cloneNode(true) as HTMLElement;
                        cloned.classList.remove("picked");
                        cloned.style.inset = cloned.style.width = cloned.style.height = "";
                        cloned.draggable = true;
                        dropBox.innerHTML = "";
                        dropBox.appendChild(cloned);
                    }

                    // dispatch event
                    this.dispatchEvent(new CustomEvent(TimelineEvent.ActionInteraction, {
                        bubbles: true,
                        detail: {
                            type: this.type
                        }
                    }));
                }
                // didn't hit anything
                else {
                    // was it in the droppable?
                    if (prevDropBox) {
                        prevDropBox.innerHTML = "&nbsp;";
                    }

                    if (dragItem.classList.contains("element")) {
                        this.timeline.content.querySelector(`.picked[data-action-id="${dragItem.dataset.actionId}"]`)?.classList.remove("picked");
                    }
                    // was it a word?
                    else if (dragItem.classList.contains("word")) {
                        Array.from(origElement.querySelectorAll(".word.picked")).forEach(word => {
                            const wordElement = word as HTMLElement;
                            if (wordElement.textContent === dragItem.textContent) {
                                wordElement.classList.remove("picked");
                            }
                        });
                    }
                }

                // empty
                this.dragItem.item = this.dragItem.prevDropBox = null;

                break;
            }
        }
    }

    checkAnswer() {
        // assume the user got it all
        let correct = true;

        // if there are any droppables, we need to cross check those with the draggables
        if (this.droppables.length) {
            this.droppables.forEach(droppable => {
                // check if the user correctly set the draggables into the correct position
                if (droppable.elements.length === 1) {
                    const dragElement = <HTMLElement> droppable.elements[0].querySelector(".draggable[data-draggable-id]");
                    if (dragElement) {
                        // id's don't match
                        if (dragElement.dataset.draggableId !== droppable.dragId.toString()) {
                            correct = false;
                        }
                        // was the draggable a word? check if this is the correct word in the correct spot
                        if (dragElement.classList.contains("word") && droppable.wordIndex.toString() !== dragElement.dataset.wordIndex) {
                            correct = false;
                        }
                    }
                    // doesn't exist
                    else {
                        correct = false;
                    }
                }
                // multiple drops on a droppable? must be space placement
                // the `onStart` created multiple droppables if a `space` index was selected
                else if (droppable.elements[0]?.parentElement === droppable.elements[1]?.parentElement) {
                    // we do +1 because the desired "should" be to the right of it
                    // const node = droppable.elements[0].parentElement!.childNodes[droppable.childIndex + 1] as HTMLElement;
                    const kid = droppable.elements[0].parentElement!.childNodes[droppable.nodeIndex + 1] as HTMLElement;
                    if (kid.dataset.draggableId !== droppable.dragId.toString()) {
                        correct = false;
                    }
                }
            });
        }
        // it was a sortable, check the order
        else {
            // was it a word? was it draggable and droppable?
            if (this.draggables.length === 1) {
                const element = this.draggables[0].elements[0];
                Array.from(element.parentElement!.querySelectorAll(".word.draggable.droppable")).forEach((element, index) => {
                    if ((element as HTMLElement).dataset.sortIndex !== index.toString()) {
                        correct = false;
                    }
                });
            }
            // check the draggables
            else {
                Array.from(this.timeline.content.querySelectorAll(".element.draggable.droppable")).forEach((element, index) => {
                    if ((element as HTMLElement).dataset.sortIndex !== index.toString()) {
                        correct = false;
                    }
                });
            }
        }

        console.log("correct", correct);

        // dispatch the event
        this.dispatchEvent(new CustomEvent(TimelineEvent.ActionInteractionResults, {
            bubbles: true,
            detail: {
                type: this.type,
                correct: correct
            }
        }));

        // play the Track
        const trackId = this.trackIds[correct ? 0 : 1];
        if (trackId) {
            this.playTrack(trackId);
        }
        // play Clip
        else {
            this.playClip(this.resourceNames[correct ? 0 : 1], () => this.timeline.resume(true));
        }
    }

    override editorHTML(content: HTMLElement) {
        const endHTML = `
            <hr />
            <div class="row">
                <div class="three columns">
                    <strong>Draggables</strong>
                </div>
                <div class="nine columns">
                    <button class="button button-green add-draggable u-pull-right">
                        <span class="material-icons">add</span>
                    </button>
                </div>
            </div>
            <div class="row draggables">
                <!-- will be populated -->
            </div>
            <div class="row" data-drag-type="true">
                <div class="three columns">
                    Drag by Type
                </div>
                <div class="nine columns">
                    <select name="drag-type"></select>
                </div>
            </div>
            <div class="row" data-order="true">
                <div class="three columns">
                    Order
                </div>
                <div class="nine columns">
                    <label style="margin-bottom:0; font-weight:300">
                        <input type="checkbox" name="shuffle" ${this.shuffle ? "checked" : ""} /> Shuffle Items
                    </label>
                    <div>
                        <!-- will be populated -->
                    </div>
                </div>
            </div>

            <hr />
            <div class="row">
                <div class="three columns">
                    <strong>Droppables</strong>
                </div>
                <div class="nine columns">
                    <button class="button button-green add-droppable u-pull-right">
                        <span class="material-icons">add</span>
                    </button>
                </div>
            </div>
            <div class="row droppables">
                <!-- will be populated -->
            </div>
        `;

        // parent will build it
        super.editorHTML(content, "", "", endHTML);

        // add a Draggable
        const buttonDraggable = <HTMLElement> content.querySelector(`button.add-draggable`);
        buttonDraggable.addEventListener("click", () => {
            // add the draggable
            const draggable = { id: Date.now() } as IDraggableData;
            this.draggables.push(draggable);

            this.addDraggableInfo(content.querySelector(".draggables") as HTMLElement, draggable);

            this.updateDroppablesDraggablesList(content);
        });

        // add a Droppable
        const buttonDroppable = <HTMLElement> content.querySelector(`button.add-droppable`);
        buttonDroppable.addEventListener("click", () => {
            // add the droppable
            const droppable = { id: Date.now() } as IDroppableData;
            this.droppables.push(droppable);

            this.addDroppableInfo(content.querySelector(".droppables") as HTMLElement, droppable);

            this.updateDroppablesDraggablesList(content);
        });

        // set the drag type
        const inputDragType = <HTMLSelectElement> content.querySelector(`select[name="drag-type"]`);
        inputDragType.addEventListener("input", () => {
            const type = inputDragType.value as keyof typeof DragType;
            this.dragType = DragType[type];
        });
        for (const type in DragType) {
            const option = document.createElement("option");
            option.text = type;
            option.value = type;
            option.selected = type.toLowerCase() === this.dragType;
            inputDragType.options.add(option);
        }

        // shuffle it?
        const inputShuffle = <HTMLInputElement> content.querySelector(`input[name="shuffle"]`);
        inputShuffle.addEventListener("input", () => {
            this.shuffle = inputShuffle.checked;
        });

        content.addEventListener("input", async () => {
            await wait(400);
            this.updateDraggableType(content.querySelector(`[data-drag-type]`) as HTMLElement);
            this.updateDraggableOrder(content.querySelector(`[data-order]`) as HTMLElement);
            this.updateDroppablesDraggablesList(content);
        });
        
        // populate
        this.draggables.forEach(draggable => this.addDraggableInfo(content.querySelector(".draggables") as HTMLElement, draggable));
        this.droppables.forEach(droppable => this.addDroppableInfo(content.querySelector(".droppables") as HTMLElement, droppable));

        // dispatch
        content.dispatchEvent(new CustomEvent("input"));
    }

    addDraggableInfo(content: HTMLElement, draggable: IDraggableData) {
        // add the div
        const div = document.createElement("div");
        div.dataset.draggableId = draggable.id.toString();
        div.classList.add("row", "draggable-options");

        // the markup
        div.innerHTML = `
            <div class="row">
                <div class="three columns">
                    Element
                </div>
                <div class="nine columns" style="display:flex">
                    <select name="drag-id" class="u-full-width"></select>
                    <button class="button button-red remove-draggable u-pull-right" style="margin-left:10px">
                        <span class="material-icons">delete</span>
                    </button>
                </div>
            </div>
        `;

        // add to the draggables list
        content.appendChild(div);

        const selectDragId = <HTMLSelectElement> div.querySelector(`select[name="drag-id"]`);
        createVisibleDropDown(this, selectDragId, (action: Action) => {
            return draggable.otherId === action.id ? 1 : 0;
        });

        // select the drag Action
        selectDragId.addEventListener("input", () => {
            draggable.otherId = parseInt(selectDragId.value);
        });

        // remove the draggable
        (div.querySelector("button.remove-draggable") as HTMLElement).addEventListener("dblclick", () => {
            // remove from array
            this.draggables.splice(this.draggables.indexOf(draggable), 1);
            // remove self
            div.remove();
            // dispatch
            content.dispatchEvent(new CustomEvent("input", { bubbles: true }));
        });

        selectDragId.dispatchEvent(new CustomEvent("input"));
    }

    addDroppableInfo(content: HTMLElement, droppable: IDroppableData) {
        // add the div
        const div = document.createElement("div");
        div.dataset.droppableId = droppable.id.toString();
        div.classList.add("row", "droppable-options");

        // the markup
        div.innerHTML = `
            <div class="row">
                <div class="three columns">
                    Element
                </div>
                <div class="nine columns" style="display: flex">
                    <select name="drop-id" class="u-full-width"></select>
                    <button class="button button-red remove-droppable u-pull-right" style="margin-left: 10px">
                        <span class="material-icons">delete</span>
                    </button>
                </div>
            </div>
            <div class="row" data-droppable-location="true">
                <div class="three columns">
                    Location
                </div>
                <div class="nine columns text-selection location">
                    <!-- will be populated data-contenteditable="true" -->
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Correct
                </div>
                <div class="nine columns">
                    <select name="drag-id" class="u-full-width"></select>
                </div>
            </div>
            <div class="row" data-droppable-word="true">
                <div class="three columns">
                    Word
                </div>
                <div class="nine columns text-selection word" >
                    <!-- will be populated data-contenteditable="true" -->
                </div>
            </div>          
        `;

        // add to the droppables list
        content.appendChild(div);

        const selectDropId = <HTMLSelectElement> div.querySelector(`select[name="drop-id"]`);
        createVisibleDropDown(this, selectDropId, (action: Action) => {
            return droppable.otherId === action.id ? 1 : 0;
        });

        // select the drop Action
        selectDropId.addEventListener("input", () => {
            droppable.otherId = parseInt(selectDropId.value);
            this.updateDroppableField(div, droppable);
        });

        // select the drag
        const selectDragId = <HTMLSelectElement> div.querySelector(`select[name="drag-id"]`);
        selectDragId.addEventListener("input", () => {
            if (selectDragId.options.length) {
                droppable.dragId = parseInt(selectDragId.value);
                this.updateDroppableWordField(div, droppable);
            }
        });

        // clicking on the "word" and "space" classes
        // recall, we replaced our text nodes to span.space
        Array.from(div.querySelectorAll(".text-selection")).forEach(inputTextSelection => {
            (inputTextSelection as HTMLElement).addEventListener("click", (event: MouseEvent) => {
                const target = event.target as HTMLElement;

                // get the word, or space, and get the index
                const wordElement = target.closest(".word") as HTMLElement;
                const spaceElement = target.closest(".space") as HTMLElement;
                const index = Array.from(inputTextSelection.children).findIndex(kid => (kid as HTMLElement) === wordElement || (kid as HTMLElement) === spaceElement);

                // only if something is selected
                if (index >= 0) {
                    // remove class from all elements
                    Array.from(inputTextSelection.children).forEach(kid => kid.classList.remove("selected"));
                    // set class
                    inputTextSelection.children[index].classList.add("selected");

                    // if it's the location, only one thing can be selected
                    if (inputTextSelection.classList.contains("location")) {
                        droppable.nodeIndex = index;
                    }
                    else {
                        droppable.wordIndex = index;
                    }
                    
                    // dispatch
                    content.dispatchEvent(new CustomEvent("input", { bubbles: true }));
                }
            });
        });

        // remove the droppable
        (div.querySelector("button.remove-droppable") as HTMLElement).addEventListener("dblclick", (event: Event) => {
            // remove from array
            this.droppables.splice(this.droppables.indexOf(droppable), 1);
            // dispatch
            content.dispatchEvent(new CustomEvent("input", { bubbles: true }));
            // remove self
            div.remove();
        });

        selectDropId.dispatchEvent(new CustomEvent("input"));
        selectDragId.dispatchEvent(new CustomEvent("input"));
    }

    updateDraggableType(content: HTMLElement) {
        if (this.draggables.length > 1) {
            this.dragType = DragType.Element;
            content.style.display = "none";
        }
        else if (this.draggables.length === 1) {
            const type = (content.querySelector("select") as HTMLSelectElement)?.value as keyof typeof DragType;
            this.dragType = DragType[type];
            content.style.display = "";
        }
    }

    // creates the order interface
    updateDraggableOrder(content: HTMLElement) {
        if (this.draggables.length <= 1 && this.dragType === DragType.Element) {
            content.style.display = "none";
            return;
        }

        content.style.display = "";

        const divOrder = <HTMLElement> content.querySelector(".nine.columns div");

        // empties the `orderBy` array and div
        if (this.shuffle) {
            this.orderBy.length = 0;
            divOrder.innerHTML = "";
        }
        else {
            // Draggables must all be the same type
            if ((this.dragType === DragType.Element && this.draggables.length >= 1) || (this.dragType === DragType.Word && this.draggables.length === 1)) {
                divOrder.innerHTML = ""; // empty div
                let delay = 240;
                let currentItem: HTMLElement | null = null;
                let lastItem: HTMLElement | null = null;

                // creates the Draggable item for manual shuffle
                const createDraggableItem = (text: string, number: number) => {
                    const item = document.createElement("div");
                    item.classList.add("drag-order");
                    item.dataset.number = (number || "").toString();
                    item.draggable = true;
                    item.innerText = text;//draggable.otherId.toString();
                    divOrder.appendChild(item);

                    item.addEventListener("dragstart", (event) => {
                        currentItem = item;
                    });

                    item.addEventListener("dragover", (event) => {
                        const overItem = event.target as HTMLElement;

                        if (lastItem === currentItem) {
                            return;
                        }
                        if (currentItem?.getAnimations().length || overItem.getAnimations().length) {
                            return;
                        }
                        if (Date.now() < delay + 240) {
                            return;
                        }

                        if (currentItem && currentItem !== overItem) {
                            const currentRect = currentItem.getBoundingClientRect();
                            const overRect = overItem.getBoundingClientRect();

                            let top = currentRect.top - overRect.top;
                            let left = currentRect.left - overRect.left;
                            overItem.animate([{ transform: `translate(${left}px, ${top}px)` }], { duration: 120 });
                            
                            // move the current to the new (over) element
                            top = overRect.top - currentRect.top;
                            left = overRect.left - currentRect.left;
                            currentItem.animate([{ transform: `translate(${left}px, ${top}px)` }], { duration: 120 }).addEventListener("finish", () => {
                                swapNodes(currentItem!, overItem);
                            });

                            lastItem = overItem;
                            delay = Date.now();
                        }
                    });

                    item.addEventListener("dragend", (event) => {
                        currentItem = null;

                        // sets the order
                        this.orderBy.length = 0;
                        Array.from(divOrder.children!).forEach(kid => {
                            this.orderBy.push(parseInt((kid as HTMLElement).dataset.number!));
                        });

                        content.dispatchEvent(new CustomEvent("input", { bubbles: true }));
                    });

                    return item;
                };
                
                // with elements
                if (this.dragType === DragType.Element) {
                    if (this.orderBy.length) {
                        this.orderBy.forEach(id => {
                            const draggable = this.draggables.find(draggable => draggable.id === id)!;
                            createDraggableItem(draggable.otherId.toString(), draggable.id);
                        });
                    }
                    else {
                        this.draggables.forEach(draggable => createDraggableItem(draggable.otherId.toString(), draggable.id));
                    }
                }
                // with words
                else {
                    // loop through the Draggables and create the items
                    this.draggables.forEach(draggable => {
                        const action = this.timeline.getAction(draggable.otherId) as unknown as AddElement;
                        const words = Array.from(action.element.querySelectorAll(".word"));
                        words.forEach((word: any) => {
                            if (word.innerText && !/\W/g.test(word.innerText)) {
                                createDraggableItem(word.innerText, word.dataset.sortIndex);
                            }
                        });
                    });
                }
            }
            // not the same type
            else {
                divOrder.innerHTML = `Manual Shuffling Disabled`;
            }
        }
    }

    // shows the Action's text wrapped with a span element
    updateDroppableField(content: HTMLElement, droppable: IDroppableData) {
        const action = this.timeline.getAction(droppable.otherId) as unknown as AddElement;

        // get the droppable rows
        const rowText = Array.from(content.querySelectorAll("[data-droppable-location]"));
        rowText.forEach(row => (row as HTMLElement).style.display = "none");

        // a text type
        if (action && action.hasText) {
            rowText.forEach(row => (row as HTMLElement).style.display = "");

            // show the words
            const inputTextSelection = <HTMLElement> content.querySelector(".text-selection.location");
            inputTextSelection.innerHTML = breakApartText(this.originalText[action.id]);

            // wrap all the text nodes with a space
            Array.from(inputTextSelection.childNodes).forEach(node => {
                if (node.nodeType === 3) {
                    const span = document.createElement("span");
                    span.classList.add("space");
                    span.innerHTML = "&nbsp;"; // to give it height
                    node.parentElement?.replaceChild(span, node);
                }
            });

            // show the one selected
            if (typeof droppable.nodeIndex === "number") {
                // inputTextSelection.children[droppable.childIndex]?.classList.add("selected");
                (inputTextSelection.childNodes[droppable.nodeIndex] as HTMLElement).classList.add("selected");
            }
        }
    }

    // makes sure to update the droppables list with all the possible draggables
    updateDroppablesDraggablesList(content: HTMLElement) {
        const elements = Array.from(content.querySelectorAll(`.row.droppable-options`));
        elements.forEach(element => {
            const select = <HTMLSelectElement> (element as HTMLElement).querySelector(`select[name="drag-id"]`);
            select.options.length = 0;

            // get our droppable by the id
            const droppable = this.droppables.find(droppable => droppable.id === parseInt((element as HTMLElement).dataset.droppableId!));

            // empty by default
            const option = document.createElement("option");
            option.innerHTML = "Empty";
            option.value = "0";
            option.selected = true;
            select.appendChild(option);

            // append draggables
            this.draggables.forEach(draggable => {
                const action = this.timeline.getAction(draggable.otherId) as unknown as AddElement;
                if (!action) {
                    return;
                }

                const option = document.createElement("option");
                option.innerHTML = action.editorName();
                option.value = draggable.id.toString();
                option.selected = droppable ? droppable.dragId === draggable.id : false;
                select.appendChild(option);
            });

            // dispatch
            select.dispatchEvent(new CustomEvent("input"));
        });
    }

    // where the admin selects the desired word if `draggable.type` === "word"
    updateDroppableWordField(content: HTMLElement, droppable: IDroppableData) {
        // get the droppable rows
        const rowText = Array.from(content.querySelectorAll("[data-droppable-word]"));
        rowText.forEach(row => (row as HTMLElement).style.display = "none");

        // was a draggable selected?
        const draggable = this.draggables.find(draggable => draggable.id === droppable.dragId);

        if (draggable && this.dragType === DragType.Word) {
            const action = this.timeline.getAction(draggable.otherId) as unknown as AddElement;

            // a text type
            if (action && action.element instanceof HTMLElement && action.hasText) {
                rowText.forEach(row => (row as HTMLElement).style.display = "");

                // show the words
                const inputTextSelection = <HTMLElement> content.querySelector(".text-selection.word");
                inputTextSelection.innerHTML = breakApartText(action.element.innerText!);

                // show the one selected
                if (typeof droppable.wordIndex === "number") {
                    inputTextSelection.children[droppable.wordIndex]?.classList.add("selected");
                }
            }
        }
        // empty it
        else {
            droppable.wordIndex = -1;
        }
    }

    override get isValid() {
        let flag = super.isValid;

        const dragIds = this.draggables.map(draggable => draggable.otherId);
        const dropIds = this.droppables.map(droppable => droppable.otherId);
        if (!validActionRelations(this, dragIds.concat(dropIds))) {
            flag = false;
        }

        return flag;
    }

    override export() {
        // deep copy, also exclude the elements
        const draggables = JSON.parse(JSON.stringify(this.draggables));
        const droppables = JSON.parse(JSON.stringify(this.droppables));

        return Object.assign(super.export(), {
            dragType: this.dragType,
            shuffle: this.shuffle,
            orderBy: this.orderBy,
            draggables: draggables,
            droppables: droppables
        });
    }
}