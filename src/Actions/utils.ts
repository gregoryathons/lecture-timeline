import { Timeline, Action, Track, Clip } from "..";
import { IResource, IResourceImage, IResourceSound } from "../Loader";
import { trace } from "../Timeline";
import AddElement from "./Elements/AddElement";

// https://stackoverflow.com/questions/6240139/highlight-text-range-using-javascript
export function getTextRange(element: HTMLElement, start: number, end: number) {
    const range = document.createRange();
    const textNodes = getTextNodesIn(element);

    let foundStart = false;
    let charCount = 0;
    let charCountEnd = 0;

    for (let i = 0, textNode; textNode = textNodes[i++]; ) {
        charCountEnd = charCount + (textNode.textContent || "").length;
        if (!foundStart && start >= charCount && (start < charCountEnd || (start === charCountEnd && i <= textNodes.length))) {
            range.setStart(textNode, start - charCount);
            foundStart = true;
        }
        if (foundStart && end <= charCountEnd) {
            range.setEnd(textNode, end - charCount);
            break;
        }
        charCount = charCountEnd;
    }

    return range;
}

export function getTextNodesIn(node: Node, includeEmpty: boolean = true) {
    const textNodes: Array<Node> = [];
    // a text type
    if (node.nodeType === 3) {
        if (includeEmpty || (!includeEmpty && node.textContent)) {
            textNodes.push(node);
        }
    }
    // keep looping
    else {
        for (let i = 0; i < node.childNodes.length; i++) {
            textNodes.push.apply(textNodes, getTextNodesIn(node.childNodes[i], includeEmpty));
        }
    }
    return textNodes;
}

// returns ALL the Actions in the Timeline
export function getActions(timeline: Timeline, actionIds: Array<number> = []) {
    const actions: Array<Action> = [];
    timeline.tracks.forEach(track => {
        track.actions.forEach(action => {
            // no array was passed OR
            // does the Action id exist in the array?
            if (!actionIds.length || actionIds.indexOf(action.id) >= 0) {
                actions.push(action);
            }
        });
    });
    return actions;
};

// return the Action with the element
export function getActionWithElement(timeline: Timeline, element: HTMLElement) {
    return getActions(timeline).find(action => action instanceof AddElement && action.element === element);
};

// get the available elements
export function createVisibleDropDown(action: Action, select: HTMLSelectElement, condition: Function) {

    // let lastTrack: Track | null = null;
    let list: {[track: string]: Array<any>} = {};

    const checkChildren = (kids: Array<HTMLElement>) => {
        kids.forEach(kid => {
            // it is an element
            if (kid.dataset.actionId) {
                const actionId = parseInt(kid.dataset.actionId);
                if (actionId !== action.id) {
                    const otherAction = action.timeline.getAction(actionId) as unknown as Action;
                    const res = condition(otherAction);
                    // must return a 0 or 1
                    if (res >= 0) {
                        let trackName = action.track.name;

                        // add by the Track name
                        if (!list[trackName]) {
                            list[trackName] = [];
                        }

                        // add the Action
                        list[trackName].push({
                            id: kid.dataset.actionId,
                            name: otherAction.editorName(),
                            selected: res === 1
                        });
                    }
                }
            }

            // keep looping
            if (kid.children.length) {
                checkChildren(Array.from(kid.children) as Array<HTMLElement>);
            }
        });
    };

    // start the loop
    checkChildren(Array.from(action.timeline.content.children) as Array<HTMLElement>);

    const option = document.createElement("option");
    option.value = "0";
    option.text = "Empty";
    select.appendChild(option);

    Object.keys(list).forEach(track => {
        const option = document.createElement("option");
        option.setAttribute("disabled", "disabled");
        option.innerHTML = `Track ${track}`;
        select.appendChild(option);

        list[track].forEach((action: any) => {
            const option = document.createElement("option");
            option.value = action.id;
            option.innerHTML = `&nbsp;&nbsp;${action.name}`;
            option.selected = action.selected;
            select.appendChild(option);
        });
    });
};

// adds the Action to the passed select field
export function createActionsDropDown(action: Action, select: HTMLSelectElement, condition: Function) {
    
    let lastTrack: Track | null = null;
    let lastClip: Clip | null = null;

    const option = document.createElement("option");
    option.value = "0";
    option.text = "Empty";
    select.appendChild(option);

    getActions(action.timeline).forEach(action => {

        // function should return:
        // -1 = don't show the action
        // 0 = show the action
        // 1 = show the action selected
        const res = condition(action);

        // don't show it
        if (res < 0) {
            return;
        }

        if (action.track instanceof Track && action.track !== lastTrack) {
            lastTrack = action.track;
            const option = document.createElement("option");
            option.setAttribute("disabled", "disabled");
            option.innerHTML = `Track ${lastTrack.name}`;
            select.appendChild(option);
        }
        if (action.clip instanceof Clip && action.clip !== lastClip) {
            lastClip = action.clip;
            const option = document.createElement("option");
            option.setAttribute("disabled", "disabled");
            option.innerHTML = `&nbsp;Clip ${lastClip.name}`;
            select.appendChild(option);
        }

        const option = document.createElement("option");
        option.innerHTML = `&nbsp;&nbsp;${action.editorName()}`;
        option.value = action.id.toString();
        option.selected = res === 1; // mark if it's selected
        
        select.appendChild(option);
    });
};

// show the speeds drop down
export function createDurationsDropDown(action: Action, select: HTMLSelectElement) {
    const durations = [
        { value: 0.001, name: "Instant"},
        { value: 0.4, name: "Fast (0.4 seconds)" },
        { value: 0.8, name: "Normal (0.8 seconds)" },
        { value: 1.2, name: "Slow (1.2 seconds)" },
        { value: 1.6, name: "Slower (1.6 seconds)" }
    ];

    // show the duration times
    durations.forEach(duration => {
        const option = document.createElement("option");
        option.text = duration.name;
        option.value = duration.value.toString();
        option.selected = duration.value === action.duration;
        select.options.add(option);
    });

    // apply the change event to the duration
    select.addEventListener("input", () => {
        action.duration = parseFloat(select.value);
    });
};

// show the easings
export function createEaseDropDown(action: Action, select: HTMLSelectElement) {
    const easeTypes: {[name: string]: any} = {
        "": [
            { label: "Linear", type: "Linear" }
        ]
    };
    // ["Quad", "Sine", "Expo", "Circ", "Cubic", "Quart", "Quint", "Elastic", "Back"].forEach(type => {
    ["Sine", "Cubic", "Back"].forEach(type => {
        easeTypes[type] = [
            { label: "In", type: `${type}In` },
            { label: "Out", type: `${type}Out` },
            { label: "InOut", type: `${type}InOut` }
        ];
    });

    Object.keys(easeTypes).forEach(easeType => {
        const option = document.createElement("optgroup");
        option.label = easeType;
        if (easeType !== "") {
            option.setAttribute("disabled", "disabled");
            select.options.add(option);
        }
        easeTypes[easeType].forEach((ease: any) => {
            const option = document.createElement("option");
            option.innerHTML = (easeType ? " &nbsp; " : "") + ease.label;
            option.value = ease.type;
            option.selected = (action as any).easeType === ease.type;
            select.options.add(option);
        });
    });
};

// show the resources
export function createResourceDropDown(action: Action, resources: Array<IResourceSound | IResourceImage>, select: HTMLSelectElement, value: string = "") {
    // add to the front
    resources.unshift({ name: "", title: "Empty" } as IResourceSound | IResourceImage);

    // loop through the resources
    resources.forEach((resource: any) => {
        const option = document.createElement("option");
        option.text = resource.title;
        option.value = resource.name;
        option.selected = value === resource.name;
        select.options.add(option);
    });
};

// returns true is nodeA comes before nodeB
export function isBefore(nodeA: Node, nodeB: Node) {
    if (nodeA.parentNode === nodeB.parentNode) {
        for (let el = nodeA.previousSibling; el && el.nodeType !== 9; el = el.previousSibling) {
            if (el === nodeB) {
                return true;
            }
        }
    }
    return false;
};

// swaps nodeA and nodeB if they have the same parent
export function swapNodes(nodeA: Node, nodeB: Node) {
    if (nodeA.parentNode === nodeB.parentNode) {
        const parentA = nodeA.parentNode!;
        const siblingA = nodeA.nextSibling === nodeB ? nodeA : nodeA.nextSibling;

        // move `nodeA` to before the `nodeB`
        nodeB.parentNode!.insertBefore(nodeA, nodeB);

        // move `nodeB` to before the sibling of `nodeA`
        parentA.insertBefore(nodeB, siblingA);
    }
};

// compares Action with other Actions 
export function validActionRelations(action: Action, otherIds: Array<number>) {
    // if the `find` returns true, a condition failed
    const result = otherIds.find(otherId => {
        const otherAction = action.timeline.getAction(otherId) as unknown as Action;
        // does it exist?
        if (!otherAction) {
            return true;
        }
        // otherAction comes AFTER our test Action
        if (otherAction.track === action.track && otherAction.index > action.index) {
            return true;
        }
    });
    // `result` will be a number if the `find` found something
    // we are confirming it's type is "undefined"
    return typeof result === "undefined";
};

export function normalizeTextNode(node: Node | null = null) {
    if (node === null) {
        return;
    }
    if (node.nodeType === 3) {
        while (node.nextSibling && node.nextSibling.nodeType === 3) {
            if (node.nodeValue) {
                node.nodeValue += node.nextSibling.nodeValue;
            }
            node.parentNode!.removeChild(node.nextSibling);
        }
    } 
    else {
        normalizeTextNode(node.firstChild);
    }
    normalizeTextNode(node.nextSibling);
};

// export function convertToUnit(value: number, ratio: "width" | "height", element: HTMLElement) {
//     const css = window.getComputedStyle(element);
//     const borderLR = parseFloat(css.borderLeftWidth) + parseFloat(css.borderRightWidth);
//     const borderTB = parseFloat(css.borderTopWidth) + parseFloat(css.borderBottomWidth);

//     const rect = element.getBoundingClientRect();
//     const size = ratio === "width" ? rect.width - borderLR : rect.height - borderTB;

//     return (value * 100) / size;
// };