import { Action } from "../..";
import AddElement from "../Elements/AddElement";
import BaseEffect from "./BaseEffect";

export default class TypingText extends BaseEffect {

    textElement: HTMLElement | null = null;
    resourceNames: Array<string> = []; // TODO
    typeSpeed = 0.08;

    override reset() {
        this.textElement = null;
        super.reset();
    }

    override onStart(direction: number) {
        const otherAction = this.timeline.getAction(this.otherIds[0]) as unknown as AddElement;
        if (!otherAction) {
            return;
        }

        // get the text element
        this.textElement = otherAction.getTextElement;

        // hide all the letters 
        if (this.textElement) {
            Array.from(this.textElement.querySelectorAll(".letter")).forEach(letter => (letter as HTMLElement).style.opacity = "0");
        }
    }

    override onUpdate(direction: number) {
        if (this.textElement) {
            const letters = Array.from(this.textElement.querySelectorAll(".letter"));
            const length = Math.ceil(letters.length * this.percentage01);

            for (let i = 0; i < length; i++) {
                // was it hidden before
                const isHidden = (letters[i] as HTMLElement).style.opacity === "0";

                // make it visible
                (letters[i] as HTMLElement).style.opacity = "1";

                // it's visible, play the sfx
                if (isHidden && direction > 0 && this.resourceNames[0]) {
                    this.timeline.sound.play(this.resourceNames[0], { volume: 0.2 });
                }
            }
        }
    }

    override onEnd() {
        if (this.textElement) {
            Array.from(this.textElement.querySelectorAll(".letter")).forEach(letter => (letter as HTMLElement).style.opacity = "1");
        }
        this.textElement = null;
    }

    override editorHTML(content: HTMLElement) {
        super.editorHTML(content);

        // not used
        const inputDuration = <HTMLElement> content.querySelector(`select[name="duration"]`);
        (inputDuration.closest(".row") as HTMLElement).style.display = "none";

        // we only manipulate one 
        const selectAction = <HTMLSelectElement> content.querySelector(`select[name="other-ids"]`);
        selectAction.multiple = false;
        selectAction.size = 1;

        selectAction.addEventListener("input", () => {
            this.otherIds[0] = parseInt(selectAction.value);

            const otherAction = this.timeline.getAction(this.otherIds[0]) as unknown as AddElement;
            if (!otherAction) {
                return;
            }

            // set the speed here
            this.duration = otherAction.getTextElement!.innerText.length * this.typeSpeed;
        });

        selectAction.dispatchEvent(new CustomEvent("input"));
    }

    override export() {
        return Object.assign(super.export(), {
            otherIds: this.otherIds,
            duration: this.duration
        });
    }
}