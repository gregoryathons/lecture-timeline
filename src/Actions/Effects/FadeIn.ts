import BaseEffect from "./BaseEffect";

export default class FadeIn extends BaseEffect {
    
    override onUpdate(direction: number) {
        Object.values(this.actionInfo).forEach(actionInfo => actionInfo.element.style.opacity = `${this.percentage01}`);
    }

    override onEnd(direction: number) {
        Object.values(this.actionInfo).forEach(actionInfo => actionInfo.element.style.opacity = "1");
    }
}