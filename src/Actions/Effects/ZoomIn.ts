import { Ease } from "../../Ease";
import { lerp } from "../../utils";
import BaseEffect from "./BaseEffect";

export default class ZoomIn extends BaseEffect {

    override onUpdate(direction: number) {
        const value = lerp(2, 1, Ease.SineOut(this.percentage01));

        Object.values(this.actionInfo).forEach(actionInfo => {
            let matrix = actionInfo.matrix;
            matrix = matrix.doScale(value, value);
            actionInfo.element.style.transform = matrix.toString();
            actionInfo.element.style.opacity = this.percentage01.toString();
        });
    }

    override onEnd(direction: number) {
        Object.values(this.actionInfo).forEach(actionInfo => {
            let matrix = actionInfo.matrix;
            matrix = matrix.doScale(1, 1);
            actionInfo.element.style.transform = matrix.toString();
            actionInfo.element.style.opacity = "1";
        });
    }
}