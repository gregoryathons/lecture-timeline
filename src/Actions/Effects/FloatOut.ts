import { Ease } from "../../Ease";
import { lerp } from "../../utils";
import BaseEffect from "./BaseEffect";

export default class FloatOut extends BaseEffect {

    direction = "up";
    remove = true;

    constructor(data: any | null = null) {
        super(data);

        this.direction = data?.direction || this.direction;
        this.remove = data && typeof data.remove === "boolean" ? data.remove : this.remove;
    }

    override onUpdate(direction: number) {
        let valueX = 0, 
            valueY = 0, 
            value = lerp(0, 100, Ease.SineOut(this.percentage01));

        if (this.direction === "up") {
            valueY = -value;
        }
        else if (this.direction === "right") {
            valueX = value;
        }
        else if (this.direction === "down") {
            valueY = value;
        }
        else if (this.direction === "left") {
            valueX = -value;
        }

        Object.values(this.actionInfo).forEach(actionInfo => {
            let matrix = actionInfo.matrix;
            matrix = matrix.doTranslation(valueX, valueY);
            actionInfo.element.style.transform = matrix.toString();
            actionInfo.element.style.opacity = `${1 - this.percentage01}`;
        });
    }

    override onEnd(direction: number) {
        Object.values(this.actionInfo).forEach(actionInfo => {
            actionInfo.element.style.opacity = "0";

            if (this.remove) {
                actionInfo.element.remove();
            }
        });
    }

    override editorHTML(content: HTMLElement) {
        // markup
        const after = `
            <div class="row">
                <div class="three columns">
                    Direction
                </div>
                <div class="nine columns" data-direction="true">
                    <svg height="50" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
                        <polyline points="50,0 75,25 25,25" style="fill:red" data-direction="up" />
                        <polyline points="75,25 100,50 75,75" style="fill:red" data-direction="right" />
                        <polyline points="75,75 50,100 25,75" style="fill:red" data-direction="down" />
                        <polyline points="25,25 25,75 0,50" style="fill:red" data-direction="left" />
                    </svg>
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Remove
                </div>
                <div class="nine columns">
                    <input type="checkbox" name="remove" ${this.remove ? "checked" : ""} />
                </div>
            </div>
        `;

        super.editorHTML(content, after);

        // show our direction
        const polys = content.querySelectorAll(`.nine.columns[data-direction] polyline[data-direction]`);
        polys.forEach(p => {
            const poly = p as SVGPolylineElement;
            poly.addEventListener("mouseover", () => poly.style.opacity = "1");
            poly.addEventListener("mouseout", () => poly.style.opacity = poly.dataset.selected === "true" ? "1" : "0.3");
            poly.addEventListener("click", () => {
                // remove all of them
                polys.forEach(p => { 
                    const poly = p as SVGPolylineElement;
                    poly.dataset.selected = "false"; 
                    poly.style.opacity = "0.3"; 
                });

                // set this one
                poly.dataset.selected = "true";
                poly.style.opacity = "1";

                // set it
                this.direction = poly.dataset.direction!.toString();

                // some random element to bubbles it way to the top
                content.dispatchEvent(new CustomEvent("input", { bubbles: true }));
            });
            
            // default
            poly.dataset.selected = this.direction === poly.dataset.direction ? "true" : "false";
            poly.style.opacity = poly.dataset.selected === "true" ? "1" : "0.3";
        });
    }

    override export() {
        return Object.assign(super.export(), {
            direction: this.direction,
            remove: this.remove
        });
    }
}