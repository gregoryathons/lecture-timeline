import { Ease } from "../../Ease";
import { lerp } from "../../utils";
import BaseEffect from "./BaseEffect";

export default class PopIn extends BaseEffect {

    constructor(data: any | null = null) {
        super(data);

        this.otherIds = data?.otherIds || this.otherIds;
    }

    override onUpdate(direction: number) {
        const value = lerp(0, 1, Ease.BackOut(this.percentage01));

        Object.values(this.actionInfo).forEach(actionInfo => {
            let matrix = actionInfo.matrix;
            matrix = matrix.doScale(value, value);
            actionInfo.element.style.transform = matrix.toString();
        });
    }

    override onEnd(direction: number) {
        Object.values(this.actionInfo).forEach(actionInfo => {
            let matrix = actionInfo.matrix;
            matrix = matrix.doScale(1, 1);
            actionInfo.element.style.transform = matrix.toString();
        });
    }
}