import { Ease } from "../../Ease";
import { lerp } from "../../utils";
import BaseEffect from "./BaseEffect";

export default class PopOut extends BaseEffect {

    remove = true;

    constructor(data: any | null = null) {
        super(data);

        this.remove = data && typeof data.remove === "boolean" ? data.remove : this.remove;
    }

    override onUpdate(direction: number) {
        const value = lerp(1, 0, Ease.BackIn(this.percentage01));

        Object.values(this.actionInfo).forEach(actionInfo => {
            let matrix = actionInfo.matrix;
            matrix = matrix.doScale(value, value);
            actionInfo.element.style.transform = matrix.toString();
        });
    }

    override onEnd(direction: number) {
        Object.values(this.actionInfo).forEach(actionInfo => {
            let matrix = actionInfo.matrix;
            matrix = matrix.doScale(0, 0);
            actionInfo.element.style.transform = matrix.toString();

            if (this.remove) {
                actionInfo.element.remove();
            }
        });
    }

    override editorHTML(content: HTMLElement) {
        const after = `
            <div class="row">
                <div class="three columns">
                    Remove
                </div>
                <div class="nine columns">
                    <input type="checkbox" name="remove" ${this.remove ? "checked" : ""} />
                </div>
            </div>
        `;

        super.editorHTML(content, after);

        const inputRemove = <HTMLInputElement> content.querySelector(`input[name="remove"]`);
        inputRemove.addEventListener("click", () => {
            this.remove = inputRemove.checked;
        });
    }

    override export() {
        return Object.assign(super.export(), {
            remove: this.remove
        });
    }
}