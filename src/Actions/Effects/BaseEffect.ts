import { Action } from "../..";
import AddElement from "../Elements/AddElement";
import { createDurationsDropDown, createVisibleDropDown, validActionRelations } from "../utils";
import Matrix from "../../Matrix";

export interface IBaseEffectElement {
    element: HTMLElement | SVGElement;
    matrix: Matrix;
};

export default class BaseEffect extends Action {
    otherIds: Array<number> = []; // the Action ids
    actionInfo: {[actionId: number]: IBaseEffectElement} = {}; // the other Elements

    constructor(data: any | null = null) {
        super(data);

        this.otherIds = data?.otherIds || this.otherIds;
    }

    override reset() {
        this.actionInfo = {};
        super.reset();
    }

    override onStart(direction: number) {
        this.otherIds.forEach(id => {
            // get Action
            const otherAction = this.timeline.getAction(id);
            if (otherAction && !this.actionInfo[id]) {
                const element = (otherAction as AddElement).element;

                this.actionInfo[id] = {
                    element: element,
                    matrix: new Matrix(element.style.transform)
                };
            }
        });
    }

    override editorHTML(content: HTMLElement, after: string = "") {
        // markup
        content.innerHTML = `
            <hr />
            <div class="row">
                <div class="three columns">
                    Time
                </div>
                <div class="nine columns">
                    <select name="duration" class="u-full-width"></select>
                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    Element(s)
                </div>
                <div class="nine columns">
                    <select name="other-ids" class="u-full-width" size="8" multiple></select>
                </div>
            </div>

            ${after}
        `;

        // fill in the actions
        const selectAction = <HTMLSelectElement> content!.querySelector(`select[name="other-ids"]`);
        createVisibleDropDown(this, selectAction, (action: Action) => {
            return this.otherIds.indexOf(action.id) >= 0 ? 1 : 0;
        });

        // keep reference to the Action id
        selectAction.addEventListener("input", () => {
            this.otherIds.length = 0;
            // grab the values selected, the values being the ids
            Array.from(selectAction.selectedOptions).forEach(option => {
                this.otherIds.push(parseInt(option.value));
            });
        });

        // show the fade times
        const selectDuration = <HTMLSelectElement> content.querySelector(`select[name="duration"]`);
        createDurationsDropDown(this, selectDuration);
    }

    override get isValid(): boolean {
        return this.otherIds.length > 0 && validActionRelations(this, this.otherIds);
    }

    override export() {
        return Object.assign(super.export(), {
            otherIds: this.otherIds
        });
    }
}