import { toRad } from "../../utils";
import BaseEffect from "./BaseEffect";

export default class Wiggle extends BaseEffect {

    override onUpdate(direction: number) {
        const angle = Math.sin(this.percentage01 * 10) * 10;

        Object.values(this.actionInfo).forEach(actionInfo => {
            let matrix = actionInfo.matrix;
            matrix = matrix.doRotate(toRad(angle));
            actionInfo.element.style.transform = matrix.toString();
        });
    }

    override onEnd(direction: number) {
        Object.values(this.actionInfo).forEach(actionInfo => actionInfo.element.style.transform = actionInfo.matrix.toString());
    }
}