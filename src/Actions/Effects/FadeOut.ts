import BaseEffect from "./BaseEffect";

export default class FadeOut extends BaseEffect {
    
    remove = true;

    constructor(data: any | null = null) {
        super(data);

        this.remove = data && typeof data.remove === "boolean" ? data.remove : this.remove;
    }

    override onUpdate(direction: number) {
        Object.values(this.actionInfo).forEach(actionInfo => actionInfo.element.style.opacity = `${1 - this.percentage01}`);
    }

    override onEnd(direction: number) {
        Object.values(this.actionInfo).forEach(actionInfo => {
            actionInfo.element.style.opacity = "0";
            
            if (this.remove) {
                actionInfo.element.remove();
            }
        });
    }

    override editorHTML(content: HTMLElement): void {
        const after = `
            <div class="row">
                <div class="three columns">
                    Remove
                </div>
                <div class="nine columns">
                    <input type="checkbox" name="remove" ${this.remove ? "checked" : ""} />
                </div>
            </div>
        `;

        super.editorHTML(content, after);

        const inputRemove = <HTMLInputElement> content.querySelector(`input[name="remove"]`);
        inputRemove.addEventListener("click", () => {
            this.remove = inputRemove.checked;
        });
    }

    override export() {
        return Object.assign(super.export(), {
            remove: this.remove
        });
    }
}