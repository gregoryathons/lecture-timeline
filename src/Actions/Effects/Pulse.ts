import { Ease } from "../../Ease";
import { lerp } from "../../utils";
import BaseEffect from "./BaseEffect";

export default class Pulse extends BaseEffect {

    override onUpdate(direction: number) {
        const value = this.percentage01 < 0.5 ? lerp(1, 1.1, Ease.SineOut(this.percentage01 * 2)) : lerp(1.1, 1, Ease.SineIn((this.percentage01 - 0.5) * 2));

        Object.values(this.actionInfo).forEach(actionInfo => {
            let matrix = actionInfo.matrix;
            matrix = matrix.doScale(value, value);
            actionInfo.element.style.transform = matrix.toString();
        });
    }

    override onEnd(direction: number) {
        Object.values(this.actionInfo).forEach(actionInfo => actionInfo.element.style.transform = actionInfo.matrix.toString());
    }
}