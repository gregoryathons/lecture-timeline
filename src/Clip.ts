import { Track } from ".";
import Emitter from "./Emitter";
import { ElementType } from "./Const";
import { log } from "./Timeline";

// define the Clip Element
class HTMLClipElement extends HTMLElement {}
customElements.define(ElementType.Clip, HTMLClipElement);

export default class Clip extends Emitter {
    static id = 1;

    id = 0;
    track: Track;

    resourceName = "";
    startTime = 0;

    private _name = ""; // for the editor
    private _audioLength = 0; // for the editor

    get name() {
        return this._name || this.getResource().title;
    }
    set name(value) {
        this._name = value;
    }
    get audioLength() {
        const audioBuffer = this.getResource()?.audioBuffer;
        return this._audioLength || audioBuffer?.duration || 0;
    }
    set audioLength(value) {
        this._audioLength = value;
    }
    get endTime() {
        return this.startTime + this.audioLength;
    }
    get timeline() {
        return this.track?.timeline;
    }

    constructor(data: any | null = null) {
        super();

        this.id = data?.id || Clip.id++;
        this.resourceName = data?.resourceName || this.resourceName;
        this.startTime = data?.startTime || this.startTime;

        // create our element
        this._element = document.createElement(ElementType.Clip);
        this._element.dataset.id = this.id.toString();

        // so we don't get duplicates
        if (this.id >= Clip.id) {
            Clip.id = this.id + 1;
        }
    }

    addTo(track: Track) {
        this.track = track;

        // append to
        this.track.appendChild(this._element);

        return this;
    }

    removeFrom() {
        this._element.remove();
        return this;
    }

    getResource() {
        return this.timeline ? this.timeline.loader.getResource(this.resourceName) : {};
    }

    // import/export

    export() {
        return { 
            id: this.id,
            resourceName: this.resourceName,
            startTime: this.startTime,
        };
    }
}